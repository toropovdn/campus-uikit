export interface AutocompleteItemInterface {
    id: number;
    title: string;
}
