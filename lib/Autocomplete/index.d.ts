import Autocomplete from "./src/autocomplete-component.vue";
import AutocompleteItem from "./src/autocomplete-item.vue";
export { Autocomplete, AutocompleteItem };
