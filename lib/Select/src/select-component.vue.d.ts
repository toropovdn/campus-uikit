import { SelectItem } from "./SelectInterface.interface";
import { DropdownPlacement } from "../../Dropdown/src/DropdownPlacement.enum";
import { SelectType } from "./Select.enum";
declare const _default: __VLS_WithTemplateSlots<import("vue").DefineComponent<__VLS_WithDefaults<__VLS_TypePropsToRuntimeProps<{
    options: SelectItem[];
    modelValue: string | string[];
    triggerClass?: string | undefined;
    label?: string | undefined;
    isLoading?: boolean | undefined;
    isDisabled?: boolean | undefined;
    isError?: boolean | undefined;
    isSuccess?: boolean | undefined;
    errorText?: string | undefined;
    withSearch?: boolean | undefined;
    isNeedClear?: boolean | undefined;
    selectType?: SelectType | undefined;
    placement?: DropdownPlacement | undefined;
    infoText?: string | undefined;
    onlyRead?: boolean | undefined;
    onlyReadText?: string | undefined;
    withBigAdaptive: boolean;
}>, {
    withSearch: boolean;
    isNeedClear: boolean;
    selectType: SelectType;
    placement: DropdownPlacement;
    modelValue: string;
    withBigAdaptive: boolean;
}>, {}, unknown, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {
    "update:modelValue": (value: string | number | (string | number)[]) => void;
    update: () => void;
}, string, import("vue").PublicProps, Readonly<import("vue").ExtractPropTypes<__VLS_WithDefaults<__VLS_TypePropsToRuntimeProps<{
    options: SelectItem[];
    modelValue: string | string[];
    triggerClass?: string | undefined;
    label?: string | undefined;
    isLoading?: boolean | undefined;
    isDisabled?: boolean | undefined;
    isError?: boolean | undefined;
    isSuccess?: boolean | undefined;
    errorText?: string | undefined;
    withSearch?: boolean | undefined;
    isNeedClear?: boolean | undefined;
    selectType?: SelectType | undefined;
    placement?: DropdownPlacement | undefined;
    infoText?: string | undefined;
    onlyRead?: boolean | undefined;
    onlyReadText?: string | undefined;
    withBigAdaptive: boolean;
}>, {
    withSearch: boolean;
    isNeedClear: boolean;
    selectType: SelectType;
    placement: DropdownPlacement;
    modelValue: string;
    withBigAdaptive: boolean;
}>>> & {
    "onUpdate:modelValue"?: ((value: string | number | (string | number)[]) => any) | undefined;
    onUpdate?: (() => any) | undefined;
}, {
    modelValue: string | string[];
    placement: DropdownPlacement;
    withSearch: boolean;
    isNeedClear: boolean;
    selectType: SelectType;
    withBigAdaptive: boolean;
}, {}>, {
    trigger?(_: {
        activeValue: {};
    }): any;
    "select-item"?(_: {
        item: SelectItem;
        changeVisibleShowContent: any;
    }): any;
}>;
export default _default;
type __VLS_NonUndefinedable<T> = T extends undefined ? never : T;
type __VLS_TypePropsToRuntimeProps<T> = {
    [K in keyof T]-?: {} extends Pick<T, K> ? {
        type: import('vue').PropType<__VLS_NonUndefinedable<T[K]>>;
    } : {
        type: import('vue').PropType<T[K]>;
        required: true;
    };
};
type __VLS_WithDefaults<P, D> = {
    [K in keyof Pick<P, keyof P>]: K extends keyof D ? __VLS_Prettify<P[K] & {
        default: D[K];
    }> : P[K];
};
type __VLS_Prettify<T> = {
    [K in keyof T]: T[K];
} & {};
type __VLS_WithTemplateSlots<T, S> = T & {
    new (): {
        $slots: S;
    };
};
