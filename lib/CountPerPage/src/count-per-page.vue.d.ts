import { TitlePositionEnum } from "./title-position.enum";
declare const _default: import("vue").DefineComponent<__VLS_WithDefaults<__VLS_TypePropsToRuntimeProps<{
    title: string;
    options: Array<number>;
    position?: TitlePositionEnum | undefined;
    modelValue: number;
}>, {
    title: string;
    options: () => number[];
    position: TitlePositionEnum;
}>, {}, unknown, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {
    "update:modelValue": (value: number) => void;
    update: (value: number) => void;
}, string, import("vue").PublicProps, Readonly<import("vue").ExtractPropTypes<__VLS_WithDefaults<__VLS_TypePropsToRuntimeProps<{
    title: string;
    options: Array<number>;
    position?: TitlePositionEnum | undefined;
    modelValue: number;
}>, {
    title: string;
    options: () => number[];
    position: TitlePositionEnum;
}>>> & {
    "onUpdate:modelValue"?: ((value: number) => any) | undefined;
    onUpdate?: ((value: number) => any) | undefined;
}, {
    title: string;
    options: Array<number>;
    position: TitlePositionEnum;
}, {}>;
export default _default;
type __VLS_NonUndefinedable<T> = T extends undefined ? never : T;
type __VLS_TypePropsToRuntimeProps<T> = {
    [K in keyof T]-?: {} extends Pick<T, K> ? {
        type: import('vue').PropType<__VLS_NonUndefinedable<T[K]>>;
    } : {
        type: import('vue').PropType<T[K]>;
        required: true;
    };
};
type __VLS_WithDefaults<P, D> = {
    [K in keyof Pick<P, keyof P>]: K extends keyof D ? __VLS_Prettify<P[K] & {
        default: D[K];
    }> : P[K];
};
type __VLS_Prettify<T> = {
    [K in keyof T]: T[K];
} & {};
