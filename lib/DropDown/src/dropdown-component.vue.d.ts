import type { DropdownTrigger } from "./DropdownTrigger.enum";
import type { DropdownPlacement } from "./DropdownPlacement.enum";
declare const _default: __VLS_WithTemplateSlots<import("vue").DefineComponent<__VLS_WithDefaults<__VLS_TypePropsToRuntimeProps<{
    modelValue?: boolean | undefined;
    distance?: number | undefined;
    skidding?: number | undefined;
    autoSize?: boolean | "min" | "max" | undefined;
    placement?: DropdownPlacement | undefined;
    trigger?: DropdownTrigger | DropdownTrigger[] | undefined;
    autoHide?: boolean | undefined;
    arrowHide?: boolean | undefined;
    onlyRead?: boolean | undefined;
}>, {
    placement: DropdownPlacement.Bottom;
    autoHide: boolean;
    arrowHide: boolean;
    distance: number;
    skidding: number;
    modelValue: undefined;
    onlyRead: boolean;
}>, {}, unknown, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {
    "update:modelValue": (value: boolean) => void;
    hideDropdown: () => void;
}, string, import("vue").PublicProps, Readonly<import("vue").ExtractPropTypes<__VLS_WithDefaults<__VLS_TypePropsToRuntimeProps<{
    modelValue?: boolean | undefined;
    distance?: number | undefined;
    skidding?: number | undefined;
    autoSize?: boolean | "min" | "max" | undefined;
    placement?: DropdownPlacement | undefined;
    trigger?: DropdownTrigger | DropdownTrigger[] | undefined;
    autoHide?: boolean | undefined;
    arrowHide?: boolean | undefined;
    onlyRead?: boolean | undefined;
}>, {
    placement: DropdownPlacement.Bottom;
    autoHide: boolean;
    arrowHide: boolean;
    distance: number;
    skidding: number;
    modelValue: undefined;
    onlyRead: boolean;
}>>> & {
    "onUpdate:modelValue"?: ((value: boolean) => any) | undefined;
    onHideDropdown?: (() => any) | undefined;
}, {
    modelValue: boolean;
    distance: number;
    skidding: number;
    placement: DropdownPlacement;
    autoHide: boolean;
    arrowHide: boolean;
    onlyRead: boolean;
}, {}>, {
    trigger?(_: {}): any;
    dropdown?(_: {
        hide: any;
    }): any;
}>;
export default _default;
type __VLS_NonUndefinedable<T> = T extends undefined ? never : T;
type __VLS_TypePropsToRuntimeProps<T> = {
    [K in keyof T]-?: {} extends Pick<T, K> ? {
        type: import('vue').PropType<__VLS_NonUndefinedable<T[K]>>;
    } : {
        type: import('vue').PropType<T[K]>;
        required: true;
    };
};
type __VLS_WithDefaults<P, D> = {
    [K in keyof Pick<P, keyof P>]: K extends keyof D ? __VLS_Prettify<P[K] & {
        default: D[K];
    }> : P[K];
};
type __VLS_Prettify<T> = {
    [K in keyof T]: T[K];
} & {};
type __VLS_WithTemplateSlots<T, S> = T & {
    new (): {
        $slots: S;
    };
};
