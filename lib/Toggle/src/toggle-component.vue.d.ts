declare const _default: import("vue").DefineComponent<__VLS_WithDefaults<__VLS_TypePropsToRuntimeProps<{
    modelValue?: boolean | undefined;
    name?: string | undefined;
    disabled?: boolean | undefined;
    required?: boolean | undefined;
    falseValue?: string | number | boolean | undefined;
    trueValue?: string | number | boolean | undefined;
    offLabel?: string | undefined;
    onLabel?: string | undefined;
    labelledby?: string | undefined;
    describedby?: string | undefined;
}>, {
    modelValue: undefined;
    name: string;
    disabled: boolean;
    required: boolean;
    falseValue: boolean;
    trueValue: boolean;
    offLabel: string;
    onLabel: string;
    labelledby: string;
    describedby: string;
}>, {}, unknown, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {
    "update:modelValue": (...args: any[]) => void;
}, string, import("vue").PublicProps, Readonly<import("vue").ExtractPropTypes<__VLS_WithDefaults<__VLS_TypePropsToRuntimeProps<{
    modelValue?: boolean | undefined;
    name?: string | undefined;
    disabled?: boolean | undefined;
    required?: boolean | undefined;
    falseValue?: string | number | boolean | undefined;
    trueValue?: string | number | boolean | undefined;
    offLabel?: string | undefined;
    onLabel?: string | undefined;
    labelledby?: string | undefined;
    describedby?: string | undefined;
}>, {
    modelValue: undefined;
    name: string;
    disabled: boolean;
    required: boolean;
    falseValue: boolean;
    trueValue: boolean;
    offLabel: string;
    onLabel: string;
    labelledby: string;
    describedby: string;
}>>> & {
    "onUpdate:modelValue"?: ((...args: any[]) => any) | undefined;
}, {
    modelValue: boolean;
    disabled: boolean;
    required: boolean;
    name: string;
    falseValue: string | number | boolean;
    trueValue: string | number | boolean;
    offLabel: string;
    onLabel: string;
    labelledby: string;
    describedby: string;
}, {}>;
export default _default;
type __VLS_NonUndefinedable<T> = T extends undefined ? never : T;
type __VLS_TypePropsToRuntimeProps<T> = {
    [K in keyof T]-?: {} extends Pick<T, K> ? {
        type: import('vue').PropType<__VLS_NonUndefinedable<T[K]>>;
    } : {
        type: import('vue').PropType<T[K]>;
        required: true;
    };
};
type __VLS_WithDefaults<P, D> = {
    [K in keyof Pick<P, keyof P>]: K extends keyof D ? __VLS_Prettify<P[K] & {
        default: D[K];
    }> : P[K];
};
type __VLS_Prettify<T> = {
    [K in keyof T]: T[K];
} & {};
