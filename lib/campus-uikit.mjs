import { defineComponent as ae, openBlock as S, createElementBlock as P, normalizeClass as R, renderSlot as ee, createBlock as $e, resolveDynamicComponent as po, mergeProps as ct, withCtx as Ce, createElementVNode as M, unref as Ee, createCommentVNode as j, computed as B, toDisplayString as pe, pushScopeId as wa, popScopeId as Ca, nextTick as mo, normalizeProps as La, guardReactiveProps as Na, withScopeId as Oa, resolveComponent as In, normalizeStyle as Nn, withKeys as ho, Fragment as Fe, createVNode as ot, toRefs as Pt, ref as ue, getCurrentScope as ka, onScopeDispose as Aa, watchEffect as $a, watch as rt, getCurrentInstance as gn, onMounted as _o, useAttrs as Sa, withDirectives as go, renderList as Tt, h as vo, inject as Ia, onUnmounted as Pa, shallowRef as yo, onBeforeMount as Ma, Text as Ra, createTextVNode as ms, useCssVars as Da, withModifiers as Fa, vShow as Va, Transition as Ba, TransitionGroup as Ua } from "vue";
const xa = /* @__PURE__ */ ae({
  __name: "label-component",
  props: {
    type: { default: "blue" }
  },
  setup(e) {
    const t = e;
    return (n, s) => (S(), P("div", {
      class: R([n.$style.label, n.$style[t.type]])
    }, [
      ee(n.$slots, "default")
    ], 2));
  }
}), Ha = "_label_158y9_1", Wa = "_blue_158y9_25", Ga = "_green_158y9_30", za = "_blueGreen_158y9_35", Ya = "_purple_158y9_40", ja = "_gray_158y9_45", qa = {
  label: Ha,
  blue: Wa,
  green: Ga,
  blueGreen: za,
  purple: Ya,
  gray: ja
}, Ie = (e, t) => {
  const n = e.__vccOpts || e;
  for (const [s, o] of t)
    n[s] = o;
  return n;
}, Ka = {
  $style: qa
}, Qt = /* @__PURE__ */ Ie(xa, [["__cssModules", Ka]]);
Qt.install = (e) => {
  e.component(Qt.__name, Qt);
};
var Eo = /* @__PURE__ */ ((e) => (e.BLUE = "blue", e.ACCENT = "accent", e.WHITE = "white", e.TEXT = "text", e.SECONDARY = "secondary", e.SKYBLUE = "skyblue", e))(Eo || {});
const Xa = { class: "slot_default" }, Za = /* @__PURE__ */ M("path", { d: "M12 6.58918C12 6.26382 11.7356 5.99707 11.4118 6.02896C10.3279 6.13573 9.28936 6.5361 8.41063 7.19211C7.37397 7.96603 6.61522 9.05427 6.24749 10.2946C5.87977 11.5349 5.92278 12.8609 6.3701 14.0747C6.81743 15.2886 7.64511 16.3254 8.72975 17.0305C9.81438 17.7356 11.0979 18.0713 12.3888 17.9875C13.6798 17.9036 14.9091 17.4048 15.8934 16.5653C16.8777 15.7258 17.5644 14.5907 17.8509 13.3292C18.0939 12.2598 18.0383 11.1482 17.6959 10.1142C17.5937 9.80534 17.2426 9.67197 16.9452 9.80402C16.6478 9.93607 16.5176 10.2836 16.6124 10.5948C16.856 11.3942 16.8886 12.2467 16.702 13.0682C16.4717 14.082 15.9199 14.9942 15.1288 15.6688C14.3378 16.3434 13.3499 16.7443 12.3125 16.8117C11.275 16.8791 10.2436 16.6093 9.37193 16.0427C8.50029 15.476 7.83514 14.6428 7.47566 13.6673C7.11617 12.6918 7.08161 11.6263 7.37712 10.6295C7.67264 9.63275 8.2824 8.7582 9.11548 8.13626C9.79052 7.63231 10.5829 7.31609 11.4123 7.21424C11.7353 7.17459 12 6.91454 12 6.58918Z" }, null, -1), Ja = [
  Za
], Qa = /* @__PURE__ */ M("svg", {
  xmlns: "http://www.w3.org/2000/svg",
  width: "16",
  height: "16",
  viewBox: "0 0 16 16",
  fill: "none"
}, [
  /* @__PURE__ */ M("path", {
    d: "M9 3L8.285 3.6965L12.075 7.5H2V8.5H12.075L8.285 12.2865L9 13L14 8L9 3Z",
    fill: "#0E89E6"
  })
], -1), er = [
  Qa
], tr = /* @__PURE__ */ ae({
  __name: "Button",
  props: {
    type: { default: "primary" },
    href: {},
    isDisabled: { type: Boolean, default: !1 },
    isLoading: { type: Boolean, default: !1 }
  },
  setup(e) {
    const t = e;
    return (n, s) => (S(), $e(po(n.href ? "router-link" : "button"), ct({
      class: [
        n.$style.button,
        n.$style[t.type],
        n.isDisabled && n.$style.disabled,
        n.isLoading && n.$style.loading
      ],
      to: n.href
    }, n.$attrs, {
      disabled: n.isDisabled || n.isLoading
    }), {
      default: Ce(() => [
        n.isLoading ? (S(), P("div", {
          key: 1,
          class: R(n.$style.loadingWrapper)
        }, [
          (S(), P("svg", {
            class: R(n.$style.loading),
            xmlns: "http://www.w3.org/2000/svg",
            width: "24",
            height: "24",
            viewBox: "0 0 24 24",
            fill: "none"
          }, Ja, 2))
        ], 2)) : (S(), P("div", {
          key: 0,
          class: R(n.$style.content)
        }, [
          ee(n.$slots, "icon"),
          M("section", Xa, [
            ee(n.$slots, "default")
          ])
        ], 2)),
        n.type === Ee(Eo).TEXT && !n.isLoading ? (S(), P("div", {
          key: 2,
          class: R(n.$style.icon)
        }, er, 2)) : j("", !0)
      ]),
      _: 3
    }, 16, ["class", "to", "disabled"]));
  }
}), nr = "_button_c7af1_1", sr = "_content_c7af1_43", or = "_loadingWrapper_c7af1_50", ar = "_rotate_c7af1_1", rr = "_blue_c7af1_61", ir = "_disabled_c7af1_78", lr = "_loading_c7af1_50", ur = "_accent_c7af1_93", cr = "_white_c7af1_125", dr = "_text_c7af1_157", fr = "_secondary_c7af1_185", pr = "_skyblue_c7af1_212", mr = "_icon_c7af1_255", hr = {
  button: nr,
  content: sr,
  loadingWrapper: or,
  rotate: ar,
  blue: rr,
  disabled: ir,
  loading: lr,
  accent: ur,
  white: cr,
  text: dr,
  secondary: fr,
  skyblue: pr,
  icon: mr
}, _r = {
  $style: hr
}, en = /* @__PURE__ */ Ie(tr, [["__cssModules", _r]]);
en.install = (e) => {
  e.component(en.__name, en);
};
var bo = /* @__PURE__ */ ((e) => (e.TOP = "top", e.RIGHT = "right", e.BOTTOM = "bottom", e.LEFT = "left", e))(bo || {});
const gr = /* @__PURE__ */ M("svg", {
  width: "20",
  height: "20",
  viewBox: "0 0 20 20",
  fill: "none",
  xmlns: "http://www.w3.org/2000/svg"
}, [
  /* @__PURE__ */ M("g", { id: "check" }, [
    /* @__PURE__ */ M("path", {
      id: "Union",
      d: "M8.11058 14C8.00623 14 7.90531 13.9819 7.80783 13.9458C7.71035 13.9102 7.61599 13.8505 7.52476 13.7667L4.22445 10.7353C4.06823 10.5918 3.99356 10.4095 4.00043 10.1885C4.00668 9.967 4.08792 9.78447 4.24414 9.64098C4.40036 9.49749 4.59563 9.42574 4.82996 9.42574C5.06429 9.42574 5.25957 9.49749 5.41579 9.64098L8.14995 12.1524L14.6137 6.21524C14.7699 6.07175 14.9618 6 15.1892 6C15.4173 6 15.6095 6.07175 15.7657 6.21524C15.9219 6.35873 16 6.53523 16 6.74473C16 6.95365 15.9219 7.12986 15.7657 7.27335L8.69641 13.7667C8.60517 13.8505 8.51082 13.9102 8.41334 13.9458C8.31585 13.9819 8.21494 14 8.11058 14Z",
      fill: "white"
    })
  ])
], -1), vr = [
  gr
], yr = { key: 0 }, Er = ["disabled", "value", "checked"], br = /* @__PURE__ */ ae({
  __name: "CheckBox",
  props: {
    label: { default: "" },
    modelValue: { type: Boolean, default: !1 },
    isSmall: { type: Boolean, default: !1 },
    labelPosition: { default: bo.RIGHT },
    rounded: { type: Boolean, default: !1 },
    disabled: { type: Boolean, default: !1 },
    size: { default: "big" }
  },
  setup(e) {
    const t = e, n = B(() => !!t.modelValue), s = B(() => t.isSmall ? "small" : t.size ? t.size : "big");
    return (o, a) => (S(), P("label", {
      class: R([o.$style.label, o.$style[o.labelPosition]])
    }, [
      M("span", {
        class: R([
          o.$style.container,
          o.$style[s.value],
          n.value && o.$style.activeContainer,
          o.rounded && o.$style.rounded,
          o.disabled && o.$style.disabled
        ])
      }, vr, 2),
      o.label ? (S(), P("span", yr, pe(o.label), 1)) : j("", !0),
      M("input", ct({
        type: "checkbox",
        disabled: o.disabled,
        onChange: a[0] || (a[0] = (r) => o.$emit("update:modelValue", r.target.checked))
      }, o.$attrs, {
        value: o.modelValue,
        checked: o.modelValue
      }), null, 16, Er)
    ], 2));
  }
}), Tr = "_label_mweb9_1", wr = "_container_mweb9_24", Cr = "_activeContainer_mweb9_36", Lr = "_disabled_mweb9_40", Nr = "_big_mweb9_49", Or = "_small_mweb9_54", kr = "_xs_mweb9_59", Ar = "_rounded_mweb9_64", $r = "_top_mweb9_68", Sr = "_right_mweb9_72", Ir = "_bottom_mweb9_76", Pr = "_left_mweb9_80", Mr = {
  label: Tr,
  container: wr,
  activeContainer: Cr,
  disabled: Lr,
  big: Nr,
  small: Or,
  xs: kr,
  rounded: Ar,
  top: $r,
  right: Sr,
  bottom: Ir,
  left: Pr
}, Rr = {
  $style: Mr
}, Mt = /* @__PURE__ */ Ie(br, [["__cssModules", Rr]]);
Mt.install = (e) => {
  e.component(Mt.__name, Mt);
};
const Dr = ["top", "right", "bottom", "left"], hs = ["start", "end"], _s = /* @__PURE__ */ Dr.reduce((e, t) => e.concat(t, t + "-" + hs[0], t + "-" + hs[1]), []), xt = Math.min, st = Math.max, Fr = {
  left: "right",
  right: "left",
  bottom: "top",
  top: "bottom"
}, Vr = {
  start: "end",
  end: "start"
};
function Pn(e, t, n) {
  return st(e, xt(t, n));
}
function dt(e, t) {
  return typeof e == "function" ? e(t) : e;
}
function Se(e) {
  return e.split("-")[0];
}
function we(e) {
  return e.split("-")[1];
}
function To(e) {
  return e === "x" ? "y" : "x";
}
function Qn(e) {
  return e === "y" ? "height" : "width";
}
function qt(e) {
  return ["top", "bottom"].includes(Se(e)) ? "y" : "x";
}
function es(e) {
  return To(qt(e));
}
function wo(e, t, n) {
  n === void 0 && (n = !1);
  const s = we(e), o = es(e), a = Qn(o);
  let r = o === "x" ? s === (n ? "end" : "start") ? "right" : "left" : s === "start" ? "bottom" : "top";
  return t.reference[a] > t.floating[a] && (r = dn(r)), [r, dn(r)];
}
function Br(e) {
  const t = dn(e);
  return [cn(e), t, cn(t)];
}
function cn(e) {
  return e.replace(/start|end/g, (t) => Vr[t]);
}
function Ur(e, t, n) {
  const s = ["left", "right"], o = ["right", "left"], a = ["top", "bottom"], r = ["bottom", "top"];
  switch (e) {
    case "top":
    case "bottom":
      return n ? t ? o : s : t ? s : o;
    case "left":
    case "right":
      return t ? a : r;
    default:
      return [];
  }
}
function xr(e, t, n, s) {
  const o = we(e);
  let a = Ur(Se(e), n === "start", s);
  return o && (a = a.map((r) => r + "-" + o), t && (a = a.concat(a.map(cn)))), a;
}
function dn(e) {
  return e.replace(/left|right|bottom|top/g, (t) => Fr[t]);
}
function Hr(e) {
  return {
    top: 0,
    right: 0,
    bottom: 0,
    left: 0,
    ...e
  };
}
function Co(e) {
  return typeof e != "number" ? Hr(e) : {
    top: e,
    right: e,
    bottom: e,
    left: e
  };
}
function Rt(e) {
  return {
    ...e,
    top: e.y,
    left: e.x,
    right: e.x + e.width,
    bottom: e.y + e.height
  };
}
function gs(e, t, n) {
  let {
    reference: s,
    floating: o
  } = e;
  const a = qt(t), r = es(t), l = Qn(r), i = Se(t), d = a === "y", _ = s.x + s.width / 2 - o.width / 2, h = s.y + s.height / 2 - o.height / 2, m = s[l] / 2 - o[l] / 2;
  let b;
  switch (i) {
    case "top":
      b = {
        x: _,
        y: s.y - o.height
      };
      break;
    case "bottom":
      b = {
        x: _,
        y: s.y + s.height
      };
      break;
    case "right":
      b = {
        x: s.x + s.width,
        y: h
      };
      break;
    case "left":
      b = {
        x: s.x - o.width,
        y: h
      };
      break;
    default:
      b = {
        x: s.x,
        y: s.y
      };
  }
  switch (we(t)) {
    case "start":
      b[r] -= m * (n && d ? -1 : 1);
      break;
    case "end":
      b[r] += m * (n && d ? -1 : 1);
      break;
  }
  return b;
}
const Wr = async (e, t, n) => {
  const {
    placement: s = "bottom",
    strategy: o = "absolute",
    middleware: a = [],
    platform: r
  } = n, l = a.filter(Boolean), i = await (r.isRTL == null ? void 0 : r.isRTL(t));
  let d = await r.getElementRects({
    reference: e,
    floating: t,
    strategy: o
  }), {
    x: _,
    y: h
  } = gs(d, s, i), m = s, b = {}, u = 0;
  for (let v = 0; v < l.length; v++) {
    const {
      name: y,
      fn: f
    } = l[v], {
      x: g,
      y: w,
      data: E,
      reset: L
    } = await f({
      x: _,
      y: h,
      initialPlacement: s,
      placement: m,
      strategy: o,
      middlewareData: b,
      rects: d,
      platform: r,
      elements: {
        reference: e,
        floating: t
      }
    });
    _ = g ?? _, h = w ?? h, b = {
      ...b,
      [y]: {
        ...b[y],
        ...E
      }
    }, L && u <= 50 && (u++, typeof L == "object" && (L.placement && (m = L.placement), L.rects && (d = L.rects === !0 ? await r.getElementRects({
      reference: e,
      floating: t,
      strategy: o
    }) : L.rects), {
      x: _,
      y: h
    } = gs(d, m, i)), v = -1);
  }
  return {
    x: _,
    y: h,
    placement: m,
    strategy: o,
    middlewareData: b
  };
};
async function vn(e, t) {
  var n;
  t === void 0 && (t = {});
  const {
    x: s,
    y: o,
    platform: a,
    rects: r,
    elements: l,
    strategy: i
  } = e, {
    boundary: d = "clippingAncestors",
    rootBoundary: _ = "viewport",
    elementContext: h = "floating",
    altBoundary: m = !1,
    padding: b = 0
  } = dt(t, e), u = Co(b), y = l[m ? h === "floating" ? "reference" : "floating" : h], f = Rt(await a.getClippingRect({
    element: (n = await (a.isElement == null ? void 0 : a.isElement(y))) == null || n ? y : y.contextElement || await (a.getDocumentElement == null ? void 0 : a.getDocumentElement(l.floating)),
    boundary: d,
    rootBoundary: _,
    strategy: i
  })), g = h === "floating" ? {
    ...r.floating,
    x: s,
    y: o
  } : r.reference, w = await (a.getOffsetParent == null ? void 0 : a.getOffsetParent(l.floating)), E = await (a.isElement == null ? void 0 : a.isElement(w)) ? await (a.getScale == null ? void 0 : a.getScale(w)) || {
    x: 1,
    y: 1
  } : {
    x: 1,
    y: 1
  }, L = Rt(a.convertOffsetParentRelativeRectToViewportRelativeRect ? await a.convertOffsetParentRelativeRectToViewportRelativeRect({
    elements: l,
    rect: g,
    offsetParent: w,
    strategy: i
  }) : g);
  return {
    top: (f.top - L.top + u.top) / E.y,
    bottom: (L.bottom - f.bottom + u.bottom) / E.y,
    left: (f.left - L.left + u.left) / E.x,
    right: (L.right - f.right + u.right) / E.x
  };
}
const Gr = (e) => ({
  name: "arrow",
  options: e,
  async fn(t) {
    const {
      x: n,
      y: s,
      placement: o,
      rects: a,
      platform: r,
      elements: l,
      middlewareData: i
    } = t, {
      element: d,
      padding: _ = 0
    } = dt(e, t) || {};
    if (d == null)
      return {};
    const h = Co(_), m = {
      x: n,
      y: s
    }, b = es(o), u = Qn(b), v = await r.getDimensions(d), y = b === "y", f = y ? "top" : "left", g = y ? "bottom" : "right", w = y ? "clientHeight" : "clientWidth", E = a.reference[u] + a.reference[b] - m[b] - a.floating[u], L = m[b] - a.reference[b], N = await (r.getOffsetParent == null ? void 0 : r.getOffsetParent(d));
    let A = N ? N[w] : 0;
    (!A || !await (r.isElement == null ? void 0 : r.isElement(N))) && (A = l.floating[w] || a.floating[u]);
    const D = E / 2 - L / 2, k = A / 2 - v[u] / 2 - 1, U = xt(h[f], k), X = xt(h[g], k), W = U, te = A - v[u] - X, ne = A / 2 - v[u] / 2 + D, ie = Pn(W, ne, te), J = !i.arrow && we(o) != null && ne !== ie && a.reference[u] / 2 - (ne < W ? U : X) - v[u] / 2 < 0, le = J ? ne < W ? ne - W : ne - te : 0;
    return {
      [b]: m[b] + le,
      data: {
        [b]: ie,
        centerOffset: ne - ie - le,
        ...J && {
          alignmentOffset: le
        }
      },
      reset: J
    };
  }
});
function zr(e, t, n) {
  return (e ? [...n.filter((o) => we(o) === e), ...n.filter((o) => we(o) !== e)] : n.filter((o) => Se(o) === o)).filter((o) => e ? we(o) === e || (t ? cn(o) !== o : !1) : !0);
}
const Yr = function(e) {
  return e === void 0 && (e = {}), {
    name: "autoPlacement",
    options: e,
    async fn(t) {
      var n, s, o;
      const {
        rects: a,
        middlewareData: r,
        placement: l,
        platform: i,
        elements: d
      } = t, {
        crossAxis: _ = !1,
        alignment: h,
        allowedPlacements: m = _s,
        autoAlignment: b = !0,
        ...u
      } = dt(e, t), v = h !== void 0 || m === _s ? zr(h || null, b, m) : m, y = await vn(t, u), f = ((n = r.autoPlacement) == null ? void 0 : n.index) || 0, g = v[f];
      if (g == null)
        return {};
      const w = wo(g, a, await (i.isRTL == null ? void 0 : i.isRTL(d.floating)));
      if (l !== g)
        return {
          reset: {
            placement: v[0]
          }
        };
      const E = [y[Se(g)], y[w[0]], y[w[1]]], L = [...((s = r.autoPlacement) == null ? void 0 : s.overflows) || [], {
        placement: g,
        overflows: E
      }], N = v[f + 1];
      if (N)
        return {
          data: {
            index: f + 1,
            overflows: L
          },
          reset: {
            placement: N
          }
        };
      const A = L.map((U) => {
        const X = we(U.placement);
        return [U.placement, X && _ ? (
          // Check along the mainAxis and main crossAxis side.
          U.overflows.slice(0, 2).reduce((W, te) => W + te, 0)
        ) : (
          // Check only the mainAxis.
          U.overflows[0]
        ), U.overflows];
      }).sort((U, X) => U[1] - X[1]), k = ((o = A.filter((U) => U[2].slice(
        0,
        // Aligned placements should not check their opposite crossAxis
        // side.
        we(U[0]) ? 2 : 3
      ).every((X) => X <= 0))[0]) == null ? void 0 : o[0]) || A[0][0];
      return k !== l ? {
        data: {
          index: f + 1,
          overflows: L
        },
        reset: {
          placement: k
        }
      } : {};
    }
  };
}, jr = function(e) {
  return e === void 0 && (e = {}), {
    name: "flip",
    options: e,
    async fn(t) {
      var n, s;
      const {
        placement: o,
        middlewareData: a,
        rects: r,
        initialPlacement: l,
        platform: i,
        elements: d
      } = t, {
        mainAxis: _ = !0,
        crossAxis: h = !0,
        fallbackPlacements: m,
        fallbackStrategy: b = "bestFit",
        fallbackAxisSideDirection: u = "none",
        flipAlignment: v = !0,
        ...y
      } = dt(e, t);
      if ((n = a.arrow) != null && n.alignmentOffset)
        return {};
      const f = Se(o), g = Se(l) === l, w = await (i.isRTL == null ? void 0 : i.isRTL(d.floating)), E = m || (g || !v ? [dn(l)] : Br(l));
      !m && u !== "none" && E.push(...xr(l, v, u, w));
      const L = [l, ...E], N = await vn(t, y), A = [];
      let D = ((s = a.flip) == null ? void 0 : s.overflows) || [];
      if (_ && A.push(N[f]), h) {
        const W = wo(o, r, w);
        A.push(N[W[0]], N[W[1]]);
      }
      if (D = [...D, {
        placement: o,
        overflows: A
      }], !A.every((W) => W <= 0)) {
        var k, U;
        const W = (((k = a.flip) == null ? void 0 : k.index) || 0) + 1, te = L[W];
        if (te)
          return {
            data: {
              index: W,
              overflows: D
            },
            reset: {
              placement: te
            }
          };
        let ne = (U = D.filter((ie) => ie.overflows[0] <= 0).sort((ie, J) => ie.overflows[1] - J.overflows[1])[0]) == null ? void 0 : U.placement;
        if (!ne)
          switch (b) {
            case "bestFit": {
              var X;
              const ie = (X = D.map((J) => [J.placement, J.overflows.filter((le) => le > 0).reduce((le, Ze) => le + Ze, 0)]).sort((J, le) => J[1] - le[1])[0]) == null ? void 0 : X[0];
              ie && (ne = ie);
              break;
            }
            case "initialPlacement":
              ne = l;
              break;
          }
        if (o !== ne)
          return {
            reset: {
              placement: ne
            }
          };
      }
      return {};
    }
  };
};
async function qr(e, t) {
  const {
    placement: n,
    platform: s,
    elements: o
  } = e, a = await (s.isRTL == null ? void 0 : s.isRTL(o.floating)), r = Se(n), l = we(n), i = qt(n) === "y", d = ["left", "top"].includes(r) ? -1 : 1, _ = a && i ? -1 : 1, h = dt(t, e);
  let {
    mainAxis: m,
    crossAxis: b,
    alignmentAxis: u
  } = typeof h == "number" ? {
    mainAxis: h,
    crossAxis: 0,
    alignmentAxis: null
  } : {
    mainAxis: 0,
    crossAxis: 0,
    alignmentAxis: null,
    ...h
  };
  return l && typeof u == "number" && (b = l === "end" ? u * -1 : u), i ? {
    x: b * _,
    y: m * d
  } : {
    x: m * d,
    y: b * _
  };
}
const Kr = function(e) {
  return e === void 0 && (e = 0), {
    name: "offset",
    options: e,
    async fn(t) {
      var n, s;
      const {
        x: o,
        y: a,
        placement: r,
        middlewareData: l
      } = t, i = await qr(t, e);
      return r === ((n = l.offset) == null ? void 0 : n.placement) && (s = l.arrow) != null && s.alignmentOffset ? {} : {
        x: o + i.x,
        y: a + i.y,
        data: {
          ...i,
          placement: r
        }
      };
    }
  };
}, Xr = function(e) {
  return e === void 0 && (e = {}), {
    name: "shift",
    options: e,
    async fn(t) {
      const {
        x: n,
        y: s,
        placement: o
      } = t, {
        mainAxis: a = !0,
        crossAxis: r = !1,
        limiter: l = {
          fn: (y) => {
            let {
              x: f,
              y: g
            } = y;
            return {
              x: f,
              y: g
            };
          }
        },
        ...i
      } = dt(e, t), d = {
        x: n,
        y: s
      }, _ = await vn(t, i), h = qt(Se(o)), m = To(h);
      let b = d[m], u = d[h];
      if (a) {
        const y = m === "y" ? "top" : "left", f = m === "y" ? "bottom" : "right", g = b + _[y], w = b - _[f];
        b = Pn(g, b, w);
      }
      if (r) {
        const y = h === "y" ? "top" : "left", f = h === "y" ? "bottom" : "right", g = u + _[y], w = u - _[f];
        u = Pn(g, u, w);
      }
      const v = l.fn({
        ...t,
        [m]: b,
        [h]: u
      });
      return {
        ...v,
        data: {
          x: v.x - n,
          y: v.y - s
        }
      };
    }
  };
}, Zr = function(e) {
  return e === void 0 && (e = {}), {
    name: "size",
    options: e,
    async fn(t) {
      const {
        placement: n,
        rects: s,
        platform: o,
        elements: a
      } = t, {
        apply: r = () => {
        },
        ...l
      } = dt(e, t), i = await vn(t, l), d = Se(n), _ = we(n), h = qt(n) === "y", {
        width: m,
        height: b
      } = s.floating;
      let u, v;
      d === "top" || d === "bottom" ? (u = d, v = _ === (await (o.isRTL == null ? void 0 : o.isRTL(a.floating)) ? "start" : "end") ? "left" : "right") : (v = d, u = _ === "end" ? "top" : "bottom");
      const y = b - i[u], f = m - i[v], g = !t.middlewareData.shift;
      let w = y, E = f;
      if (h) {
        const N = m - i.left - i.right;
        E = _ || g ? xt(f, N) : N;
      } else {
        const N = b - i.top - i.bottom;
        w = _ || g ? xt(y, N) : N;
      }
      if (g && !_) {
        const N = st(i.left, 0), A = st(i.right, 0), D = st(i.top, 0), k = st(i.bottom, 0);
        h ? E = m - 2 * (N !== 0 || A !== 0 ? N + A : st(i.left, i.right)) : w = b - 2 * (D !== 0 || k !== 0 ? D + k : st(i.top, i.bottom));
      }
      await r({
        ...t,
        availableWidth: E,
        availableHeight: w
      });
      const L = await o.getDimensions(a.floating);
      return m !== L.width || b !== L.height ? {
        reset: {
          rects: !0
        }
      } : {};
    }
  };
};
function ye(e) {
  var t;
  return ((t = e.ownerDocument) == null ? void 0 : t.defaultView) || window;
}
function ke(e) {
  return ye(e).getComputedStyle(e);
}
const vs = Math.min, Dt = Math.max, fn = Math.round;
function Lo(e) {
  const t = ke(e);
  let n = parseFloat(t.width), s = parseFloat(t.height);
  const o = e.offsetWidth, a = e.offsetHeight, r = fn(n) !== o || fn(s) !== a;
  return r && (n = o, s = a), { width: n, height: s, fallback: r };
}
function qe(e) {
  return Oo(e) ? (e.nodeName || "").toLowerCase() : "";
}
let Xt;
function No() {
  if (Xt)
    return Xt;
  const e = navigator.userAgentData;
  return e && Array.isArray(e.brands) ? (Xt = e.brands.map((t) => t.brand + "/" + t.version).join(" "), Xt) : navigator.userAgent;
}
function Ae(e) {
  return e instanceof ye(e).HTMLElement;
}
function ze(e) {
  return e instanceof ye(e).Element;
}
function Oo(e) {
  return e instanceof ye(e).Node;
}
function ys(e) {
  return typeof ShadowRoot > "u" ? !1 : e instanceof ye(e).ShadowRoot || e instanceof ShadowRoot;
}
function yn(e) {
  const { overflow: t, overflowX: n, overflowY: s, display: o } = ke(e);
  return /auto|scroll|overlay|hidden|clip/.test(t + s + n) && !["inline", "contents"].includes(o);
}
function Jr(e) {
  return ["table", "td", "th"].includes(qe(e));
}
function Mn(e) {
  const t = /firefox/i.test(No()), n = ke(e), s = n.backdropFilter || n.WebkitBackdropFilter;
  return n.transform !== "none" || n.perspective !== "none" || !!s && s !== "none" || t && n.willChange === "filter" || t && !!n.filter && n.filter !== "none" || ["transform", "perspective"].some((o) => n.willChange.includes(o)) || ["paint", "layout", "strict", "content"].some((o) => {
    const a = n.contain;
    return a != null && a.includes(o);
  });
}
function ko() {
  return !/^((?!chrome|android).)*safari/i.test(No());
}
function ts(e) {
  return ["html", "body", "#document"].includes(qe(e));
}
function Ao(e) {
  return ze(e) ? e : e.contextElement;
}
const $o = { x: 1, y: 1 };
function yt(e) {
  const t = Ao(e);
  if (!Ae(t))
    return $o;
  const n = t.getBoundingClientRect(), { width: s, height: o, fallback: a } = Lo(t);
  let r = (a ? fn(n.width) : n.width) / s, l = (a ? fn(n.height) : n.height) / o;
  return r && Number.isFinite(r) || (r = 1), l && Number.isFinite(l) || (l = 1), { x: r, y: l };
}
function Ht(e, t, n, s) {
  var o, a;
  t === void 0 && (t = !1), n === void 0 && (n = !1);
  const r = e.getBoundingClientRect(), l = Ao(e);
  let i = $o;
  t && (s ? ze(s) && (i = yt(s)) : i = yt(e));
  const d = l ? ye(l) : window, _ = !ko() && n;
  let h = (r.left + (_ && ((o = d.visualViewport) == null ? void 0 : o.offsetLeft) || 0)) / i.x, m = (r.top + (_ && ((a = d.visualViewport) == null ? void 0 : a.offsetTop) || 0)) / i.y, b = r.width / i.x, u = r.height / i.y;
  if (l) {
    const v = ye(l), y = s && ze(s) ? ye(s) : s;
    let f = v.frameElement;
    for (; f && s && y !== v; ) {
      const g = yt(f), w = f.getBoundingClientRect(), E = getComputedStyle(f);
      w.x += (f.clientLeft + parseFloat(E.paddingLeft)) * g.x, w.y += (f.clientTop + parseFloat(E.paddingTop)) * g.y, h *= g.x, m *= g.y, b *= g.x, u *= g.y, h += w.x, m += w.y, f = ye(f).frameElement;
    }
  }
  return { width: b, height: u, top: m, right: h + b, bottom: m + u, left: h, x: h, y: m };
}
function Ye(e) {
  return ((Oo(e) ? e.ownerDocument : e.document) || window.document).documentElement;
}
function En(e) {
  return ze(e) ? { scrollLeft: e.scrollLeft, scrollTop: e.scrollTop } : { scrollLeft: e.pageXOffset, scrollTop: e.pageYOffset };
}
function So(e) {
  return Ht(Ye(e)).left + En(e).scrollLeft;
}
function Wt(e) {
  if (qe(e) === "html")
    return e;
  const t = e.assignedSlot || e.parentNode || ys(e) && e.host || Ye(e);
  return ys(t) ? t.host : t;
}
function Io(e) {
  const t = Wt(e);
  return ts(t) ? t.ownerDocument.body : Ae(t) && yn(t) ? t : Io(t);
}
function pn(e, t) {
  var n;
  t === void 0 && (t = []);
  const s = Io(e), o = s === ((n = e.ownerDocument) == null ? void 0 : n.body), a = ye(s);
  return o ? t.concat(a, a.visualViewport || [], yn(s) ? s : []) : t.concat(s, pn(s));
}
function Es(e, t, n) {
  return t === "viewport" ? Rt(function(s, o) {
    const a = ye(s), r = Ye(s), l = a.visualViewport;
    let i = r.clientWidth, d = r.clientHeight, _ = 0, h = 0;
    if (l) {
      i = l.width, d = l.height;
      const m = ko();
      (m || !m && o === "fixed") && (_ = l.offsetLeft, h = l.offsetTop);
    }
    return { width: i, height: d, x: _, y: h };
  }(e, n)) : ze(t) ? Rt(function(s, o) {
    const a = Ht(s, !0, o === "fixed"), r = a.top + s.clientTop, l = a.left + s.clientLeft, i = Ae(s) ? yt(s) : { x: 1, y: 1 };
    return { width: s.clientWidth * i.x, height: s.clientHeight * i.y, x: l * i.x, y: r * i.y };
  }(t, n)) : Rt(function(s) {
    const o = Ye(s), a = En(s), r = s.ownerDocument.body, l = Dt(o.scrollWidth, o.clientWidth, r.scrollWidth, r.clientWidth), i = Dt(o.scrollHeight, o.clientHeight, r.scrollHeight, r.clientHeight);
    let d = -a.scrollLeft + So(s);
    const _ = -a.scrollTop;
    return ke(r).direction === "rtl" && (d += Dt(o.clientWidth, r.clientWidth) - l), { width: l, height: i, x: d, y: _ };
  }(Ye(e)));
}
function bs(e) {
  return Ae(e) && ke(e).position !== "fixed" ? e.offsetParent : null;
}
function Ts(e) {
  const t = ye(e);
  let n = bs(e);
  for (; n && Jr(n) && ke(n).position === "static"; )
    n = bs(n);
  return n && (qe(n) === "html" || qe(n) === "body" && ke(n).position === "static" && !Mn(n)) ? t : n || function(s) {
    let o = Wt(s);
    for (; Ae(o) && !ts(o); ) {
      if (Mn(o))
        return o;
      o = Wt(o);
    }
    return null;
  }(e) || t;
}
function Qr(e, t, n) {
  const s = Ae(t), o = Ye(t), a = Ht(e, !0, n === "fixed", t);
  let r = { scrollLeft: 0, scrollTop: 0 };
  const l = { x: 0, y: 0 };
  if (s || !s && n !== "fixed")
    if ((qe(t) !== "body" || yn(o)) && (r = En(t)), Ae(t)) {
      const i = Ht(t, !0);
      l.x = i.x + t.clientLeft, l.y = i.y + t.clientTop;
    } else
      o && (l.x = So(o));
  return { x: a.left + r.scrollLeft - l.x, y: a.top + r.scrollTop - l.y, width: a.width, height: a.height };
}
const ei = { getClippingRect: function(e) {
  let { element: t, boundary: n, rootBoundary: s, strategy: o } = e;
  const a = n === "clippingAncestors" ? function(d, _) {
    const h = _.get(d);
    if (h)
      return h;
    let m = pn(d).filter((y) => ze(y) && qe(y) !== "body"), b = null;
    const u = ke(d).position === "fixed";
    let v = u ? Wt(d) : d;
    for (; ze(v) && !ts(v); ) {
      const y = ke(v), f = Mn(v);
      (u ? f || b : f || y.position !== "static" || !b || !["absolute", "fixed"].includes(b.position)) ? b = y : m = m.filter((g) => g !== v), v = Wt(v);
    }
    return _.set(d, m), m;
  }(t, this._c) : [].concat(n), r = [...a, s], l = r[0], i = r.reduce((d, _) => {
    const h = Es(t, _, o);
    return d.top = Dt(h.top, d.top), d.right = vs(h.right, d.right), d.bottom = vs(h.bottom, d.bottom), d.left = Dt(h.left, d.left), d;
  }, Es(t, l, o));
  return { width: i.right - i.left, height: i.bottom - i.top, x: i.left, y: i.top };
}, convertOffsetParentRelativeRectToViewportRelativeRect: function(e) {
  let { rect: t, offsetParent: n, strategy: s } = e;
  const o = Ae(n), a = Ye(n);
  if (n === a)
    return t;
  let r = { scrollLeft: 0, scrollTop: 0 }, l = { x: 1, y: 1 };
  const i = { x: 0, y: 0 };
  if ((o || !o && s !== "fixed") && ((qe(n) !== "body" || yn(a)) && (r = En(n)), Ae(n))) {
    const d = Ht(n);
    l = yt(n), i.x = d.x + n.clientLeft, i.y = d.y + n.clientTop;
  }
  return { width: t.width * l.x, height: t.height * l.y, x: t.x * l.x - r.scrollLeft * l.x + i.x, y: t.y * l.y - r.scrollTop * l.y + i.y };
}, isElement: ze, getDimensions: function(e) {
  return Ae(e) ? Lo(e) : e.getBoundingClientRect();
}, getOffsetParent: Ts, getDocumentElement: Ye, getScale: yt, async getElementRects(e) {
  let { reference: t, floating: n, strategy: s } = e;
  const o = this.getOffsetParent || Ts, a = this.getDimensions;
  return { reference: Qr(t, await o(n), s), floating: { x: 0, y: 0, ...await a(n) } };
}, getClientRects: (e) => Array.from(e.getClientRects()), isRTL: (e) => ke(e).direction === "rtl" }, ti = (e, t, n) => {
  const s = /* @__PURE__ */ new Map(), o = { platform: ei, ...n }, a = { ...o.platform, _c: s };
  return Wr(e, t, { ...o, platform: a });
}, at = {
  // Disable popper components
  disabled: !1,
  // Default position offset along main axis (px)
  distance: 5,
  // Default position offset along cross axis (px)
  skidding: 0,
  // Default container where the tooltip will be appended
  container: "body",
  // Element used to compute position and size boundaries
  boundary: void 0,
  // Skip delay & CSS transitions when another popper is shown, so that the popper appear to instanly move to the new position.
  instantMove: !1,
  // Auto destroy tooltip DOM nodes (ms)
  disposeTimeout: 150,
  // Triggers on the popper itself
  popperTriggers: [],
  // Positioning strategy
  strategy: "absolute",
  // Prevent overflow
  preventOverflow: !0,
  // Flip to the opposite placement if needed
  flip: !0,
  // Shift on the cross axis to prevent the popper from overflowing
  shift: !0,
  // Overflow padding (px)
  overflowPadding: 0,
  // Arrow padding (px)
  arrowPadding: 0,
  // Compute arrow overflow (useful to hide it)
  arrowOverflow: !0,
  /**
   * By default, compute autohide on 'click'.
   */
  autoHideOnMousedown: !1,
  // Themes
  themes: {
    tooltip: {
      // Default tooltip placement relative to target element
      placement: "top",
      // Default events that trigger the tooltip
      triggers: ["hover", "focus", "touch"],
      // Close tooltip on click on tooltip target
      hideTriggers: (e) => [...e, "click"],
      // Delay (ms)
      delay: {
        show: 200,
        hide: 0
      },
      // Update popper on content resize
      handleResize: !1,
      // Enable HTML content in directive
      html: !1,
      // Displayed when tooltip content is loading
      loadingContent: "..."
    },
    dropdown: {
      // Default dropdown placement relative to target element
      placement: "bottom",
      // Default events that trigger the dropdown
      triggers: ["click"],
      // Delay (ms)
      delay: 0,
      // Update popper on content resize
      handleResize: !0,
      // Hide on clock outside
      autoHide: !0
    },
    menu: {
      $extend: "dropdown",
      triggers: ["hover", "focus"],
      popperTriggers: ["hover"],
      delay: {
        show: 0,
        hide: 400
      }
    }
  }
};
function Rn(e, t) {
  let n = at.themes[e] || {}, s;
  do
    s = n[t], typeof s > "u" ? n.$extend ? n = at.themes[n.$extend] || {} : (n = null, s = at[t]) : n = null;
  while (n);
  return s;
}
function ni(e) {
  const t = [e];
  let n = at.themes[e] || {};
  do
    n.$extend && !n.$resetCss ? (t.push(n.$extend), n = at.themes[n.$extend] || {}) : n = null;
  while (n);
  return t.map((s) => `v-popper--theme-${s}`);
}
function ws(e) {
  const t = [e];
  let n = at.themes[e] || {};
  do
    n.$extend ? (t.push(n.$extend), n = at.themes[n.$extend] || {}) : n = null;
  while (n);
  return t;
}
let Gt = !1;
if (typeof window < "u") {
  Gt = !1;
  try {
    const e = Object.defineProperty({}, "passive", {
      get() {
        Gt = !0;
      }
    });
    window.addEventListener("test", null, e);
  } catch {
  }
}
let Po = !1;
typeof window < "u" && typeof navigator < "u" && (Po = /iPad|iPhone|iPod/.test(navigator.userAgent) && !window.MSStream);
const si = ["auto", "top", "bottom", "left", "right"].reduce((e, t) => e.concat([
  t,
  `${t}-start`,
  `${t}-end`
]), []), Cs = {
  hover: "mouseenter",
  focus: "focus",
  click: "click",
  touch: "touchstart",
  pointer: "pointerdown"
}, Ls = {
  hover: "mouseleave",
  focus: "blur",
  click: "click",
  touch: "touchend",
  pointer: "pointerup"
};
function Ns(e, t) {
  const n = e.indexOf(t);
  n !== -1 && e.splice(n, 1);
}
function On() {
  return new Promise((e) => requestAnimationFrame(() => {
    requestAnimationFrame(e);
  }));
}
const Te = [];
let tt = null;
const Os = {};
function ks(e) {
  let t = Os[e];
  return t || (t = Os[e] = []), t;
}
let Dn = function() {
};
typeof window < "u" && (Dn = window.Element);
function H(e) {
  return function(t) {
    return Rn(t.theme, e);
  };
}
const kn = "__floating-vue__popper", Mo = () => ae({
  name: "VPopper",
  provide() {
    return {
      [kn]: {
        parentPopper: this
      }
    };
  },
  inject: {
    [kn]: { default: null }
  },
  props: {
    theme: {
      type: String,
      required: !0
    },
    targetNodes: {
      type: Function,
      required: !0
    },
    referenceNode: {
      type: Function,
      default: null
    },
    popperNode: {
      type: Function,
      required: !0
    },
    shown: {
      type: Boolean,
      default: !1
    },
    showGroup: {
      type: String,
      default: null
    },
    // eslint-disable-next-line vue/require-prop-types
    ariaId: {
      default: null
    },
    disabled: {
      type: Boolean,
      default: H("disabled")
    },
    positioningDisabled: {
      type: Boolean,
      default: H("positioningDisabled")
    },
    placement: {
      type: String,
      default: H("placement"),
      validator: (e) => si.includes(e)
    },
    delay: {
      type: [String, Number, Object],
      default: H("delay")
    },
    distance: {
      type: [Number, String],
      default: H("distance")
    },
    skidding: {
      type: [Number, String],
      default: H("skidding")
    },
    triggers: {
      type: Array,
      default: H("triggers")
    },
    showTriggers: {
      type: [Array, Function],
      default: H("showTriggers")
    },
    hideTriggers: {
      type: [Array, Function],
      default: H("hideTriggers")
    },
    popperTriggers: {
      type: Array,
      default: H("popperTriggers")
    },
    popperShowTriggers: {
      type: [Array, Function],
      default: H("popperShowTriggers")
    },
    popperHideTriggers: {
      type: [Array, Function],
      default: H("popperHideTriggers")
    },
    container: {
      type: [String, Object, Dn, Boolean],
      default: H("container")
    },
    boundary: {
      type: [String, Dn],
      default: H("boundary")
    },
    strategy: {
      type: String,
      validator: (e) => ["absolute", "fixed"].includes(e),
      default: H("strategy")
    },
    autoHide: {
      type: [Boolean, Function],
      default: H("autoHide")
    },
    handleResize: {
      type: Boolean,
      default: H("handleResize")
    },
    instantMove: {
      type: Boolean,
      default: H("instantMove")
    },
    eagerMount: {
      type: Boolean,
      default: H("eagerMount")
    },
    popperClass: {
      type: [String, Array, Object],
      default: H("popperClass")
    },
    computeTransformOrigin: {
      type: Boolean,
      default: H("computeTransformOrigin")
    },
    /**
     * @deprecated
     */
    autoMinSize: {
      type: Boolean,
      default: H("autoMinSize")
    },
    autoSize: {
      type: [Boolean, String],
      default: H("autoSize")
    },
    /**
     * @deprecated
     */
    autoMaxSize: {
      type: Boolean,
      default: H("autoMaxSize")
    },
    autoBoundaryMaxSize: {
      type: Boolean,
      default: H("autoBoundaryMaxSize")
    },
    preventOverflow: {
      type: Boolean,
      default: H("preventOverflow")
    },
    overflowPadding: {
      type: [Number, String],
      default: H("overflowPadding")
    },
    arrowPadding: {
      type: [Number, String],
      default: H("arrowPadding")
    },
    arrowOverflow: {
      type: Boolean,
      default: H("arrowOverflow")
    },
    flip: {
      type: Boolean,
      default: H("flip")
    },
    shift: {
      type: Boolean,
      default: H("shift")
    },
    shiftCrossAxis: {
      type: Boolean,
      default: H("shiftCrossAxis")
    },
    noAutoFocus: {
      type: Boolean,
      default: H("noAutoFocus")
    },
    disposeTimeout: {
      type: Number,
      default: H("disposeTimeout")
    }
  },
  emits: {
    show: () => !0,
    hide: () => !0,
    "update:shown": (e) => !0,
    "apply-show": () => !0,
    "apply-hide": () => !0,
    "close-group": () => !0,
    "close-directive": () => !0,
    "auto-hide": () => !0,
    resize: () => !0
  },
  data() {
    return {
      isShown: !1,
      isMounted: !1,
      skipTransition: !1,
      classes: {
        showFrom: !1,
        showTo: !1,
        hideFrom: !1,
        hideTo: !0
      },
      result: {
        x: 0,
        y: 0,
        placement: "",
        strategy: this.strategy,
        arrow: {
          x: 0,
          y: 0,
          centerOffset: 0
        },
        transformOrigin: null
      },
      randomId: `popper_${[Math.random(), Date.now()].map((e) => e.toString(36).substring(2, 10)).join("_")}`,
      shownChildren: /* @__PURE__ */ new Set(),
      lastAutoHide: !0,
      pendingHide: !1,
      containsGlobalTarget: !1,
      isDisposed: !0,
      mouseDownContains: !1
    };
  },
  computed: {
    popperId() {
      return this.ariaId != null ? this.ariaId : this.randomId;
    },
    shouldMountContent() {
      return this.eagerMount || this.isMounted;
    },
    slotData() {
      return {
        popperId: this.popperId,
        isShown: this.isShown,
        shouldMountContent: this.shouldMountContent,
        skipTransition: this.skipTransition,
        autoHide: typeof this.autoHide == "function" ? this.lastAutoHide : this.autoHide,
        show: this.show,
        hide: this.hide,
        handleResize: this.handleResize,
        onResize: this.onResize,
        classes: {
          ...this.classes,
          popperClass: this.popperClass
        },
        result: this.positioningDisabled ? null : this.result,
        attrs: this.$attrs
      };
    },
    parentPopper() {
      var e;
      return (e = this[kn]) == null ? void 0 : e.parentPopper;
    },
    hasPopperShowTriggerHover() {
      var e, t;
      return ((e = this.popperTriggers) == null ? void 0 : e.includes("hover")) || ((t = this.popperShowTriggers) == null ? void 0 : t.includes("hover"));
    }
  },
  watch: {
    shown: "$_autoShowHide",
    disabled(e) {
      e ? this.dispose() : this.init();
    },
    async container() {
      this.isShown && (this.$_ensureTeleport(), await this.$_computePosition());
    },
    triggers: {
      handler: "$_refreshListeners",
      deep: !0
    },
    positioningDisabled: "$_refreshListeners",
    ...[
      "placement",
      "distance",
      "skidding",
      "boundary",
      "strategy",
      "overflowPadding",
      "arrowPadding",
      "preventOverflow",
      "shift",
      "shiftCrossAxis",
      "flip"
    ].reduce((e, t) => (e[t] = "$_computePosition", e), {})
  },
  created() {
    this.autoMinSize && console.warn('[floating-vue] `autoMinSize` option is deprecated. Use `autoSize="min"` instead.'), this.autoMaxSize && console.warn("[floating-vue] `autoMaxSize` option is deprecated. Use `autoBoundaryMaxSize` instead.");
  },
  mounted() {
    this.init(), this.$_detachPopperNode();
  },
  activated() {
    this.$_autoShowHide();
  },
  deactivated() {
    this.hide();
  },
  beforeUnmount() {
    this.dispose();
  },
  methods: {
    show({ event: e = null, skipDelay: t = !1, force: n = !1 } = {}) {
      var s, o;
      (s = this.parentPopper) != null && s.lockedChild && this.parentPopper.lockedChild !== this || (this.pendingHide = !1, (n || !this.disabled) && (((o = this.parentPopper) == null ? void 0 : o.lockedChild) === this && (this.parentPopper.lockedChild = null), this.$_scheduleShow(e, t), this.$emit("show"), this.$_showFrameLocked = !0, requestAnimationFrame(() => {
        this.$_showFrameLocked = !1;
      })), this.$emit("update:shown", !0));
    },
    hide({ event: e = null, skipDelay: t = !1 } = {}) {
      var n;
      if (!this.$_hideInProgress) {
        if (this.shownChildren.size > 0) {
          this.pendingHide = !0;
          return;
        }
        if (this.hasPopperShowTriggerHover && this.$_isAimingPopper()) {
          this.parentPopper && (this.parentPopper.lockedChild = this, clearTimeout(this.parentPopper.lockedChildTimer), this.parentPopper.lockedChildTimer = setTimeout(() => {
            this.parentPopper.lockedChild === this && (this.parentPopper.lockedChild.hide({ skipDelay: t }), this.parentPopper.lockedChild = null);
          }, 1e3));
          return;
        }
        ((n = this.parentPopper) == null ? void 0 : n.lockedChild) === this && (this.parentPopper.lockedChild = null), this.pendingHide = !1, this.$_scheduleHide(e, t), this.$emit("hide"), this.$emit("update:shown", !1);
      }
    },
    init() {
      var e;
      this.isDisposed && (this.isDisposed = !1, this.isMounted = !1, this.$_events = [], this.$_preventShow = !1, this.$_referenceNode = ((e = this.referenceNode) == null ? void 0 : e.call(this)) ?? this.$el, this.$_targetNodes = this.targetNodes().filter((t) => t.nodeType === t.ELEMENT_NODE), this.$_popperNode = this.popperNode(), this.$_innerNode = this.$_popperNode.querySelector(".v-popper__inner"), this.$_arrowNode = this.$_popperNode.querySelector(".v-popper__arrow-container"), this.$_swapTargetAttrs("title", "data-original-title"), this.$_detachPopperNode(), this.triggers.length && this.$_addEventListeners(), this.shown && this.show());
    },
    dispose() {
      this.isDisposed || (this.isDisposed = !0, this.$_removeEventListeners(), this.hide({ skipDelay: !0 }), this.$_detachPopperNode(), this.isMounted = !1, this.isShown = !1, this.$_updateParentShownChildren(!1), this.$_swapTargetAttrs("data-original-title", "title"));
    },
    async onResize() {
      this.isShown && (await this.$_computePosition(), this.$emit("resize"));
    },
    async $_computePosition() {
      if (this.isDisposed || this.positioningDisabled)
        return;
      const e = {
        strategy: this.strategy,
        middleware: []
      };
      (this.distance || this.skidding) && e.middleware.push(Kr({
        mainAxis: this.distance,
        crossAxis: this.skidding
      }));
      const t = this.placement.startsWith("auto");
      if (t ? e.middleware.push(Yr({
        alignment: this.placement.split("-")[1] ?? ""
      })) : e.placement = this.placement, this.preventOverflow && (this.shift && e.middleware.push(Xr({
        padding: this.overflowPadding,
        boundary: this.boundary,
        crossAxis: this.shiftCrossAxis
      })), !t && this.flip && e.middleware.push(jr({
        padding: this.overflowPadding,
        boundary: this.boundary
      }))), e.middleware.push(Gr({
        element: this.$_arrowNode,
        padding: this.arrowPadding
      })), this.arrowOverflow && e.middleware.push({
        name: "arrowOverflow",
        fn: ({ placement: s, rects: o, middlewareData: a }) => {
          let r;
          const { centerOffset: l } = a.arrow;
          return s.startsWith("top") || s.startsWith("bottom") ? r = Math.abs(l) > o.reference.width / 2 : r = Math.abs(l) > o.reference.height / 2, {
            data: {
              overflow: r
            }
          };
        }
      }), this.autoMinSize || this.autoSize) {
        const s = this.autoSize ? this.autoSize : this.autoMinSize ? "min" : null;
        e.middleware.push({
          name: "autoSize",
          fn: ({ rects: o, placement: a, middlewareData: r }) => {
            var l;
            if ((l = r.autoSize) != null && l.skip)
              return {};
            let i, d;
            return a.startsWith("top") || a.startsWith("bottom") ? i = o.reference.width : d = o.reference.height, this.$_innerNode.style[s === "min" ? "minWidth" : s === "max" ? "maxWidth" : "width"] = i != null ? `${i}px` : null, this.$_innerNode.style[s === "min" ? "minHeight" : s === "max" ? "maxHeight" : "height"] = d != null ? `${d}px` : null, {
              data: {
                skip: !0
              },
              reset: {
                rects: !0
              }
            };
          }
        });
      }
      (this.autoMaxSize || this.autoBoundaryMaxSize) && (this.$_innerNode.style.maxWidth = null, this.$_innerNode.style.maxHeight = null, e.middleware.push(Zr({
        boundary: this.boundary,
        padding: this.overflowPadding,
        apply: ({ availableWidth: s, availableHeight: o }) => {
          this.$_innerNode.style.maxWidth = s != null ? `${s}px` : null, this.$_innerNode.style.maxHeight = o != null ? `${o}px` : null;
        }
      })));
      const n = await ti(this.$_referenceNode, this.$_popperNode, e);
      Object.assign(this.result, {
        x: n.x,
        y: n.y,
        placement: n.placement,
        strategy: n.strategy,
        arrow: {
          ...n.middlewareData.arrow,
          ...n.middlewareData.arrowOverflow
        }
      });
    },
    $_scheduleShow(e, t = !1) {
      if (this.$_updateParentShownChildren(!0), this.$_hideInProgress = !1, clearTimeout(this.$_scheduleTimer), tt && this.instantMove && tt.instantMove && tt !== this.parentPopper) {
        tt.$_applyHide(!0), this.$_applyShow(!0);
        return;
      }
      t ? this.$_applyShow() : this.$_scheduleTimer = setTimeout(this.$_applyShow.bind(this), this.$_computeDelay("show"));
    },
    $_scheduleHide(e, t = !1) {
      if (this.shownChildren.size > 0) {
        this.pendingHide = !0;
        return;
      }
      this.$_updateParentShownChildren(!1), this.$_hideInProgress = !0, clearTimeout(this.$_scheduleTimer), this.isShown && (tt = this), t ? this.$_applyHide() : this.$_scheduleTimer = setTimeout(this.$_applyHide.bind(this), this.$_computeDelay("hide"));
    },
    $_computeDelay(e) {
      const t = this.delay;
      return parseInt(t && t[e] || t || 0);
    },
    async $_applyShow(e = !1) {
      clearTimeout(this.$_disposeTimer), clearTimeout(this.$_scheduleTimer), this.skipTransition = e, !this.isShown && (this.$_ensureTeleport(), await On(), await this.$_computePosition(), await this.$_applyShowEffect(), this.positioningDisabled || this.$_registerEventListeners([
        ...pn(this.$_referenceNode),
        ...pn(this.$_popperNode)
      ], "scroll", () => {
        this.$_computePosition();
      }));
    },
    async $_applyShowEffect() {
      if (this.$_hideInProgress)
        return;
      if (this.computeTransformOrigin) {
        const t = this.$_referenceNode.getBoundingClientRect(), n = this.$_popperNode.querySelector(".v-popper__wrapper"), s = n.parentNode.getBoundingClientRect(), o = t.x + t.width / 2 - (s.left + n.offsetLeft), a = t.y + t.height / 2 - (s.top + n.offsetTop);
        this.result.transformOrigin = `${o}px ${a}px`;
      }
      this.isShown = !0, this.$_applyAttrsToTarget({
        "aria-describedby": this.popperId,
        "data-popper-shown": ""
      });
      const e = this.showGroup;
      if (e) {
        let t;
        for (let n = 0; n < Te.length; n++)
          t = Te[n], t.showGroup !== e && (t.hide(), t.$emit("close-group"));
      }
      Te.push(this), document.body.classList.add("v-popper--some-open");
      for (const t of ws(this.theme))
        ks(t).push(this), document.body.classList.add(`v-popper--some-open--${t}`);
      this.$emit("apply-show"), this.classes.showFrom = !0, this.classes.showTo = !1, this.classes.hideFrom = !1, this.classes.hideTo = !1, await On(), this.classes.showFrom = !1, this.classes.showTo = !0, this.noAutoFocus || this.$_popperNode.focus();
    },
    async $_applyHide(e = !1) {
      if (this.shownChildren.size > 0) {
        this.pendingHide = !0, this.$_hideInProgress = !1;
        return;
      }
      if (clearTimeout(this.$_scheduleTimer), !this.isShown)
        return;
      this.skipTransition = e, Ns(Te, this), Te.length === 0 && document.body.classList.remove("v-popper--some-open");
      for (const n of ws(this.theme)) {
        const s = ks(n);
        Ns(s, this), s.length === 0 && document.body.classList.remove(`v-popper--some-open--${n}`);
      }
      tt === this && (tt = null), this.isShown = !1, this.$_applyAttrsToTarget({
        "aria-describedby": void 0,
        "data-popper-shown": void 0
      }), clearTimeout(this.$_disposeTimer);
      const t = this.disposeTimeout;
      t !== null && (this.$_disposeTimer = setTimeout(() => {
        this.$_popperNode && (this.$_detachPopperNode(), this.isMounted = !1);
      }, t)), this.$_removeEventListeners("scroll"), this.$emit("apply-hide"), this.classes.showFrom = !1, this.classes.showTo = !1, this.classes.hideFrom = !0, this.classes.hideTo = !1, await On(), this.classes.hideFrom = !1, this.classes.hideTo = !0;
    },
    $_autoShowHide() {
      this.shown ? this.show() : this.hide();
    },
    $_ensureTeleport() {
      if (this.isDisposed)
        return;
      let e = this.container;
      if (typeof e == "string" ? e = window.document.querySelector(e) : e === !1 && (e = this.$_targetNodes[0].parentNode), !e)
        throw new Error("No container for popover: " + this.container);
      e.appendChild(this.$_popperNode), this.isMounted = !0;
    },
    $_addEventListeners() {
      const e = (n) => {
        this.isShown && !this.$_hideInProgress || (n.usedByTooltip = !0, !this.$_preventShow && this.show({ event: n }));
      };
      this.$_registerTriggerListeners(this.$_targetNodes, Cs, this.triggers, this.showTriggers, e), this.$_registerTriggerListeners([this.$_popperNode], Cs, this.popperTriggers, this.popperShowTriggers, e);
      const t = (n) => {
        n.usedByTooltip || this.hide({ event: n });
      };
      this.$_registerTriggerListeners(this.$_targetNodes, Ls, this.triggers, this.hideTriggers, t), this.$_registerTriggerListeners([this.$_popperNode], Ls, this.popperTriggers, this.popperHideTriggers, t);
    },
    $_registerEventListeners(e, t, n) {
      this.$_events.push({ targetNodes: e, eventType: t, handler: n }), e.forEach((s) => s.addEventListener(t, n, Gt ? {
        passive: !0
      } : void 0));
    },
    $_registerTriggerListeners(e, t, n, s, o) {
      let a = n;
      s != null && (a = typeof s == "function" ? s(a) : s), a.forEach((r) => {
        const l = t[r];
        l && this.$_registerEventListeners(e, l, o);
      });
    },
    $_removeEventListeners(e) {
      const t = [];
      this.$_events.forEach((n) => {
        const { targetNodes: s, eventType: o, handler: a } = n;
        !e || e === o ? s.forEach((r) => r.removeEventListener(o, a)) : t.push(n);
      }), this.$_events = t;
    },
    $_refreshListeners() {
      this.isDisposed || (this.$_removeEventListeners(), this.$_addEventListeners());
    },
    $_handleGlobalClose(e, t = !1) {
      this.$_showFrameLocked || (this.hide({ event: e }), e.closePopover ? this.$emit("close-directive") : this.$emit("auto-hide"), t && (this.$_preventShow = !0, setTimeout(() => {
        this.$_preventShow = !1;
      }, 300)));
    },
    $_detachPopperNode() {
      this.$_popperNode.parentNode && this.$_popperNode.parentNode.removeChild(this.$_popperNode);
    },
    $_swapTargetAttrs(e, t) {
      for (const n of this.$_targetNodes) {
        const s = n.getAttribute(e);
        s && (n.removeAttribute(e), n.setAttribute(t, s));
      }
    },
    $_applyAttrsToTarget(e) {
      for (const t of this.$_targetNodes)
        for (const n in e) {
          const s = e[n];
          s == null ? t.removeAttribute(n) : t.setAttribute(n, s);
        }
    },
    $_updateParentShownChildren(e) {
      let t = this.parentPopper;
      for (; t; )
        e ? t.shownChildren.add(this.randomId) : (t.shownChildren.delete(this.randomId), t.pendingHide && t.hide()), t = t.parentPopper;
    },
    $_isAimingPopper() {
      const e = this.$_referenceNode.getBoundingClientRect();
      if (Ft >= e.left && Ft <= e.right && Vt >= e.top && Vt <= e.bottom) {
        const t = this.$_popperNode.getBoundingClientRect(), n = Ft - We, s = Vt - Ge, o = t.left + t.width / 2 - We + (t.top + t.height / 2) - Ge + t.width + t.height, a = We + n * o, r = Ge + s * o;
        return Zt(We, Ge, a, r, t.left, t.top, t.left, t.bottom) || // Left edge
        Zt(We, Ge, a, r, t.left, t.top, t.right, t.top) || // Top edge
        Zt(We, Ge, a, r, t.right, t.top, t.right, t.bottom) || // Right edge
        Zt(We, Ge, a, r, t.left, t.bottom, t.right, t.bottom);
      }
      return !1;
    }
  },
  render() {
    return this.$slots.default(this.slotData);
  }
});
if (typeof document < "u" && typeof window < "u") {
  if (Po) {
    const e = Gt ? {
      passive: !0,
      capture: !0
    } : !0;
    document.addEventListener("touchstart", (t) => As(t), e), document.addEventListener("touchend", (t) => $s(t, !0), e);
  } else
    window.addEventListener("mousedown", (e) => As(e), !0), window.addEventListener("click", (e) => $s(e, !1), !0);
  window.addEventListener("resize", ri);
}
function As(e, t) {
  for (let n = 0; n < Te.length; n++) {
    const s = Te[n];
    try {
      s.mouseDownContains = s.popperNode().contains(e.target);
    } catch {
    }
  }
}
function $s(e, t) {
  oi(e, t);
}
function oi(e, t) {
  const n = {};
  for (let s = Te.length - 1; s >= 0; s--) {
    const o = Te[s];
    try {
      const a = o.containsGlobalTarget = o.mouseDownContains || o.popperNode().contains(e.target);
      o.pendingHide = !1, requestAnimationFrame(() => {
        if (o.pendingHide = !1, !n[o.randomId] && Ss(o, a, e)) {
          if (o.$_handleGlobalClose(e, t), !e.closeAllPopover && e.closePopover && a) {
            let l = o.parentPopper;
            for (; l; )
              n[l.randomId] = !0, l = l.parentPopper;
            return;
          }
          let r = o.parentPopper;
          for (; r && Ss(r, r.containsGlobalTarget, e); )
            r.$_handleGlobalClose(e, t), r = r.parentPopper;
        }
      });
    } catch {
    }
  }
}
function Ss(e, t, n) {
  return n.closeAllPopover || n.closePopover && t || ai(e, n) && !t;
}
function ai(e, t) {
  if (typeof e.autoHide == "function") {
    const n = e.autoHide(t);
    return e.lastAutoHide = n, n;
  }
  return e.autoHide;
}
function ri() {
  for (let e = 0; e < Te.length; e++)
    Te[e].$_computePosition();
}
let We = 0, Ge = 0, Ft = 0, Vt = 0;
typeof window < "u" && window.addEventListener("mousemove", (e) => {
  We = Ft, Ge = Vt, Ft = e.clientX, Vt = e.clientY;
}, Gt ? {
  passive: !0
} : void 0);
function Zt(e, t, n, s, o, a, r, l) {
  const i = ((r - o) * (t - a) - (l - a) * (e - o)) / ((l - a) * (n - e) - (r - o) * (s - t)), d = ((n - e) * (t - a) - (s - t) * (e - o)) / ((l - a) * (n - e) - (r - o) * (s - t));
  return i >= 0 && i <= 1 && d >= 0 && d <= 1;
}
const ii = {
  extends: Mo()
}, ns = (e, t) => {
  const n = e.__vccOpts || e;
  for (const [s, o] of t)
    n[s] = o;
  return n;
};
function li(e, t, n, s, o, a) {
  return S(), P("div", {
    ref: "reference",
    class: R(["v-popper", {
      "v-popper--shown": e.slotData.isShown
    }])
  }, [
    ee(e.$slots, "default", La(Na(e.slotData)))
  ], 2);
}
const ui = /* @__PURE__ */ ns(ii, [["render", li]]);
function ci() {
  var e = window.navigator.userAgent, t = e.indexOf("MSIE ");
  if (t > 0)
    return parseInt(e.substring(t + 5, e.indexOf(".", t)), 10);
  var n = e.indexOf("Trident/");
  if (n > 0) {
    var s = e.indexOf("rv:");
    return parseInt(e.substring(s + 3, e.indexOf(".", s)), 10);
  }
  var o = e.indexOf("Edge/");
  return o > 0 ? parseInt(e.substring(o + 5, e.indexOf(".", o)), 10) : -1;
}
let tn;
function Fn() {
  Fn.init || (Fn.init = !0, tn = ci() !== -1);
}
var bn = {
  name: "ResizeObserver",
  props: {
    emitOnMount: {
      type: Boolean,
      default: !1
    },
    ignoreWidth: {
      type: Boolean,
      default: !1
    },
    ignoreHeight: {
      type: Boolean,
      default: !1
    }
  },
  emits: [
    "notify"
  ],
  mounted() {
    Fn(), mo(() => {
      this._w = this.$el.offsetWidth, this._h = this.$el.offsetHeight, this.emitOnMount && this.emitSize();
    });
    const e = document.createElement("object");
    this._resizeObject = e, e.setAttribute("aria-hidden", "true"), e.setAttribute("tabindex", -1), e.onload = this.addResizeHandlers, e.type = "text/html", tn && this.$el.appendChild(e), e.data = "about:blank", tn || this.$el.appendChild(e);
  },
  beforeUnmount() {
    this.removeResizeHandlers();
  },
  methods: {
    compareAndNotify() {
      (!this.ignoreWidth && this._w !== this.$el.offsetWidth || !this.ignoreHeight && this._h !== this.$el.offsetHeight) && (this._w = this.$el.offsetWidth, this._h = this.$el.offsetHeight, this.emitSize());
    },
    emitSize() {
      this.$emit("notify", {
        width: this._w,
        height: this._h
      });
    },
    addResizeHandlers() {
      this._resizeObject.contentDocument.defaultView.addEventListener("resize", this.compareAndNotify), this.compareAndNotify();
    },
    removeResizeHandlers() {
      this._resizeObject && this._resizeObject.onload && (!tn && this._resizeObject.contentDocument && this._resizeObject.contentDocument.defaultView.removeEventListener("resize", this.compareAndNotify), this.$el.removeChild(this._resizeObject), this._resizeObject.onload = null, this._resizeObject = null);
    }
  }
};
const di = /* @__PURE__ */ Oa("data-v-b329ee4c");
wa("data-v-b329ee4c");
const fi = {
  class: "resize-observer",
  tabindex: "-1"
};
Ca();
const pi = /* @__PURE__ */ di((e, t, n, s, o, a) => (S(), $e("div", fi)));
bn.render = pi;
bn.__scopeId = "data-v-b329ee4c";
bn.__file = "src/components/ResizeObserver.vue";
const Ro = (e = "theme") => ({
  computed: {
    themeClass() {
      return ni(this[e]);
    }
  }
}), mi = ae({
  name: "VPopperContent",
  components: {
    ResizeObserver: bn
  },
  mixins: [
    Ro()
  ],
  props: {
    popperId: String,
    theme: String,
    shown: Boolean,
    mounted: Boolean,
    skipTransition: Boolean,
    autoHide: Boolean,
    handleResize: Boolean,
    classes: Object,
    result: Object
  },
  emits: [
    "hide",
    "resize"
  ],
  methods: {
    toPx(e) {
      return e != null && !isNaN(e) ? `${e}px` : null;
    }
  }
}), hi = ["id", "aria-hidden", "tabindex", "data-popper-placement"], _i = {
  ref: "inner",
  class: "v-popper__inner"
}, gi = /* @__PURE__ */ M("div", { class: "v-popper__arrow-outer" }, null, -1), vi = /* @__PURE__ */ M("div", { class: "v-popper__arrow-inner" }, null, -1), yi = [
  gi,
  vi
];
function Ei(e, t, n, s, o, a) {
  const r = In("ResizeObserver");
  return S(), P("div", {
    id: e.popperId,
    ref: "popover",
    class: R(["v-popper__popper", [
      e.themeClass,
      e.classes.popperClass,
      {
        "v-popper__popper--shown": e.shown,
        "v-popper__popper--hidden": !e.shown,
        "v-popper__popper--show-from": e.classes.showFrom,
        "v-popper__popper--show-to": e.classes.showTo,
        "v-popper__popper--hide-from": e.classes.hideFrom,
        "v-popper__popper--hide-to": e.classes.hideTo,
        "v-popper__popper--skip-transition": e.skipTransition,
        "v-popper__popper--arrow-overflow": e.result && e.result.arrow.overflow,
        "v-popper__popper--no-positioning": !e.result
      }
    ]]),
    style: Nn(e.result ? {
      position: e.result.strategy,
      transform: `translate3d(${Math.round(e.result.x)}px,${Math.round(e.result.y)}px,0)`
    } : void 0),
    "aria-hidden": e.shown ? "false" : "true",
    tabindex: e.autoHide ? 0 : void 0,
    "data-popper-placement": e.result ? e.result.placement : void 0,
    onKeyup: t[2] || (t[2] = ho((l) => e.autoHide && e.$emit("hide"), ["esc"]))
  }, [
    M("div", {
      class: "v-popper__backdrop",
      onClick: t[0] || (t[0] = (l) => e.autoHide && e.$emit("hide"))
    }),
    M("div", {
      class: "v-popper__wrapper",
      style: Nn(e.result ? {
        transformOrigin: e.result.transformOrigin
      } : void 0)
    }, [
      M("div", _i, [
        e.mounted ? (S(), P(Fe, { key: 0 }, [
          M("div", null, [
            ee(e.$slots, "default")
          ]),
          e.handleResize ? (S(), $e(r, {
            key: 0,
            onNotify: t[1] || (t[1] = (l) => e.$emit("resize", l))
          })) : j("", !0)
        ], 64)) : j("", !0)
      ], 512),
      M("div", {
        ref: "arrow",
        class: "v-popper__arrow-container",
        style: Nn(e.result ? {
          left: e.toPx(e.result.arrow.x),
          top: e.toPx(e.result.arrow.y)
        } : void 0)
      }, yi, 4)
    ], 4)
  ], 46, hi);
}
const Do = /* @__PURE__ */ ns(mi, [["render", Ei]]), Fo = {
  methods: {
    show(...e) {
      return this.$refs.popper.show(...e);
    },
    hide(...e) {
      return this.$refs.popper.hide(...e);
    },
    dispose(...e) {
      return this.$refs.popper.dispose(...e);
    },
    onResize(...e) {
      return this.$refs.popper.onResize(...e);
    }
  }
};
let Vn = function() {
};
typeof window < "u" && (Vn = window.Element);
const bi = ae({
  name: "VPopperWrapper",
  components: {
    Popper: ui,
    PopperContent: Do
  },
  mixins: [
    Fo,
    Ro("finalTheme")
  ],
  props: {
    theme: {
      type: String,
      default: null
    },
    referenceNode: {
      type: Function,
      default: null
    },
    shown: {
      type: Boolean,
      default: !1
    },
    showGroup: {
      type: String,
      default: null
    },
    // eslint-disable-next-line vue/require-prop-types
    ariaId: {
      default: null
    },
    disabled: {
      type: Boolean,
      default: void 0
    },
    positioningDisabled: {
      type: Boolean,
      default: void 0
    },
    placement: {
      type: String,
      default: void 0
    },
    delay: {
      type: [String, Number, Object],
      default: void 0
    },
    distance: {
      type: [Number, String],
      default: void 0
    },
    skidding: {
      type: [Number, String],
      default: void 0
    },
    triggers: {
      type: Array,
      default: void 0
    },
    showTriggers: {
      type: [Array, Function],
      default: void 0
    },
    hideTriggers: {
      type: [Array, Function],
      default: void 0
    },
    popperTriggers: {
      type: Array,
      default: void 0
    },
    popperShowTriggers: {
      type: [Array, Function],
      default: void 0
    },
    popperHideTriggers: {
      type: [Array, Function],
      default: void 0
    },
    container: {
      type: [String, Object, Vn, Boolean],
      default: void 0
    },
    boundary: {
      type: [String, Vn],
      default: void 0
    },
    strategy: {
      type: String,
      default: void 0
    },
    autoHide: {
      type: [Boolean, Function],
      default: void 0
    },
    handleResize: {
      type: Boolean,
      default: void 0
    },
    instantMove: {
      type: Boolean,
      default: void 0
    },
    eagerMount: {
      type: Boolean,
      default: void 0
    },
    popperClass: {
      type: [String, Array, Object],
      default: void 0
    },
    computeTransformOrigin: {
      type: Boolean,
      default: void 0
    },
    /**
     * @deprecated
     */
    autoMinSize: {
      type: Boolean,
      default: void 0
    },
    autoSize: {
      type: [Boolean, String],
      default: void 0
    },
    /**
     * @deprecated
     */
    autoMaxSize: {
      type: Boolean,
      default: void 0
    },
    autoBoundaryMaxSize: {
      type: Boolean,
      default: void 0
    },
    preventOverflow: {
      type: Boolean,
      default: void 0
    },
    overflowPadding: {
      type: [Number, String],
      default: void 0
    },
    arrowPadding: {
      type: [Number, String],
      default: void 0
    },
    arrowOverflow: {
      type: Boolean,
      default: void 0
    },
    flip: {
      type: Boolean,
      default: void 0
    },
    shift: {
      type: Boolean,
      default: void 0
    },
    shiftCrossAxis: {
      type: Boolean,
      default: void 0
    },
    noAutoFocus: {
      type: Boolean,
      default: void 0
    },
    disposeTimeout: {
      type: Number,
      default: void 0
    }
  },
  emits: {
    show: () => !0,
    hide: () => !0,
    "update:shown": (e) => !0,
    "apply-show": () => !0,
    "apply-hide": () => !0,
    "close-group": () => !0,
    "close-directive": () => !0,
    "auto-hide": () => !0,
    resize: () => !0
  },
  computed: {
    finalTheme() {
      return this.theme ?? this.$options.vPopperTheme;
    }
  },
  methods: {
    getTargetNodes() {
      return Array.from(this.$el.children).filter((e) => e !== this.$refs.popperContent.$el);
    }
  }
});
function Ti(e, t, n, s, o, a) {
  const r = In("PopperContent"), l = In("Popper");
  return S(), $e(l, ct({ ref: "popper" }, e.$props, {
    theme: e.finalTheme,
    "target-nodes": e.getTargetNodes,
    "popper-node": () => e.$refs.popperContent.$el,
    class: [
      e.themeClass
    ],
    onShow: t[0] || (t[0] = () => e.$emit("show")),
    onHide: t[1] || (t[1] = () => e.$emit("hide")),
    "onUpdate:shown": t[2] || (t[2] = (i) => e.$emit("update:shown", i)),
    onApplyShow: t[3] || (t[3] = () => e.$emit("apply-show")),
    onApplyHide: t[4] || (t[4] = () => e.$emit("apply-hide")),
    onCloseGroup: t[5] || (t[5] = () => e.$emit("close-group")),
    onCloseDirective: t[6] || (t[6] = () => e.$emit("close-directive")),
    onAutoHide: t[7] || (t[7] = () => e.$emit("auto-hide")),
    onResize: t[8] || (t[8] = () => e.$emit("resize"))
  }), {
    default: Ce(({
      popperId: i,
      isShown: d,
      shouldMountContent: _,
      skipTransition: h,
      autoHide: m,
      show: b,
      hide: u,
      handleResize: v,
      onResize: y,
      classes: f,
      result: g
    }) => [
      ee(e.$slots, "default", {
        shown: d,
        show: b,
        hide: u
      }),
      ot(r, {
        ref: "popperContent",
        "popper-id": i,
        theme: e.finalTheme,
        shown: d,
        mounted: _,
        "skip-transition": h,
        "auto-hide": m,
        "handle-resize": v,
        classes: f,
        result: g,
        onHide: u,
        onResize: y
      }, {
        default: Ce(() => [
          ee(e.$slots, "popper", {
            shown: d,
            hide: u
          })
        ]),
        _: 2
      }, 1032, ["popper-id", "theme", "shown", "mounted", "skip-transition", "auto-hide", "handle-resize", "classes", "result", "onHide", "onResize"])
    ]),
    _: 3
  }, 16, ["theme", "target-nodes", "popper-node", "class"]);
}
const ss = /* @__PURE__ */ ns(bi, [["render", Ti]]), wi = {
  ...ss,
  name: "VDropdown",
  vPopperTheme: "dropdown"
};
({
  ...ss
});
({
  ...ss
});
ae({
  name: "VTooltipDirective",
  components: {
    Popper: Mo(),
    PopperContent: Do
  },
  mixins: [
    Fo
  ],
  inheritAttrs: !1,
  props: {
    theme: {
      type: String,
      default: "tooltip"
    },
    html: {
      type: Boolean,
      default: (e) => Rn(e.theme, "html")
    },
    content: {
      type: [String, Number, Function],
      default: null
    },
    loadingContent: {
      type: String,
      default: (e) => Rn(e.theme, "loadingContent")
    },
    targetNodes: {
      type: Function,
      required: !0
    }
  },
  data() {
    return {
      asyncContent: null
    };
  },
  computed: {
    isContentAsync() {
      return typeof this.content == "function";
    },
    loading() {
      return this.isContentAsync && this.asyncContent == null;
    },
    finalContent() {
      return this.isContentAsync ? this.loading ? this.loadingContent : this.asyncContent : this.content;
    }
  },
  watch: {
    content: {
      handler() {
        this.fetchContent(!0);
      },
      immediate: !0
    },
    async finalContent() {
      await this.$nextTick(), this.$refs.popper.onResize();
    }
  },
  created() {
    this.$_fetchId = 0;
  },
  methods: {
    fetchContent(e) {
      if (typeof this.content == "function" && this.$_isShown && (e || !this.$_loading && this.asyncContent == null)) {
        this.asyncContent = null, this.$_loading = !0;
        const t = ++this.$_fetchId, n = this.content(this);
        n.then ? n.then((s) => this.onResult(t, s)) : this.onResult(t, n);
      }
    },
    onResult(e, t) {
      e === this.$_fetchId && (this.$_loading = !1, this.asyncContent = t);
    },
    onShow() {
      this.$_isShown = !0, this.fetchContent();
    },
    onHide() {
      this.$_isShown = !1;
    }
  }
});
const Ci = wi, Et = /* @__PURE__ */ ae({
  __name: "dropdown-component",
  props: {
    modelValue: { type: Boolean, default: void 0 },
    distance: { default: 6 },
    skidding: { default: 0 },
    autoSize: { type: [Boolean, String] },
    placement: { default: "bottom" },
    trigger: {},
    autoHide: { type: Boolean, default: !0 },
    arrowHide: { type: Boolean, default: !0 },
    onlyRead: { type: Boolean, default: !1 }
  },
  emits: ["update:modelValue", "hideDropdown"],
  setup(e, { emit: t }) {
    const n = e, s = t, { modelValue: o } = Pt(n), a = ue(n.modelValue ?? !1), r = B({
      get: () => (o == null ? void 0 : o.value) ?? a.value,
      set: (d) => {
        (o == null ? void 0 : o.value) !== void 0 && s("update:modelValue", d), a.value = d;
      }
    });
    function l() {
      r.value = !0;
    }
    function i() {
      r.value = !1, s("hideDropdown");
    }
    return (d, _) => (S(), $e(Ee(Ci), ct({
      shown: r.value,
      theme: "dropdown",
      distance: d.arrowHide ? d.distance : d.distance + 14,
      skidding: d.skidding,
      autoSize: d.autoSize,
      "auto-hide": d.autoHide,
      placement: d.placement,
      "compute-transform-origin": "",
      popperClass: `ui-dropdown ${d.arrowHide ? "ui-dropdown__arrowHide" : ""}`,
      onHide: i,
      onShow: l,
      disabled: d.onlyRead
    }, d.$attrs), {
      popper: Ce(({ hide: h }) => [
        ee(d.$slots, "dropdown", { hide: h })
      ]),
      default: Ce(() => [
        ee(d.$slots, "trigger")
      ]),
      _: 3
    }, 16, ["shown", "distance", "skidding", "autoSize", "auto-hide", "placement", "popperClass", "disabled"]));
  }
});
Et.install = (e) => {
  e.component(Et.__name, Et);
};
var os = /* @__PURE__ */ ((e) => (e.Auto = "auto", e.AutoStart = "auto-start", e.AutoEnd = "auto-end", e.TOP = "top", e.TopStart = "top-start", e.TopEnd = "top-end", e.Right = "right", e.RightStart = "right-start", e.RightEnd = "right-end", e.Bottom = "bottom", e.BottomStart = "bottom-start", e.BottomEnd = "bottom-end", e.Left = "left", e.LeftStart = "left-start", e.LeftEnd = "left-end", e))(os || {}), Li = /* @__PURE__ */ ((e) => (e.Click = "click", e.Hover = "hover", e.Focus = "focus", e.Touch = "touch", e))(Li || {});
/*! maska v2.1.11 | (c) Alexander Shabunevich | Released under the MIT license */
var Ni = Object.defineProperty, Oi = (e, t, n) => t in e ? Ni(e, t, { enumerable: !0, configurable: !0, writable: !0, value: n }) : e[t] = n, Bt = (e, t, n) => (Oi(e, typeof t != "symbol" ? t + "" : t, n), n);
const Is = {
  "#": { pattern: /[0-9]/ },
  "@": { pattern: /[a-zA-Z]/ },
  "*": { pattern: /[a-zA-Z0-9]/ }
};
class Ps {
  constructor(t = {}) {
    Bt(this, "opts", {}), Bt(this, "memo", /* @__PURE__ */ new Map());
    const n = { ...t };
    if (n.tokens != null) {
      n.tokens = n.tokensReplace ? { ...n.tokens } : { ...Is, ...n.tokens };
      for (const s of Object.values(n.tokens))
        typeof s.pattern == "string" && (s.pattern = new RegExp(s.pattern));
    } else
      n.tokens = Is;
    Array.isArray(n.mask) && (n.mask.length > 1 ? n.mask = [...n.mask].sort((s, o) => s.length - o.length) : n.mask = n.mask[0] ?? ""), n.mask === "" && (n.mask = null), this.opts = n;
  }
  masked(t) {
    return this.process(t, this.findMask(t));
  }
  unmasked(t) {
    return this.process(t, this.findMask(t), !1);
  }
  isEager() {
    return this.opts.eager === !0;
  }
  isReversed() {
    return this.opts.reversed === !0;
  }
  completed(t) {
    const n = this.findMask(t);
    if (this.opts.mask == null || n == null)
      return !1;
    const s = this.process(t, n).length;
    return typeof this.opts.mask == "string" ? s >= this.opts.mask.length : typeof this.opts.mask == "function" ? s >= n.length : this.opts.mask.filter((o) => s >= o.length).length === this.opts.mask.length;
  }
  findMask(t) {
    const n = this.opts.mask;
    if (n == null)
      return null;
    if (typeof n == "string")
      return n;
    if (typeof n == "function")
      return n(t);
    const s = this.process(t, n.slice(-1).pop() ?? "", !1);
    return n.find((o) => this.process(t, o, !1).length >= s.length) ?? "";
  }
  escapeMask(t) {
    const n = [], s = [];
    return t.split("").forEach((o, a) => {
      o === "!" && t[a - 1] !== "!" ? s.push(a - s.length) : n.push(o);
    }), { mask: n.join(""), escaped: s };
  }
  process(t, n, s = !0) {
    if (n == null)
      return t;
    const o = `value=${t},mask=${n},masked=${s ? 1 : 0}`;
    if (this.memo.has(o))
      return this.memo.get(o);
    const { mask: a, escaped: r } = this.escapeMask(n), l = [], i = this.opts.tokens != null ? this.opts.tokens : {}, d = this.isReversed() ? -1 : 1, _ = this.isReversed() ? "unshift" : "push", h = this.isReversed() ? 0 : a.length - 1, m = this.isReversed() ? () => y > -1 && f > -1 : () => y < a.length && f < t.length, b = (w) => !this.isReversed() && w <= h || this.isReversed() && w >= h;
    let u, v = -1, y = this.isReversed() ? a.length - 1 : 0, f = this.isReversed() ? t.length - 1 : 0, g = !1;
    for (; m(); ) {
      const w = a.charAt(y), E = i[w], L = (E == null ? void 0 : E.transform) != null ? E.transform(t.charAt(f)) : t.charAt(f);
      if (!r.includes(y) && E != null ? (L.match(E.pattern) != null ? (l[_](L), E.repeated ? (v === -1 ? v = y : y === h && y !== v && (y = v - d), h === v && (y -= d)) : E.multiple && (g = !0, y -= d), y += d) : E.multiple ? g && (y += d, f -= d, g = !1) : L === u ? u = void 0 : E.optional && (y += d, f -= d), f += d) : (s && !this.isEager() && l[_](w), L === w && !this.isEager() ? f += d : u = w, this.isEager() || (y += d)), this.isEager())
        for (; b(y) && (i[a.charAt(y)] == null || r.includes(y)); )
          s ? l[_](a.charAt(y)) : a.charAt(y) === t.charAt(f) && (f += d), y += d;
    }
    return this.memo.set(o, l.join("")), this.memo.get(o);
  }
}
const Vo = (e) => JSON.parse(e.replaceAll("'", '"')), Ms = (e, t = {}) => {
  const n = { ...t };
  return e.dataset.maska != null && e.dataset.maska !== "" && (n.mask = ki(e.dataset.maska)), e.dataset.maskaEager != null && (n.eager = An(e.dataset.maskaEager)), e.dataset.maskaReversed != null && (n.reversed = An(e.dataset.maskaReversed)), e.dataset.maskaTokensReplace != null && (n.tokensReplace = An(e.dataset.maskaTokensReplace)), e.dataset.maskaTokens != null && (n.tokens = Ai(e.dataset.maskaTokens)), n;
}, An = (e) => e !== "" ? !!JSON.parse(e) : !0, ki = (e) => e.startsWith("[") && e.endsWith("]") ? Vo(e) : e, Ai = (e) => {
  if (e.startsWith("{") && e.endsWith("}"))
    return Vo(e);
  const t = {};
  return e.split("|").forEach((n) => {
    const s = n.split(":");
    t[s[0]] = {
      pattern: new RegExp(s[1]),
      optional: s[2] === "optional",
      multiple: s[2] === "multiple",
      repeated: s[2] === "repeated"
    };
  }), t;
};
class $i {
  constructor(t, n = {}) {
    Bt(this, "items", /* @__PURE__ */ new Map()), Bt(this, "beforeinputEvent", (s) => {
      const o = s.target, a = this.items.get(o);
      a.isEager() && "inputType" in s && s.inputType.startsWith("delete") && a.unmasked(o.value).length <= 1 && this.setMaskedValue(o, "");
    }), Bt(this, "inputEvent", (s) => {
      if (s instanceof CustomEvent && s.type === "input" && s.detail != null && typeof s.detail == "object" && "masked" in s.detail)
        return;
      const o = s.target, a = this.items.get(o), r = o.value, l = o.selectionStart, i = o.selectionEnd;
      let d = r;
      if (a.isEager()) {
        const _ = a.masked(r), h = a.unmasked(r);
        h === "" && "data" in s && s.data != null ? d = s.data : h !== a.unmasked(_) && (d = h);
      }
      if (this.setMaskedValue(o, d), "inputType" in s && (s.inputType.startsWith("delete") || l != null && l < r.length))
        try {
          o.setSelectionRange(l, i);
        } catch {
        }
    }), this.options = n, typeof t == "string" ? this.init(
      Array.from(document.querySelectorAll(t)),
      this.getMaskOpts(n)
    ) : this.init(
      "length" in t ? Array.from(t) : [t],
      this.getMaskOpts(n)
    );
  }
  destroy() {
    for (const t of this.items.keys())
      t.removeEventListener("input", this.inputEvent), t.removeEventListener("beforeinput", this.beforeinputEvent);
    this.items.clear();
  }
  needUpdateOptions(t, n) {
    const s = this.items.get(t), o = new Ps(Ms(t, this.getMaskOpts(n)));
    return JSON.stringify(s.opts) !== JSON.stringify(o.opts);
  }
  needUpdateValue(t) {
    const n = t.dataset.maskaValue;
    return n == null && t.value !== "" || n != null && n !== t.value;
  }
  getMaskOpts(t) {
    const { onMaska: n, preProcess: s, postProcess: o, ...a } = t;
    return a;
  }
  init(t, n) {
    for (const s of t) {
      const o = new Ps(Ms(s, n));
      this.items.set(s, o), s.value !== "" && this.setMaskedValue(s, s.value), s.addEventListener("input", this.inputEvent), s.addEventListener("beforeinput", this.beforeinputEvent);
    }
  }
  setMaskedValue(t, n) {
    const s = this.items.get(t);
    this.options.preProcess != null && (n = this.options.preProcess(n));
    const o = s.masked(n), a = s.unmasked(s.isEager() ? o : n), r = s.completed(n), l = { masked: o, unmasked: a, completed: r };
    n = o, this.options.postProcess != null && (n = this.options.postProcess(n)), t.value = n, t.dataset.maskaValue = n, this.options.onMaska != null && (Array.isArray(this.options.onMaska) ? this.options.onMaska.forEach((i) => i(l)) : this.options.onMaska(l)), t.dispatchEvent(new CustomEvent("maska", { detail: l })), t.dispatchEvent(new CustomEvent("input", { detail: l }));
  }
}
const Bn = /* @__PURE__ */ new WeakMap(), Si = (e) => {
  setTimeout(() => {
    var t;
    ((t = Bn.get(e)) == null ? void 0 : t.needUpdateValue(e)) === !0 && e.dispatchEvent(new CustomEvent("input"));
  });
}, Ii = (e, t) => {
  const n = e instanceof HTMLInputElement ? e : e.querySelector("input"), s = { ...t.arg };
  if (n == null || (n == null ? void 0 : n.type) === "file")
    return;
  Si(n);
  const o = Bn.get(n);
  if (o != null) {
    if (!o.needUpdateOptions(n, s))
      return;
    o.destroy();
  }
  if (t.value != null) {
    const a = t.value, r = (l) => {
      a.masked = l.masked, a.unmasked = l.unmasked, a.completed = l.completed;
    };
    s.onMaska = s.onMaska == null ? r : Array.isArray(s.onMaska) ? [...s.onMaska, r] : [s.onMaska, r];
  }
  Bn.set(n, new $i(n, s));
};
function Bo(e) {
  return ka() ? (Aa(e), !0) : !1;
}
function as(e) {
  return typeof e == "function" ? e() : Ee(e);
}
const Pi = typeof window < "u" && typeof document < "u";
typeof WorkerGlobalScope < "u" && globalThis instanceof WorkerGlobalScope;
const Mi = Object.prototype.toString, Ri = (e) => Mi.call(e) === "[object Object]", Di = () => {
};
function Uo(e) {
  var t;
  const n = as(e);
  return (t = n == null ? void 0 : n.$el) != null ? t : n;
}
const xo = Pi ? window : void 0;
function Rs(...e) {
  let t, n, s, o;
  if (typeof e[0] == "string" || Array.isArray(e[0]) ? ([n, s, o] = e, t = xo) : [t, n, s, o] = e, !t)
    return Di;
  Array.isArray(n) || (n = [n]), Array.isArray(s) || (s = [s]);
  const a = [], r = () => {
    a.forEach((_) => _()), a.length = 0;
  }, l = (_, h, m, b) => (_.addEventListener(h, m, b), () => _.removeEventListener(h, m, b)), i = rt(
    () => [Uo(t), as(o)],
    ([_, h]) => {
      if (r(), !_)
        return;
      const m = Ri(h) ? { ...h } : h;
      a.push(
        ...n.flatMap((b) => s.map((u) => l(_, b, u, m)))
      );
    },
    { immediate: !0, flush: "post" }
  ), d = () => {
    i(), r();
  };
  return Bo(d), d;
}
function Fi() {
  const e = ue(!1), t = gn();
  return t && _o(() => {
    e.value = !0;
  }, t), e;
}
function Vi(e) {
  const t = Fi();
  return B(() => (t.value, !!e()));
}
function Bi(e, t = {}) {
  const { window: n = xo } = t, s = Vi(() => n && "matchMedia" in n && typeof n.matchMedia == "function");
  let o;
  const a = ue(!1), r = (d) => {
    a.value = d.matches;
  }, l = () => {
    o && ("removeEventListener" in o ? o.removeEventListener("change", r) : o.removeListener(r));
  }, i = $a(() => {
    s.value && (l(), o = n.matchMedia(as(e)), "addEventListener" in o ? o.addEventListener("change", r) : o.addListener(r), a.value = o.matches);
  });
  return Bo(() => {
    i(), l(), o = void 0;
  }), a;
}
function Ui(e, t = {}) {
  const { initialValue: n = !1, focusVisible: s = !1 } = t, o = ue(!1), a = B(() => Uo(e));
  Rs(a, "focus", (l) => {
    var i, d;
    (!s || (d = (i = l.target).matches) != null && d.call(i, ":focus-visible")) && (o.value = !0);
  }), Rs(a, "blur", () => o.value = !1);
  const r = B({
    get: () => o.value,
    set(l) {
      var i, d;
      !l && o.value ? (i = a.value) == null || i.blur() : l && !o.value && ((d = a.value) == null || d.focus());
    }
  });
  return rt(
    a,
    () => {
      r.value = n;
    },
    { immediate: !0, flush: "post" }
  ), { focused: r };
}
var Ho = /* @__PURE__ */ ((e) => (e.DEFAULT = "default", e.BLUE = "blue", e))(Ho || {});
const xi = /* @__PURE__ */ M("svg", {
  viewBox: "0 0 18 18",
  fill: "none",
  xmlns: "http://www.w3.org/2000/svg"
}, [
  /* @__PURE__ */ M("path", {
    d: "M17.1256 16.2418L12.4056 11.5218C13.5398 10.1601 14.1053 8.41361 13.9847 6.64558C13.864 4.87756 13.0663 3.22412 11.7576 2.02924C10.4488 0.834363 8.72983 0.190035 6.95814 0.230295C5.18645 0.270555 3.49849 0.992304 2.2454 2.2454C0.992304 3.49849 0.270555 5.18645 0.230295 6.95814C0.190035 8.72983 0.834363 10.4488 2.02924 11.7576C3.22412 13.0663 4.87756 13.864 6.64558 13.9847C8.41361 14.1053 10.1601 13.5398 11.5218 12.4056L16.2418 17.1256L17.1256 16.2418ZM1.50064 7.12564C1.50064 6.01312 1.83054 4.92558 2.44862 4.00055C3.0667 3.07553 3.94521 2.35456 4.97304 1.92881C6.00088 1.50307 7.13188 1.39168 8.22302 1.60872C9.31416 1.82576 10.3164 2.36149 11.1031 3.14816C11.8898 3.93483 12.4255 4.93711 12.6426 6.02825C12.8596 7.1194 12.7482 8.2504 12.3225 9.27823C11.8967 10.3061 11.1757 11.1846 10.2507 11.8027C9.32569 12.4207 8.23816 12.7506 7.12564 12.7506C5.6343 12.749 4.20452 12.1558 3.14999 11.1013C2.09545 10.0468 1.50229 8.61697 1.50064 7.12564Z",
    fill: "#808080"
  })
], -1), Hi = /* @__PURE__ */ M("g", { id: "Ä°cons/loader" }, [
  /* @__PURE__ */ M("path", {
    id: "Union",
    d: "M7 1.5C5.70632 1.5 4.44729 1.91813 3.41063 2.69205C2.37397 3.46596 1.61522 4.55421 1.24749 5.79452C0.879769 7.03484 0.922777 8.36079 1.3701 9.57467C1.81743 10.7886 2.64511 11.8253 3.72975 12.5305C4.81438 13.2356 6.09786 13.5712 7.38882 13.4874C8.67978 13.4036 9.90907 12.9047 10.8934 12.0652C11.8777 11.2258 12.5644 10.0906 12.8509 8.82911C13.1375 7.56757 13.0087 6.24719 12.4836 5.06485L11.4068 5.54305C11.8287 6.49321 11.9323 7.5543 11.702 8.56811C11.4717 9.58192 10.9199 10.4941 10.1288 11.1688C9.33781 11.8434 8.34992 12.2443 7.31247 12.3116C6.27502 12.379 5.24357 12.1093 4.37193 11.5426C3.50029 10.976 2.83514 10.1428 2.47566 9.16726C2.11617 8.19176 2.08161 7.12619 2.37712 6.12943C2.67264 5.13268 3.2824 4.25813 4.11548 3.63619C4.94857 3.01425 5.96036 2.67823 7 2.67823V1.5Z",
    fill: "#808080"
  })
], -1), Wi = [
  Hi
], Gi = /* @__PURE__ */ M("g", { id: "Ä°cons/close" }, [
  /* @__PURE__ */ M("path", {
    id: "Vector",
    d: "M12.25 3.16875L11.3313 2.25L7 6.58125L2.66875 2.25L1.75 3.16875L6.08125 7.5L1.75 11.8313L2.66875 12.75L7 8.41875L11.3313 12.75L12.25 11.8313L7.91875 7.5L12.25 3.16875Z",
    fill: "#808080"
  })
], -1), zi = [
  Gi
], Yi = /* @__PURE__ */ M("g", { id: "Ä°cons/view-off" }, [
  /* @__PURE__ */ M("path", {
    id: "Vector",
    d: "M3.77473 14.0688L4.66848 13.1812C3.68987 12.3026 2.92097 11.2154 2.41848 10C3.68723 6.83125 7.18723 4.375 10.4997 4.375C11.3522 4.38625 12.1969 4.5383 12.9997 4.825L13.9685 3.85C12.8701 3.38583 11.692 3.13959 10.4997 3.125C8.46256 3.20161 6.49248 3.87347 4.83299 5.05755C3.17349 6.24164 1.89733 7.88603 1.16223 9.7875C1.11259 9.92482 1.11259 10.0752 1.16223 10.2125C1.71739 11.6856 2.6125 13.0069 3.77473 14.0688Z",
    fill: "#808080"
  }),
  /* @__PURE__ */ M("path", {
    id: "Vector_2",
    d: "M8 9.83125C8.04346 9.23232 8.30106 8.66905 8.72568 8.24443C9.1503 7.81981 9.71357 7.56221 10.3125 7.51875L11.4438 6.38125C10.8099 6.21434 10.1433 6.21651 9.51047 6.38754C8.87767 6.55857 8.30076 6.89248 7.83725 7.356C7.37373 7.81951 7.03982 8.39642 6.86879 9.02922C6.69776 9.66202 6.69559 10.3286 6.8625 10.9625L8 9.83125ZM19.8375 9.7875C19.1208 7.92075 17.8738 6.30439 16.25 5.1375L19.25 2.13125L18.3687 1.25L1.75 17.8687L2.63125 18.75L5.81875 15.5625C7.23992 16.3963 8.85253 16.8484 10.5 16.875C12.5372 16.7984 14.5073 16.1265 16.1667 14.9424C17.8262 13.7584 19.1024 12.114 19.8375 10.2125C19.8871 10.0752 19.8871 9.92482 19.8375 9.7875ZM13 10C12.9974 10.4376 12.8799 10.8668 12.6594 11.2447C12.4389 11.6227 12.1231 11.9361 11.7435 12.1538C11.3639 12.3714 10.9338 12.4856 10.4962 12.485C10.0587 12.4843 9.62894 12.3688 9.25 12.15L12.65 8.75C12.8747 9.12858 12.9954 9.55978 13 10ZM10.5 15.625C9.18879 15.6021 7.90243 15.2634 6.75 14.6375L8.3375 13.05C9.05959 13.551 9.93471 13.7825 10.8101 13.704C11.6855 13.6256 12.5055 13.2421 13.1269 12.6207C13.7484 11.9992 14.1318 11.1792 14.2103 10.3038C14.2888 9.42846 14.0573 8.55333 13.5562 7.83125L15.35 6.0375C16.7841 7.02186 17.9056 8.39714 18.5812 10C17.3125 13.1687 13.8125 15.625 10.5 15.625Z",
    fill: "#808080"
  })
], -1), ji = [
  Yi
], qi = /* @__PURE__ */ M("g", { id: "Ä°cons/eye-on" }, [
  /* @__PURE__ */ M("path", {
    id: "Vector",
    d: "M19.375 9.75C17.875 5.875 14.125 3.25 10 3.125C5.875 3.25 2.125 5.875 0.625 9.75C0.625 9.875 0.625 10 0.625 10.125C2.125 14 5.75 16.625 10 16.75C14.125 16.625 17.875 14 19.375 10.125C19.375 10.125 19.375 9.875 19.375 9.75ZM10 15.625C6.625 15.625 3.25 13.125 1.875 10C3.125 6.875 6.625 4.375 10 4.375C13.375 4.375 16.75 6.875 18.125 10C16.75 13.125 13.25 15.625 10 15.625Z",
    fill: "#808080"
  }),
  /* @__PURE__ */ M("path", {
    id: "Vector_2",
    d: "M10 6.25C7.875 6.25 6.25 7.875 6.25 10C6.25 12.125 7.875 13.75 10 13.75C12.125 13.75 13.75 12.125 13.75 10C13.75 7.875 12.125 6.25 10 6.25ZM10 12.5C8.625 12.5 7.5 11.375 7.5 10C7.5 8.625 8.625 7.5 10 7.5C11.375 7.5 12.5 8.625 12.5 10C12.5 11.375 11.375 12.5 10 12.5Z",
    fill: "#808080"
  })
], -1), Ki = [
  qi
], Xi = /* @__PURE__ */ M("svg", {
  width: "20",
  height: "20",
  viewBox: "0 0 20 20",
  fill: "none",
  xmlns: "http://www.w3.org/2000/svg"
}, [
  /* @__PURE__ */ M("g", { id: "Ä°cons/check" }, [
    /* @__PURE__ */ M("path", {
      id: "Vector",
      d: "M8.125 15.0009L2.5 9.37594L3.38375 8.49219L8.125 13.2328L16.6163 4.74219L17.5 5.62594L8.125 15.0009Z",
      fill: "#007C65"
    })
  ])
], -1), Zi = /* @__PURE__ */ M("g", {
  id: "information_16",
  opacity: "0.5"
}, [
  /* @__PURE__ */ M("path", {
    id: "Vector",
    d: "M10.625 13.75V8.125H8.125V9.375H9.375V13.75H7.5V15H12.5V13.75H10.625ZM10 4.375C9.5 4.375 9 4.75 9 5.375C9 6 9.5 6.25 10 6.25C10.5 6.25 11 5.875 11 5.25C11 4.625 10.5 4.375 10 4.375Z",
    fill: "#808080"
  }),
  /* @__PURE__ */ M("path", {
    id: "Vector_2",
    d: "M10 18.75C5.125 18.75 1.25 14.875 1.25 10C1.25 5.125 5.125 1.25 10 1.25C14.875 1.25 18.75 5.125 18.75 10C18.75 14.875 14.875 18.75 10 18.75ZM10 2.5C5.875 2.5 2.5 5.875 2.5 10C2.5 14.125 5.875 17.5 10 17.5C14.125 17.5 17.5 14.125 17.5 10C17.5 5.875 14.125 2.5 10 2.5Z",
    fill: "#808080"
  })
], -1), Ji = [
  Zi
], Qi = /* @__PURE__ */ ae({
  __name: "input-component",
  props: {
    isError: { type: Boolean, default: !1 },
    isSuccess: { type: Boolean, default: !1 },
    isLoading: { type: Boolean, default: !1 },
    isDisabled: { type: Boolean, default: !1 },
    modelValue: { default: "" },
    isTextarea: { type: Boolean, default: !1 },
    containerClass: { default: "" },
    errorText: { default: "" },
    label: { default: "" },
    isNeedSearchIcon: { type: Boolean, default: !0 },
    info: {},
    maskPattern: { default: "" },
    dataMaskTokens: { default: "" },
    inputType: { default: Ho.DEFAULT },
    commentTextAreaStyle: { default: "" }
  },
  emits: ["update:modelValue", "update:isError"],
  setup(e, { emit: t }) {
    const n = Sa(), s = e, o = t, a = ue(), { focused: r } = Ui(a), l = ue(!1), i = ue(!1), d = B(() => s.modelValue.length), _ = B(() => i.value || d.value), h = () => {
      o("update:modelValue", "");
    };
    rt(r, (u) => {
      i.value = u;
    });
    const m = B(() => n.type === "password"), b = B(() => l.value ? "text" : n.type);
    return (u, v) => (S(), P("div", {
      class: R([
        u.$style[u.inputType],
        u.$style.inputContainer,
        [u.$slots.extra && u.$style.withExtraContainer, u.label && u.$style.withLabel],
        u.containerClass,
        {
          [u.$style.active]: _.value,
          [u.$style.error]: u.isError,
          [u.$style.success]: u.isSuccess,
          [u.$style.loading]: u.isLoading,
          [u.$style.disabled]: u.isDisabled
        }
      ])
    }, [
      u.label ? (S(), P("div", {
        key: 0,
        class: R(u.$style.label)
      }, pe(u.label), 3)) : j("", !0),
      u.isNeedSearchIcon && !u.isTextarea ? (S(), P("div", {
        key: 1,
        class: R(u.$style.icon)
      }, [
        ee(u.$slots, "search-icon", {}, () => [
          xi
        ])
      ], 2)) : j("", !0),
      go((S(), $e(po(u.isTextarea ? "textarea" : "input"), ct({
        value: u.modelValue,
        onInput: v[0] || (v[0] = (y) => u.$emit("update:modelValue", y.target.value)),
        ref_key: "input",
        ref: a
      }, u.$attrs, {
        type: b.value,
        class: [
          u.$style.input,
          u.info && u.info.length && u.$style.infoInputPadding,
          u.isTextarea && u.$style.textarea
        ],
        disabled: u.isDisabled || u.isLoading,
        onKeydown: v[1] || (v[1] = (y) => u.$emit("update:isError")),
        "data-maska": u.maskPattern,
        "data-maska-tokens": u.dataMaskTokens
      }), null, 16, ["value", "type", "class", "disabled", "data-maska", "data-maska-tokens"])), [
        [Ee(Ii)]
      ]),
      M("div", {
        class: R(u.$style.clear)
      }, [
        ee(u.$slots, "close-icon", {
          isShowCloseButton: _.value,
          clear: h
        }, () => [
          u.isLoading && !u.isTextarea ? (S(), P("svg", {
            key: 0,
            viewBox: "0 0 14 15",
            fill: "none",
            xmlns: "http://www.w3.org/2000/svg",
            class: R(u.$style.loaderIcon)
          }, Wi, 2)) : _.value && !u.isTextarea && !m.value ? (S(), P("svg", {
            key: 1,
            viewBox: "0 0 14 15",
            fill: "none",
            xmlns: "http://www.w3.org/2000/svg",
            onClick: h
          }, zi)) : j("", !0),
          m.value && !l.value ? (S(), P("svg", {
            key: 2,
            viewBox: "0 0 21 20",
            fill: "none",
            xmlns: "http://www.w3.org/2000/svg",
            class: R([l.value && u.$style.activeShowPass]),
            onClick: v[2] || (v[2] = (y) => l.value = !l.value)
          }, ji, 2)) : j("", !0),
          m.value && l.value ? (S(), P("svg", {
            key: 3,
            viewBox: "0 0 20 20",
            fill: "none",
            xmlns: "http://www.w3.org/2000/svg",
            class: R(u.$style.activeShowPass),
            onClick: v[3] || (v[3] = (y) => l.value = !l.value)
          }, Ki, 2)) : j("", !0),
          u.isSuccess ? (S(), P("div", {
            key: 4,
            class: R(u.$style.successIcon)
          }, [
            ee(u.$slots, "successIcon", {}, () => [
              Xi
            ])
          ], 2)) : j("", !0)
        ])
      ], 2),
      ee(u.$slots, "append"),
      M("div", {
        class: R([u.$style.extra, u.$slots.extra && u.isError && u.$style.withExtra])
      }, [
        M("div", null, [
          ee(u.$slots, "extra")
        ]),
        u.isError ? (S(), P("div", {
          key: 0,
          class: R(u.$style.errorText)
        }, pe(u.errorText), 3)) : j("", !0)
      ], 2),
      u.isTextarea ? (S(), P("div", {
        key: 2,
        class: R([u.$style.commentTextarea, u.commentTextAreaStyle])
      }, [
        ee(u.$slots, "comment")
      ], 2)) : j("", !0),
      u.info ? (S(), $e(Ee(Et), {
        key: 3,
        placement: Ee(os).Right,
        class: R(u.$style.info)
      }, {
        trigger: Ce(() => [
          ee(u.$slots, "info-trigger", {}, () => [
            (S(), P("svg", {
              width: "20",
              height: "20",
              viewBox: "0 0 20 20",
              fill: "none",
              xmlns: "http://www.w3.org/2000/svg",
              class: R([u.$style.infoTrigger, u.isError && u.$style.infoTriggerError])
            }, Ji, 2))
          ])
        ]),
        dropdown: Ce(() => [
          ee(u.$slots, "info-content", {}, () => [
            M("div", {
              class: R(u.$style.infoContent)
            }, [
              M("ul", null, [
                (S(!0), P(Fe, null, Tt(u.info, (y) => (S(), P("li", { key: y }, pe(y), 1))), 128))
              ])
            ], 2)
          ])
        ]),
        _: 3
      }, 8, ["placement", "class"])) : j("", !0)
    ], 2));
  }
}), el = "_inputContainer_9s52h_1", tl = "_active_9s52h_32", nl = "_blue_9s52h_37", sl = "_commentTextarea_9s52h_46", ol = "_withLabel_9s52h_60", al = "_withExtraContainer_9s52h_64", rl = "_icon_9s52h_68", il = "_input_9s52h_1", ll = "_clear_9s52h_101", ul = "_loaderIcon_9s52h_118", cl = "_rotate_9s52h_1", dl = "_activeShowPass_9s52h_122", fl = "_extra_9s52h_126", pl = "_withExtra_9s52h_64", ml = "_info_9s52h_153", hl = "_infoInputPadding_9s52h_165", _l = "_infoTrigger_9s52h_170", gl = "_infoTriggerError_9s52h_175", vl = "_infoContent_9s52h_182", yl = "_errorText_9s52h_197", El = "_successIcon_9s52h_213", bl = "_label_9s52h_218", Tl = "_textarea_9s52h_235", wl = "_error_9s52h_197", Cl = "_success_9s52h_213", Ll = "_loading_9s52h_261", Nl = "_disabled_9s52h_265", Ol = {
  inputContainer: el,
  default: "_default_9s52h_23",
  active: tl,
  blue: nl,
  commentTextarea: sl,
  withLabel: ol,
  withExtraContainer: al,
  icon: rl,
  input: il,
  clear: ll,
  loaderIcon: ul,
  rotate: cl,
  activeShowPass: dl,
  extra: fl,
  withExtra: pl,
  info: ml,
  infoInputPadding: hl,
  infoTrigger: _l,
  infoTriggerError: gl,
  infoContent: vl,
  errorText: yl,
  successIcon: El,
  label: bl,
  textarea: Tl,
  error: wl,
  success: Cl,
  loading: Ll,
  disabled: Nl
}, kl = {
  $style: Ol
}, bt = /* @__PURE__ */ Ie(Qi, [["__cssModules", kl]]);
bt.install = (e) => {
  e.component(bt.__name, bt);
};
/*!
  * shared v9.10.2
  * (c) 2024 kazuya kawaguchi
  * Released under the MIT License.
  */
const Ve = typeof window < "u";
let ge, it;
if (process.env.NODE_ENV !== "production") {
  const e = Ve && window.performance;
  e && e.mark && e.measure && e.clearMarks && // @ts-ignore browser compat
  e.clearMeasures && (ge = (t) => {
    e.mark(t);
  }, it = (t, n, s) => {
    e.measure(t, n, s), e.clearMarks(n), e.clearMarks(s);
  });
}
const Al = /\{([0-9a-zA-Z]+)\}/g;
function rs(e, ...t) {
  return t.length === 1 && Y(t[0]) && (t = t[0]), (!t || !t.hasOwnProperty) && (t = {}), e.replace(Al, (n, s) => t.hasOwnProperty(s) ? t[s] : "");
}
const Be = (e, t = !1) => t ? Symbol.for(e) : Symbol(e), $l = (e, t, n) => Sl({ l: e, k: t, s: n }), Sl = (e) => JSON.stringify(e).replace(/\u2028/g, "\\u2028").replace(/\u2029/g, "\\u2029").replace(/\u0027/g, "\\u0027"), ce = (e) => typeof e == "number" && isFinite(e), Il = (e) => Go(e) === "[object Date]", wt = (e) => Go(e) === "[object RegExp]", Tn = (e) => x(e) && Object.keys(e).length === 0, de = Object.assign;
let Ds;
const De = () => Ds || (Ds = typeof globalThis < "u" ? globalThis : typeof self < "u" ? self : typeof window < "u" ? window : typeof global < "u" ? global : {});
function Fs(e) {
  return e.replace(/</g, "&lt;").replace(/>/g, "&gt;").replace(/"/g, "&quot;").replace(/'/g, "&apos;");
}
const Pl = Object.prototype.hasOwnProperty;
function mn(e, t) {
  return Pl.call(e, t);
}
const oe = Array.isArray, K = (e) => typeof e == "function", I = (e) => typeof e == "string", q = (e) => typeof e == "boolean", Y = (e) => e !== null && typeof e == "object", Ml = (e) => Y(e) && K(e.then) && K(e.catch), Wo = Object.prototype.toString, Go = (e) => Wo.call(e), x = (e) => {
  if (!Y(e))
    return !1;
  const t = Object.getPrototypeOf(e);
  return t === null || t.constructor === Object;
}, Rl = (e) => e == null ? "" : oe(e) || x(e) && e.toString === Wo ? JSON.stringify(e, null, 2) : String(e);
function Dl(e, t = "") {
  return e.reduce((n, s, o) => o === 0 ? n + s : n + t + s, "");
}
const Vs = 2;
function Fl(e, t = 0, n = e.length) {
  const s = e.split(/\r?\n/);
  let o = 0;
  const a = [];
  for (let r = 0; r < s.length; r++)
    if (o += s[r].length + 1, o >= t) {
      for (let l = r - Vs; l <= r + Vs || n > o; l++) {
        if (l < 0 || l >= s.length)
          continue;
        const i = l + 1;
        a.push(`${i}${" ".repeat(3 - String(i).length)}|  ${s[l]}`);
        const d = s[l].length;
        if (l === r) {
          const _ = t - (o - d) + 1, h = Math.max(1, n > o ? d - _ : n - t);
          a.push("   |  " + " ".repeat(_) + "^".repeat(h));
        } else if (l > r) {
          if (n > o) {
            const _ = Math.max(Math.min(n - o, d), 1);
            a.push("   |  " + "^".repeat(_));
          }
          o += d + 1;
        }
      }
      break;
    }
  return a.join(`
`);
}
function is(e) {
  let t = e;
  return () => ++t;
}
function Ke(e, t) {
  typeof console < "u" && (console.warn("[intlify] " + e), t && console.warn(t.stack));
}
const Bs = {};
function zo(e) {
  Bs[e] || (Bs[e] = !0, Ke(e));
}
function Vl() {
  const e = /* @__PURE__ */ new Map();
  return {
    events: e,
    on(n, s) {
      const o = e.get(n);
      o && o.push(s) || e.set(n, [s]);
    },
    off(n, s) {
      const o = e.get(n);
      o && o.splice(o.indexOf(s) >>> 0, 1);
    },
    emit(n, s) {
      (e.get(n) || []).slice().map((o) => o(s)), (e.get("*") || []).slice().map((o) => o(n, s));
    }
  };
}
const Jt = (e) => !Y(e) || oe(e);
function nn(e, t) {
  if (Jt(e) || Jt(t))
    throw new Error("Invalid value");
  const n = [{ src: e, des: t }];
  for (; n.length; ) {
    const { src: s, des: o } = n.pop();
    Object.keys(s).forEach((a) => {
      Jt(s[a]) || Jt(o[a]) ? o[a] = s[a] : n.push({ src: s[a], des: o[a] });
    });
  }
}
/*!
  * message-compiler v9.10.2
  * (c) 2024 kazuya kawaguchi
  * Released under the MIT License.
  */
function Bl(e, t, n) {
  return { line: e, column: t, offset: n };
}
function Un(e, t, n) {
  const s = { start: e, end: t };
  return n != null && (s.source = n), s;
}
const Ul = /\{([0-9a-zA-Z]+)\}/g;
function xl(e, ...t) {
  return t.length === 1 && Hl(t[0]) && (t = t[0]), (!t || !t.hasOwnProperty) && (t = {}), e.replace(Ul, (n, s) => t.hasOwnProperty(s) ? t[s] : "");
}
const Yo = Object.assign, Us = (e) => typeof e == "string", Hl = (e) => e !== null && typeof e == "object";
function jo(e, t = "") {
  return e.reduce((n, s, o) => o === 0 ? n + s : n + t + s, "");
}
const F = {
  // tokenizer error codes
  EXPECTED_TOKEN: 1,
  INVALID_TOKEN_IN_PLACEHOLDER: 2,
  UNTERMINATED_SINGLE_QUOTE_IN_PLACEHOLDER: 3,
  UNKNOWN_ESCAPE_SEQUENCE: 4,
  INVALID_UNICODE_ESCAPE_SEQUENCE: 5,
  UNBALANCED_CLOSING_BRACE: 6,
  UNTERMINATED_CLOSING_BRACE: 7,
  EMPTY_PLACEHOLDER: 8,
  NOT_ALLOW_NEST_PLACEHOLDER: 9,
  INVALID_LINKED_FORMAT: 10,
  // parser error codes
  MUST_HAVE_MESSAGES_IN_PLURAL: 11,
  UNEXPECTED_EMPTY_LINKED_MODIFIER: 12,
  UNEXPECTED_EMPTY_LINKED_KEY: 13,
  UNEXPECTED_LEXICAL_ANALYSIS: 14,
  // generator error codes
  UNHANDLED_CODEGEN_NODE_TYPE: 15,
  // minifier error codes
  UNHANDLED_MINIFIER_NODE_TYPE: 16,
  // Special value for higher-order compilers to pick up the last code
  // to avoid collision of error codes. This should always be kept as the last
  // item.
  __EXTEND_POINT__: 17
}, Wl = {
  // tokenizer error messages
  [F.EXPECTED_TOKEN]: "Expected token: '{0}'",
  [F.INVALID_TOKEN_IN_PLACEHOLDER]: "Invalid token in placeholder: '{0}'",
  [F.UNTERMINATED_SINGLE_QUOTE_IN_PLACEHOLDER]: "Unterminated single quote in placeholder",
  [F.UNKNOWN_ESCAPE_SEQUENCE]: "Unknown escape sequence: \\{0}",
  [F.INVALID_UNICODE_ESCAPE_SEQUENCE]: "Invalid unicode escape sequence: {0}",
  [F.UNBALANCED_CLOSING_BRACE]: "Unbalanced closing brace",
  [F.UNTERMINATED_CLOSING_BRACE]: "Unterminated closing brace",
  [F.EMPTY_PLACEHOLDER]: "Empty placeholder",
  [F.NOT_ALLOW_NEST_PLACEHOLDER]: "Not allowed nest placeholder",
  [F.INVALID_LINKED_FORMAT]: "Invalid linked format",
  // parser error messages
  [F.MUST_HAVE_MESSAGES_IN_PLURAL]: "Plural must have messages",
  [F.UNEXPECTED_EMPTY_LINKED_MODIFIER]: "Unexpected empty linked modifier",
  [F.UNEXPECTED_EMPTY_LINKED_KEY]: "Unexpected empty linked key",
  [F.UNEXPECTED_LEXICAL_ANALYSIS]: "Unexpected lexical analysis in token: '{0}'",
  // generator error messages
  [F.UNHANDLED_CODEGEN_NODE_TYPE]: "unhandled codegen node type: '{0}'",
  // minimizer error messages
  [F.UNHANDLED_MINIFIER_NODE_TYPE]: "unhandled mimifier node type: '{0}'"
};
function Lt(e, t, n = {}) {
  const { domain: s, messages: o, args: a } = n, r = xl((o || Wl)[e] || "", ...a || []), l = new SyntaxError(String(r));
  return l.code = e, t && (l.location = t), l.domain = s, l;
}
function Gl(e) {
  throw e;
}
const zl = /<\/?[\w\s="/.':;#-\/]+>/, Yl = (e) => zl.test(e), Me = " ", jl = "\r", fe = `
`, ql = "\u2028", Kl = "\u2029";
function Xl(e) {
  const t = e;
  let n = 0, s = 1, o = 1, a = 0;
  const r = (A) => t[A] === jl && t[A + 1] === fe, l = (A) => t[A] === fe, i = (A) => t[A] === Kl, d = (A) => t[A] === ql, _ = (A) => r(A) || l(A) || i(A) || d(A), h = () => n, m = () => s, b = () => o, u = () => a, v = (A) => r(A) || i(A) || d(A) ? fe : t[A], y = () => v(n), f = () => v(n + a);
  function g() {
    return a = 0, _(n) && (s++, o = 0), r(n) && n++, n++, o++, t[n];
  }
  function w() {
    return r(n + a) && a++, a++, t[n + a];
  }
  function E() {
    n = 0, s = 1, o = 1, a = 0;
  }
  function L(A = 0) {
    a = A;
  }
  function N() {
    const A = n + a;
    for (; A !== n; )
      g();
    a = 0;
  }
  return {
    index: h,
    line: m,
    column: b,
    peekOffset: u,
    charAt: v,
    currentChar: y,
    currentPeek: f,
    next: g,
    peek: w,
    reset: E,
    resetPeek: L,
    skipToPeek: N
  };
}
const He = void 0, Zl = ".", xs = "'", Jl = "tokenizer";
function Ql(e, t = {}) {
  const n = t.location !== !1, s = Xl(e), o = () => s.index(), a = () => Bl(s.line(), s.column(), s.index()), r = a(), l = o(), i = {
    currentType: 14,
    offset: l,
    startLoc: r,
    endLoc: r,
    lastType: 14,
    lastOffset: l,
    lastStartLoc: r,
    lastEndLoc: r,
    braceNest: 0,
    inLinked: !1,
    text: ""
  }, d = () => i, { onError: _ } = t;
  function h(p, c, T, ...$) {
    const V = d();
    if (c.column += T, c.offset += T, _) {
      const G = n ? Un(V.startLoc, c) : null, Le = Lt(p, G, {
        domain: Jl,
        args: $
      });
      _(Le);
    }
  }
  function m(p, c, T) {
    p.endLoc = a(), p.currentType = c;
    const $ = { type: c };
    return n && ($.loc = Un(p.startLoc, p.endLoc)), T != null && ($.value = T), $;
  }
  const b = (p) => m(
    p,
    14
    /* TokenTypes.EOF */
  );
  function u(p, c) {
    return p.currentChar() === c ? (p.next(), c) : (h(F.EXPECTED_TOKEN, a(), 0, c), "");
  }
  function v(p) {
    let c = "";
    for (; p.currentPeek() === Me || p.currentPeek() === fe; )
      c += p.currentPeek(), p.peek();
    return c;
  }
  function y(p) {
    const c = v(p);
    return p.skipToPeek(), c;
  }
  function f(p) {
    if (p === He)
      return !1;
    const c = p.charCodeAt(0);
    return c >= 97 && c <= 122 || // a-z
    c >= 65 && c <= 90 || // A-Z
    c === 95;
  }
  function g(p) {
    if (p === He)
      return !1;
    const c = p.charCodeAt(0);
    return c >= 48 && c <= 57;
  }
  function w(p, c) {
    const { currentType: T } = c;
    if (T !== 2)
      return !1;
    v(p);
    const $ = f(p.currentPeek());
    return p.resetPeek(), $;
  }
  function E(p, c) {
    const { currentType: T } = c;
    if (T !== 2)
      return !1;
    v(p);
    const $ = p.currentPeek() === "-" ? p.peek() : p.currentPeek(), V = g($);
    return p.resetPeek(), V;
  }
  function L(p, c) {
    const { currentType: T } = c;
    if (T !== 2)
      return !1;
    v(p);
    const $ = p.currentPeek() === xs;
    return p.resetPeek(), $;
  }
  function N(p, c) {
    const { currentType: T } = c;
    if (T !== 8)
      return !1;
    v(p);
    const $ = p.currentPeek() === ".";
    return p.resetPeek(), $;
  }
  function A(p, c) {
    const { currentType: T } = c;
    if (T !== 9)
      return !1;
    v(p);
    const $ = f(p.currentPeek());
    return p.resetPeek(), $;
  }
  function D(p, c) {
    const { currentType: T } = c;
    if (!(T === 8 || T === 12))
      return !1;
    v(p);
    const $ = p.currentPeek() === ":";
    return p.resetPeek(), $;
  }
  function k(p, c) {
    const { currentType: T } = c;
    if (T !== 10)
      return !1;
    const $ = () => {
      const G = p.currentPeek();
      return G === "{" ? f(p.peek()) : G === "@" || G === "%" || G === "|" || G === ":" || G === "." || G === Me || !G ? !1 : G === fe ? (p.peek(), $()) : f(G);
    }, V = $();
    return p.resetPeek(), V;
  }
  function U(p) {
    v(p);
    const c = p.currentPeek() === "|";
    return p.resetPeek(), c;
  }
  function X(p) {
    const c = v(p), T = p.currentPeek() === "%" && p.peek() === "{";
    return p.resetPeek(), {
      isModulo: T,
      hasSpace: c.length > 0
    };
  }
  function W(p, c = !0) {
    const T = (V = !1, G = "", Le = !1) => {
      const Pe = p.currentPeek();
      return Pe === "{" ? G === "%" ? !1 : V : Pe === "@" || !Pe ? G === "%" ? !0 : V : Pe === "%" ? (p.peek(), T(V, "%", !0)) : Pe === "|" ? G === "%" || Le ? !0 : !(G === Me || G === fe) : Pe === Me ? (p.peek(), T(!0, Me, Le)) : Pe === fe ? (p.peek(), T(!0, fe, Le)) : !0;
    }, $ = T();
    return c && p.resetPeek(), $;
  }
  function te(p, c) {
    const T = p.currentChar();
    return T === He ? He : c(T) ? (p.next(), T) : null;
  }
  function ne(p) {
    return te(p, (T) => {
      const $ = T.charCodeAt(0);
      return $ >= 97 && $ <= 122 || // a-z
      $ >= 65 && $ <= 90 || // A-Z
      $ >= 48 && $ <= 57 || // 0-9
      $ === 95 || // _
      $ === 36;
    });
  }
  function ie(p) {
    return te(p, (T) => {
      const $ = T.charCodeAt(0);
      return $ >= 48 && $ <= 57;
    });
  }
  function J(p) {
    return te(p, (T) => {
      const $ = T.charCodeAt(0);
      return $ >= 48 && $ <= 57 || // 0-9
      $ >= 65 && $ <= 70 || // A-F
      $ >= 97 && $ <= 102;
    });
  }
  function le(p) {
    let c = "", T = "";
    for (; c = ie(p); )
      T += c;
    return T;
  }
  function Ze(p) {
    y(p);
    const c = p.currentChar();
    return c !== "%" && h(F.EXPECTED_TOKEN, a(), 0, c), p.next(), "%";
  }
  function ft(p) {
    let c = "";
    for (; ; ) {
      const T = p.currentChar();
      if (T === "{" || T === "}" || T === "@" || T === "|" || !T)
        break;
      if (T === "%")
        if (W(p))
          c += T, p.next();
        else
          break;
      else if (T === Me || T === fe)
        if (W(p))
          c += T, p.next();
        else {
          if (U(p))
            break;
          c += T, p.next();
        }
      else
        c += T, p.next();
    }
    return c;
  }
  function Nt(p) {
    y(p);
    let c = "", T = "";
    for (; c = ne(p); )
      T += c;
    return p.currentChar() === He && h(F.UNTERMINATED_CLOSING_BRACE, a(), 0), T;
  }
  function Ot(p) {
    y(p);
    let c = "";
    return p.currentChar() === "-" ? (p.next(), c += `-${le(p)}`) : c += le(p), p.currentChar() === He && h(F.UNTERMINATED_CLOSING_BRACE, a(), 0), c;
  }
  function be(p) {
    y(p), u(p, "'");
    let c = "", T = "";
    const $ = (G) => G !== xs && G !== fe;
    for (; c = te(p, $); )
      c === "\\" ? T += pt(p) : T += c;
    const V = p.currentChar();
    return V === fe || V === He ? (h(F.UNTERMINATED_SINGLE_QUOTE_IN_PLACEHOLDER, a(), 0), V === fe && (p.next(), u(p, "'")), T) : (u(p, "'"), T);
  }
  function pt(p) {
    const c = p.currentChar();
    switch (c) {
      case "\\":
      case "'":
        return p.next(), `\\${c}`;
      case "u":
        return mt(p, c, 4);
      case "U":
        return mt(p, c, 6);
      default:
        return h(F.UNKNOWN_ESCAPE_SEQUENCE, a(), 0, c), "";
    }
  }
  function mt(p, c, T) {
    u(p, c);
    let $ = "";
    for (let V = 0; V < T; V++) {
      const G = J(p);
      if (!G) {
        h(F.INVALID_UNICODE_ESCAPE_SEQUENCE, a(), 0, `\\${c}${$}${p.currentChar()}`);
        break;
      }
      $ += G;
    }
    return `\\${c}${$}`;
  }
  function kt(p) {
    y(p);
    let c = "", T = "";
    const $ = (V) => V !== "{" && V !== "}" && V !== Me && V !== fe;
    for (; c = te(p, $); )
      T += c;
    return T;
  }
  function At(p) {
    let c = "", T = "";
    for (; c = ne(p); )
      T += c;
    return T;
  }
  function $t(p) {
    const c = (T = !1, $) => {
      const V = p.currentChar();
      return V === "{" || V === "%" || V === "@" || V === "|" || V === "(" || V === ")" || !V || V === Me ? $ : V === fe || V === Zl ? ($ += V, p.next(), c(T, $)) : ($ += V, p.next(), c(!0, $));
    };
    return c(!1, "");
  }
  function ht(p) {
    y(p);
    const c = u(
      p,
      "|"
      /* TokenChars.Pipe */
    );
    return y(p), c;
  }
  function Je(p, c) {
    let T = null;
    switch (p.currentChar()) {
      case "{":
        return c.braceNest >= 1 && h(F.NOT_ALLOW_NEST_PLACEHOLDER, a(), 0), p.next(), T = m(
          c,
          2,
          "{"
          /* TokenChars.BraceLeft */
        ), y(p), c.braceNest++, T;
      case "}":
        return c.braceNest > 0 && c.currentType === 2 && h(F.EMPTY_PLACEHOLDER, a(), 0), p.next(), T = m(
          c,
          3,
          "}"
          /* TokenChars.BraceRight */
        ), c.braceNest--, c.braceNest > 0 && y(p), c.inLinked && c.braceNest === 0 && (c.inLinked = !1), T;
      case "@":
        return c.braceNest > 0 && h(F.UNTERMINATED_CLOSING_BRACE, a(), 0), T = Ue(p, c) || b(c), c.braceNest = 0, T;
      default: {
        let V = !0, G = !0, Le = !0;
        if (U(p))
          return c.braceNest > 0 && h(F.UNTERMINATED_CLOSING_BRACE, a(), 0), T = m(c, 1, ht(p)), c.braceNest = 0, c.inLinked = !1, T;
        if (c.braceNest > 0 && (c.currentType === 5 || c.currentType === 6 || c.currentType === 7))
          return h(F.UNTERMINATED_CLOSING_BRACE, a(), 0), c.braceNest = 0, Qe(p, c);
        if (V = w(p, c))
          return T = m(c, 5, Nt(p)), y(p), T;
        if (G = E(p, c))
          return T = m(c, 6, Ot(p)), y(p), T;
        if (Le = L(p, c))
          return T = m(c, 7, be(p)), y(p), T;
        if (!V && !G && !Le)
          return T = m(c, 13, kt(p)), h(F.INVALID_TOKEN_IN_PLACEHOLDER, a(), 0, T.value), y(p), T;
        break;
      }
    }
    return T;
  }
  function Ue(p, c) {
    const { currentType: T } = c;
    let $ = null;
    const V = p.currentChar();
    switch ((T === 8 || T === 9 || T === 12 || T === 10) && (V === fe || V === Me) && h(F.INVALID_LINKED_FORMAT, a(), 0), V) {
      case "@":
        return p.next(), $ = m(
          c,
          8,
          "@"
          /* TokenChars.LinkedAlias */
        ), c.inLinked = !0, $;
      case ".":
        return y(p), p.next(), m(
          c,
          9,
          "."
          /* TokenChars.LinkedDot */
        );
      case ":":
        return y(p), p.next(), m(
          c,
          10,
          ":"
          /* TokenChars.LinkedDelimiter */
        );
      default:
        return U(p) ? ($ = m(c, 1, ht(p)), c.braceNest = 0, c.inLinked = !1, $) : N(p, c) || D(p, c) ? (y(p), Ue(p, c)) : A(p, c) ? (y(p), m(c, 12, At(p))) : k(p, c) ? (y(p), V === "{" ? Je(p, c) || $ : m(c, 11, $t(p))) : (T === 8 && h(F.INVALID_LINKED_FORMAT, a(), 0), c.braceNest = 0, c.inLinked = !1, Qe(p, c));
    }
  }
  function Qe(p, c) {
    let T = {
      type: 14
      /* TokenTypes.EOF */
    };
    if (c.braceNest > 0)
      return Je(p, c) || b(c);
    if (c.inLinked)
      return Ue(p, c) || b(c);
    switch (p.currentChar()) {
      case "{":
        return Je(p, c) || b(c);
      case "}":
        return h(F.UNBALANCED_CLOSING_BRACE, a(), 0), p.next(), m(
          c,
          3,
          "}"
          /* TokenChars.BraceRight */
        );
      case "@":
        return Ue(p, c) || b(c);
      default: {
        if (U(p))
          return T = m(c, 1, ht(p)), c.braceNest = 0, c.inLinked = !1, T;
        const { isModulo: V, hasSpace: G } = X(p);
        if (V)
          return G ? m(c, 0, ft(p)) : m(c, 4, Ze(p));
        if (W(p))
          return m(c, 0, ft(p));
        break;
      }
    }
    return T;
  }
  function St() {
    const { currentType: p, offset: c, startLoc: T, endLoc: $ } = i;
    return i.lastType = p, i.lastOffset = c, i.lastStartLoc = T, i.lastEndLoc = $, i.offset = o(), i.startLoc = a(), s.currentChar() === He ? m(
      i,
      14
      /* TokenTypes.EOF */
    ) : Qe(s, i);
  }
  return {
    nextToken: St,
    currentOffset: o,
    currentPosition: a,
    context: d
  };
}
const eu = "parser", tu = /(?:\\\\|\\'|\\u([0-9a-fA-F]{4})|\\U([0-9a-fA-F]{6}))/g;
function nu(e, t, n) {
  switch (e) {
    case "\\\\":
      return "\\";
    case "\\'":
      return "'";
    default: {
      const s = parseInt(t || n, 16);
      return s <= 55295 || s >= 57344 ? String.fromCodePoint(s) : "�";
    }
  }
}
function su(e = {}) {
  const t = e.location !== !1, { onError: n } = e;
  function s(f, g, w, E, ...L) {
    const N = f.currentPosition();
    if (N.offset += E, N.column += E, n) {
      const A = t ? Un(w, N) : null, D = Lt(g, A, {
        domain: eu,
        args: L
      });
      n(D);
    }
  }
  function o(f, g, w) {
    const E = { type: f };
    return t && (E.start = g, E.end = g, E.loc = { start: w, end: w }), E;
  }
  function a(f, g, w, E) {
    E && (f.type = E), t && (f.end = g, f.loc && (f.loc.end = w));
  }
  function r(f, g) {
    const w = f.context(), E = o(3, w.offset, w.startLoc);
    return E.value = g, a(E, f.currentOffset(), f.currentPosition()), E;
  }
  function l(f, g) {
    const w = f.context(), { lastOffset: E, lastStartLoc: L } = w, N = o(5, E, L);
    return N.index = parseInt(g, 10), f.nextToken(), a(N, f.currentOffset(), f.currentPosition()), N;
  }
  function i(f, g) {
    const w = f.context(), { lastOffset: E, lastStartLoc: L } = w, N = o(4, E, L);
    return N.key = g, f.nextToken(), a(N, f.currentOffset(), f.currentPosition()), N;
  }
  function d(f, g) {
    const w = f.context(), { lastOffset: E, lastStartLoc: L } = w, N = o(9, E, L);
    return N.value = g.replace(tu, nu), f.nextToken(), a(N, f.currentOffset(), f.currentPosition()), N;
  }
  function _(f) {
    const g = f.nextToken(), w = f.context(), { lastOffset: E, lastStartLoc: L } = w, N = o(8, E, L);
    return g.type !== 12 ? (s(f, F.UNEXPECTED_EMPTY_LINKED_MODIFIER, w.lastStartLoc, 0), N.value = "", a(N, E, L), {
      nextConsumeToken: g,
      node: N
    }) : (g.value == null && s(f, F.UNEXPECTED_LEXICAL_ANALYSIS, w.lastStartLoc, 0, Ne(g)), N.value = g.value || "", a(N, f.currentOffset(), f.currentPosition()), {
      node: N
    });
  }
  function h(f, g) {
    const w = f.context(), E = o(7, w.offset, w.startLoc);
    return E.value = g, a(E, f.currentOffset(), f.currentPosition()), E;
  }
  function m(f) {
    const g = f.context(), w = o(6, g.offset, g.startLoc);
    let E = f.nextToken();
    if (E.type === 9) {
      const L = _(f);
      w.modifier = L.node, E = L.nextConsumeToken || f.nextToken();
    }
    switch (E.type !== 10 && s(f, F.UNEXPECTED_LEXICAL_ANALYSIS, g.lastStartLoc, 0, Ne(E)), E = f.nextToken(), E.type === 2 && (E = f.nextToken()), E.type) {
      case 11:
        E.value == null && s(f, F.UNEXPECTED_LEXICAL_ANALYSIS, g.lastStartLoc, 0, Ne(E)), w.key = h(f, E.value || "");
        break;
      case 5:
        E.value == null && s(f, F.UNEXPECTED_LEXICAL_ANALYSIS, g.lastStartLoc, 0, Ne(E)), w.key = i(f, E.value || "");
        break;
      case 6:
        E.value == null && s(f, F.UNEXPECTED_LEXICAL_ANALYSIS, g.lastStartLoc, 0, Ne(E)), w.key = l(f, E.value || "");
        break;
      case 7:
        E.value == null && s(f, F.UNEXPECTED_LEXICAL_ANALYSIS, g.lastStartLoc, 0, Ne(E)), w.key = d(f, E.value || "");
        break;
      default: {
        s(f, F.UNEXPECTED_EMPTY_LINKED_KEY, g.lastStartLoc, 0);
        const L = f.context(), N = o(7, L.offset, L.startLoc);
        return N.value = "", a(N, L.offset, L.startLoc), w.key = N, a(w, L.offset, L.startLoc), {
          nextConsumeToken: E,
          node: w
        };
      }
    }
    return a(w, f.currentOffset(), f.currentPosition()), {
      node: w
    };
  }
  function b(f) {
    const g = f.context(), w = g.currentType === 1 ? f.currentOffset() : g.offset, E = g.currentType === 1 ? g.endLoc : g.startLoc, L = o(2, w, E);
    L.items = [];
    let N = null;
    do {
      const k = N || f.nextToken();
      switch (N = null, k.type) {
        case 0:
          k.value == null && s(f, F.UNEXPECTED_LEXICAL_ANALYSIS, g.lastStartLoc, 0, Ne(k)), L.items.push(r(f, k.value || ""));
          break;
        case 6:
          k.value == null && s(f, F.UNEXPECTED_LEXICAL_ANALYSIS, g.lastStartLoc, 0, Ne(k)), L.items.push(l(f, k.value || ""));
          break;
        case 5:
          k.value == null && s(f, F.UNEXPECTED_LEXICAL_ANALYSIS, g.lastStartLoc, 0, Ne(k)), L.items.push(i(f, k.value || ""));
          break;
        case 7:
          k.value == null && s(f, F.UNEXPECTED_LEXICAL_ANALYSIS, g.lastStartLoc, 0, Ne(k)), L.items.push(d(f, k.value || ""));
          break;
        case 8: {
          const U = m(f);
          L.items.push(U.node), N = U.nextConsumeToken || null;
          break;
        }
      }
    } while (g.currentType !== 14 && g.currentType !== 1);
    const A = g.currentType === 1 ? g.lastOffset : f.currentOffset(), D = g.currentType === 1 ? g.lastEndLoc : f.currentPosition();
    return a(L, A, D), L;
  }
  function u(f, g, w, E) {
    const L = f.context();
    let N = E.items.length === 0;
    const A = o(1, g, w);
    A.cases = [], A.cases.push(E);
    do {
      const D = b(f);
      N || (N = D.items.length === 0), A.cases.push(D);
    } while (L.currentType !== 14);
    return N && s(f, F.MUST_HAVE_MESSAGES_IN_PLURAL, w, 0), a(A, f.currentOffset(), f.currentPosition()), A;
  }
  function v(f) {
    const g = f.context(), { offset: w, startLoc: E } = g, L = b(f);
    return g.currentType === 14 ? L : u(f, w, E, L);
  }
  function y(f) {
    const g = Ql(f, Yo({}, e)), w = g.context(), E = o(0, w.offset, w.startLoc);
    return t && E.loc && (E.loc.source = f), E.body = v(g), e.onCacheKey && (E.cacheKey = e.onCacheKey(f)), w.currentType !== 14 && s(g, F.UNEXPECTED_LEXICAL_ANALYSIS, w.lastStartLoc, 0, f[w.offset] || ""), a(E, g.currentOffset(), g.currentPosition()), E;
  }
  return { parse: y };
}
function Ne(e) {
  if (e.type === 14)
    return "EOF";
  const t = (e.value || "").replace(/\r?\n/gu, "\\n");
  return t.length > 10 ? t.slice(0, 9) + "…" : t;
}
function ou(e, t = {}) {
  const n = {
    ast: e,
    helpers: /* @__PURE__ */ new Set()
  };
  return { context: () => n, helper: (a) => (n.helpers.add(a), a) };
}
function Hs(e, t) {
  for (let n = 0; n < e.length; n++)
    ls(e[n], t);
}
function ls(e, t) {
  switch (e.type) {
    case 1:
      Hs(e.cases, t), t.helper(
        "plural"
        /* HelperNameMap.PLURAL */
      );
      break;
    case 2:
      Hs(e.items, t);
      break;
    case 6: {
      ls(e.key, t), t.helper(
        "linked"
        /* HelperNameMap.LINKED */
      ), t.helper(
        "type"
        /* HelperNameMap.TYPE */
      );
      break;
    }
    case 5:
      t.helper(
        "interpolate"
        /* HelperNameMap.INTERPOLATE */
      ), t.helper(
        "list"
        /* HelperNameMap.LIST */
      );
      break;
    case 4:
      t.helper(
        "interpolate"
        /* HelperNameMap.INTERPOLATE */
      ), t.helper(
        "named"
        /* HelperNameMap.NAMED */
      );
      break;
  }
}
function au(e, t = {}) {
  const n = ou(e);
  n.helper(
    "normalize"
    /* HelperNameMap.NORMALIZE */
  ), e.body && ls(e.body, n);
  const s = n.context();
  e.helpers = Array.from(s.helpers);
}
function ru(e) {
  const t = e.body;
  return t.type === 2 ? Ws(t) : t.cases.forEach((n) => Ws(n)), e;
}
function Ws(e) {
  if (e.items.length === 1) {
    const t = e.items[0];
    (t.type === 3 || t.type === 9) && (e.static = t.value, delete t.value);
  } else {
    const t = [];
    for (let n = 0; n < e.items.length; n++) {
      const s = e.items[n];
      if (!(s.type === 3 || s.type === 9) || s.value == null)
        break;
      t.push(s.value);
    }
    if (t.length === e.items.length) {
      e.static = jo(t);
      for (let n = 0; n < e.items.length; n++) {
        const s = e.items[n];
        (s.type === 3 || s.type === 9) && delete s.value;
      }
    }
  }
}
const iu = "minifier";
function gt(e) {
  switch (e.t = e.type, e.type) {
    case 0: {
      const t = e;
      gt(t.body), t.b = t.body, delete t.body;
      break;
    }
    case 1: {
      const t = e, n = t.cases;
      for (let s = 0; s < n.length; s++)
        gt(n[s]);
      t.c = n, delete t.cases;
      break;
    }
    case 2: {
      const t = e, n = t.items;
      for (let s = 0; s < n.length; s++)
        gt(n[s]);
      t.i = n, delete t.items, t.static && (t.s = t.static, delete t.static);
      break;
    }
    case 3:
    case 9:
    case 8:
    case 7: {
      const t = e;
      t.value && (t.v = t.value, delete t.value);
      break;
    }
    case 6: {
      const t = e;
      gt(t.key), t.k = t.key, delete t.key, t.modifier && (gt(t.modifier), t.m = t.modifier, delete t.modifier);
      break;
    }
    case 5: {
      const t = e;
      t.i = t.index, delete t.index;
      break;
    }
    case 4: {
      const t = e;
      t.k = t.key, delete t.key;
      break;
    }
    default:
      throw Lt(F.UNHANDLED_MINIFIER_NODE_TYPE, null, {
        domain: iu,
        args: [e.type]
      });
  }
  delete e.type;
}
const lu = "parser";
function uu(e, t) {
  const { sourceMap: n, filename: s, breakLineCode: o, needIndent: a } = t, r = t.location !== !1, l = {
    filename: s,
    code: "",
    column: 1,
    line: 1,
    offset: 0,
    map: void 0,
    breakLineCode: o,
    needIndent: a,
    indentLevel: 0
  };
  r && e.loc && (l.source = e.loc.source);
  const i = () => l;
  function d(y, f) {
    l.code += y;
  }
  function _(y, f = !0) {
    const g = f ? o : "";
    d(a ? g + "  ".repeat(y) : g);
  }
  function h(y = !0) {
    const f = ++l.indentLevel;
    y && _(f);
  }
  function m(y = !0) {
    const f = --l.indentLevel;
    y && _(f);
  }
  function b() {
    _(l.indentLevel);
  }
  return {
    context: i,
    push: d,
    indent: h,
    deindent: m,
    newline: b,
    helper: (y) => `_${y}`,
    needIndent: () => l.needIndent
  };
}
function cu(e, t) {
  const { helper: n } = e;
  e.push(`${n(
    "linked"
    /* HelperNameMap.LINKED */
  )}(`), Ct(e, t.key), t.modifier ? (e.push(", "), Ct(e, t.modifier), e.push(", _type")) : e.push(", undefined, _type"), e.push(")");
}
function du(e, t) {
  const { helper: n, needIndent: s } = e;
  e.push(`${n(
    "normalize"
    /* HelperNameMap.NORMALIZE */
  )}([`), e.indent(s());
  const o = t.items.length;
  for (let a = 0; a < o && (Ct(e, t.items[a]), a !== o - 1); a++)
    e.push(", ");
  e.deindent(s()), e.push("])");
}
function fu(e, t) {
  const { helper: n, needIndent: s } = e;
  if (t.cases.length > 1) {
    e.push(`${n(
      "plural"
      /* HelperNameMap.PLURAL */
    )}([`), e.indent(s());
    const o = t.cases.length;
    for (let a = 0; a < o && (Ct(e, t.cases[a]), a !== o - 1); a++)
      e.push(", ");
    e.deindent(s()), e.push("])");
  }
}
function pu(e, t) {
  t.body ? Ct(e, t.body) : e.push("null");
}
function Ct(e, t) {
  const { helper: n } = e;
  switch (t.type) {
    case 0:
      pu(e, t);
      break;
    case 1:
      fu(e, t);
      break;
    case 2:
      du(e, t);
      break;
    case 6:
      cu(e, t);
      break;
    case 8:
      e.push(JSON.stringify(t.value), t);
      break;
    case 7:
      e.push(JSON.stringify(t.value), t);
      break;
    case 5:
      e.push(`${n(
        "interpolate"
        /* HelperNameMap.INTERPOLATE */
      )}(${n(
        "list"
        /* HelperNameMap.LIST */
      )}(${t.index}))`, t);
      break;
    case 4:
      e.push(`${n(
        "interpolate"
        /* HelperNameMap.INTERPOLATE */
      )}(${n(
        "named"
        /* HelperNameMap.NAMED */
      )}(${JSON.stringify(t.key)}))`, t);
      break;
    case 9:
      e.push(JSON.stringify(t.value), t);
      break;
    case 3:
      e.push(JSON.stringify(t.value), t);
      break;
    default:
      throw Lt(F.UNHANDLED_CODEGEN_NODE_TYPE, null, {
        domain: lu,
        args: [t.type]
      });
  }
}
const mu = (e, t = {}) => {
  const n = Us(t.mode) ? t.mode : "normal", s = Us(t.filename) ? t.filename : "message.intl", o = !!t.sourceMap, a = t.breakLineCode != null ? t.breakLineCode : n === "arrow" ? ";" : `
`, r = t.needIndent ? t.needIndent : n !== "arrow", l = e.helpers || [], i = uu(e, {
    mode: n,
    filename: s,
    sourceMap: o,
    breakLineCode: a,
    needIndent: r
  });
  i.push(n === "normal" ? "function __msg__ (ctx) {" : "(ctx) => {"), i.indent(r), l.length > 0 && (i.push(`const { ${jo(l.map((h) => `${h}: _${h}`), ", ")} } = ctx`), i.newline()), i.push("return "), Ct(i, e), i.deindent(r), i.push("}"), delete e.helpers;
  const { code: d, map: _ } = i.context();
  return {
    ast: e,
    code: d,
    map: _ ? _.toJSON() : void 0
    // eslint-disable-line @typescript-eslint/no-explicit-any
  };
};
function hu(e, t = {}) {
  const n = Yo({}, t), s = !!n.jit, o = !!n.minify, a = n.optimize == null ? !0 : n.optimize, l = su(n).parse(e);
  return s ? (a && ru(l), o && gt(l), { ast: l, code: "" }) : (au(l, n), mu(l, n));
}
/*!
  * core-base v9.10.2
  * (c) 2024 kazuya kawaguchi
  * Released under the MIT License.
  */
function _u() {
  typeof __INTLIFY_PROD_DEVTOOLS__ != "boolean" && (De().__INTLIFY_PROD_DEVTOOLS__ = !1), typeof __INTLIFY_JIT_COMPILATION__ != "boolean" && (De().__INTLIFY_JIT_COMPILATION__ = !1), typeof __INTLIFY_DROP_MESSAGE_COMPILER__ != "boolean" && (De().__INTLIFY_DROP_MESSAGE_COMPILER__ = !1);
}
const Xe = [];
Xe[
  0
  /* States.BEFORE_PATH */
] = {
  w: [
    0
    /* States.BEFORE_PATH */
  ],
  i: [
    3,
    0
    /* Actions.APPEND */
  ],
  "[": [
    4
    /* States.IN_SUB_PATH */
  ],
  o: [
    7
    /* States.AFTER_PATH */
  ]
};
Xe[
  1
  /* States.IN_PATH */
] = {
  w: [
    1
    /* States.IN_PATH */
  ],
  ".": [
    2
    /* States.BEFORE_IDENT */
  ],
  "[": [
    4
    /* States.IN_SUB_PATH */
  ],
  o: [
    7
    /* States.AFTER_PATH */
  ]
};
Xe[
  2
  /* States.BEFORE_IDENT */
] = {
  w: [
    2
    /* States.BEFORE_IDENT */
  ],
  i: [
    3,
    0
    /* Actions.APPEND */
  ],
  0: [
    3,
    0
    /* Actions.APPEND */
  ]
};
Xe[
  3
  /* States.IN_IDENT */
] = {
  i: [
    3,
    0
    /* Actions.APPEND */
  ],
  0: [
    3,
    0
    /* Actions.APPEND */
  ],
  w: [
    1,
    1
    /* Actions.PUSH */
  ],
  ".": [
    2,
    1
    /* Actions.PUSH */
  ],
  "[": [
    4,
    1
    /* Actions.PUSH */
  ],
  o: [
    7,
    1
    /* Actions.PUSH */
  ]
};
Xe[
  4
  /* States.IN_SUB_PATH */
] = {
  "'": [
    5,
    0
    /* Actions.APPEND */
  ],
  '"': [
    6,
    0
    /* Actions.APPEND */
  ],
  "[": [
    4,
    2
    /* Actions.INC_SUB_PATH_DEPTH */
  ],
  "]": [
    1,
    3
    /* Actions.PUSH_SUB_PATH */
  ],
  o: 8,
  l: [
    4,
    0
    /* Actions.APPEND */
  ]
};
Xe[
  5
  /* States.IN_SINGLE_QUOTE */
] = {
  "'": [
    4,
    0
    /* Actions.APPEND */
  ],
  o: 8,
  l: [
    5,
    0
    /* Actions.APPEND */
  ]
};
Xe[
  6
  /* States.IN_DOUBLE_QUOTE */
] = {
  '"': [
    4,
    0
    /* Actions.APPEND */
  ],
  o: 8,
  l: [
    6,
    0
    /* Actions.APPEND */
  ]
};
const gu = /^\s?(?:true|false|-?[\d.]+|'[^']*'|"[^"]*")\s?$/;
function vu(e) {
  return gu.test(e);
}
function yu(e) {
  const t = e.charCodeAt(0), n = e.charCodeAt(e.length - 1);
  return t === n && (t === 34 || t === 39) ? e.slice(1, -1) : e;
}
function Eu(e) {
  if (e == null)
    return "o";
  switch (e.charCodeAt(0)) {
    case 91:
    case 93:
    case 46:
    case 34:
    case 39:
      return e;
    case 95:
    case 36:
    case 45:
      return "i";
    case 9:
    case 10:
    case 13:
    case 160:
    case 65279:
    case 8232:
    case 8233:
      return "w";
  }
  return "i";
}
function bu(e) {
  const t = e.trim();
  return e.charAt(0) === "0" && isNaN(parseInt(e)) ? !1 : vu(t) ? yu(t) : "*" + t;
}
function Tu(e) {
  const t = [];
  let n = -1, s = 0, o = 0, a, r, l, i, d, _, h;
  const m = [];
  m[
    0
    /* Actions.APPEND */
  ] = () => {
    r === void 0 ? r = l : r += l;
  }, m[
    1
    /* Actions.PUSH */
  ] = () => {
    r !== void 0 && (t.push(r), r = void 0);
  }, m[
    2
    /* Actions.INC_SUB_PATH_DEPTH */
  ] = () => {
    m[
      0
      /* Actions.APPEND */
    ](), o++;
  }, m[
    3
    /* Actions.PUSH_SUB_PATH */
  ] = () => {
    if (o > 0)
      o--, s = 4, m[
        0
        /* Actions.APPEND */
      ]();
    else {
      if (o = 0, r === void 0 || (r = bu(r), r === !1))
        return !1;
      m[
        1
        /* Actions.PUSH */
      ]();
    }
  };
  function b() {
    const u = e[n + 1];
    if (s === 5 && u === "'" || s === 6 && u === '"')
      return n++, l = "\\" + u, m[
        0
        /* Actions.APPEND */
      ](), !0;
  }
  for (; s !== null; )
    if (n++, a = e[n], !(a === "\\" && b())) {
      if (i = Eu(a), h = Xe[s], d = h[i] || h.l || 8, d === 8 || (s = d[0], d[1] !== void 0 && (_ = m[d[1]], _ && (l = a, _() === !1))))
        return;
      if (s === 7)
        return t;
    }
}
const Gs = /* @__PURE__ */ new Map();
function wu(e, t) {
  return Y(e) ? e[t] : null;
}
function Cu(e, t) {
  if (!Y(e))
    return null;
  let n = Gs.get(t);
  if (n || (n = Tu(t), n && Gs.set(t, n)), !n)
    return null;
  const s = n.length;
  let o = e, a = 0;
  for (; a < s; ) {
    const r = o[n[a]];
    if (r === void 0 || K(o))
      return null;
    o = r, a++;
  }
  return o;
}
const Lu = (e) => e, Nu = (e) => "", Ou = "text", ku = (e) => e.length === 0 ? "" : Dl(e), Au = Rl;
function zs(e, t) {
  return e = Math.abs(e), t === 2 ? e ? e > 1 ? 1 : 0 : 1 : e ? Math.min(e, 2) : 0;
}
function $u(e) {
  const t = ce(e.pluralIndex) ? e.pluralIndex : -1;
  return e.named && (ce(e.named.count) || ce(e.named.n)) ? ce(e.named.count) ? e.named.count : ce(e.named.n) ? e.named.n : t : t;
}
function Su(e, t) {
  t.count || (t.count = e), t.n || (t.n = e);
}
function Iu(e = {}) {
  const t = e.locale, n = $u(e), s = Y(e.pluralRules) && I(t) && K(e.pluralRules[t]) ? e.pluralRules[t] : zs, o = Y(e.pluralRules) && I(t) && K(e.pluralRules[t]) ? zs : void 0, a = (f) => f[s(n, f.length, o)], r = e.list || [], l = (f) => r[f], i = e.named || {};
  ce(e.pluralIndex) && Su(n, i);
  const d = (f) => i[f];
  function _(f) {
    const g = K(e.messages) ? e.messages(f) : Y(e.messages) ? e.messages[f] : !1;
    return g || (e.parent ? e.parent.message(f) : Nu);
  }
  const h = (f) => e.modifiers ? e.modifiers[f] : Lu, m = x(e.processor) && K(e.processor.normalize) ? e.processor.normalize : ku, b = x(e.processor) && K(e.processor.interpolate) ? e.processor.interpolate : Au, u = x(e.processor) && I(e.processor.type) ? e.processor.type : Ou, y = {
    list: l,
    named: d,
    plural: a,
    linked: (f, ...g) => {
      const [w, E] = g;
      let L = "text", N = "";
      g.length === 1 ? Y(w) ? (N = w.modifier || N, L = w.type || L) : I(w) && (N = w || N) : g.length === 2 && (I(w) && (N = w || N), I(E) && (L = E || L));
      const A = _(f)(y), D = (
        // The message in vnode resolved with linked are returned as an array by processor.nomalize
        L === "vnode" && oe(A) && N ? A[0] : A
      );
      return N ? h(N)(D, L) : D;
    },
    message: _,
    type: u,
    interpolate: b,
    normalize: m,
    values: de({}, r, i)
  };
  return y;
}
let zt = null;
function Pu(e) {
  zt = e;
}
function Mu(e, t, n) {
  zt && zt.emit("i18n:init", {
    timestamp: Date.now(),
    i18n: e,
    version: t,
    meta: n
  });
}
const Ru = /* @__PURE__ */ Du(
  "function:translate"
  /* IntlifyDevToolsHooks.FunctionTranslate */
);
function Du(e) {
  return (t) => zt && zt.emit(e, t);
}
const me = {
  NOT_FOUND_KEY: 1,
  FALLBACK_TO_TRANSLATE: 2,
  CANNOT_FORMAT_NUMBER: 3,
  FALLBACK_TO_NUMBER_FORMAT: 4,
  CANNOT_FORMAT_DATE: 5,
  FALLBACK_TO_DATE_FORMAT: 6,
  EXPERIMENTAL_CUSTOM_MESSAGE_COMPILER: 7,
  __EXTEND_POINT__: 8
}, Fu = {
  [me.NOT_FOUND_KEY]: "Not found '{key}' key in '{locale}' locale messages.",
  [me.FALLBACK_TO_TRANSLATE]: "Fall back to translate '{key}' key with '{target}' locale.",
  [me.CANNOT_FORMAT_NUMBER]: "Cannot format a number value due to not supported Intl.NumberFormat.",
  [me.FALLBACK_TO_NUMBER_FORMAT]: "Fall back to number format '{key}' key with '{target}' locale.",
  [me.CANNOT_FORMAT_DATE]: "Cannot format a date value due to not supported Intl.DateTimeFormat.",
  [me.FALLBACK_TO_DATE_FORMAT]: "Fall back to datetime format '{key}' key with '{target}' locale.",
  [me.EXPERIMENTAL_CUSTOM_MESSAGE_COMPILER]: "This project is using Custom Message Compiler, which is an experimental feature. It may receive breaking changes or be removed in the future."
};
function lt(e, ...t) {
  return rs(Fu[e], ...t);
}
const qo = F.__EXTEND_POINT__, nt = is(qo), re = {
  INVALID_ARGUMENT: qo,
  // 18
  INVALID_DATE_ARGUMENT: nt(),
  // 19
  INVALID_ISO_DATE_ARGUMENT: nt(),
  // 20
  NOT_SUPPORT_NON_STRING_MESSAGE: nt(),
  // 21
  NOT_SUPPORT_LOCALE_PROMISE_VALUE: nt(),
  // 22
  NOT_SUPPORT_LOCALE_ASYNC_FUNCTION: nt(),
  // 23
  NOT_SUPPORT_LOCALE_TYPE: nt(),
  // 24
  __EXTEND_POINT__: nt()
  // 25
};
function Oe(e) {
  return Lt(e, null, process.env.NODE_ENV !== "production" ? { messages: Vu } : void 0);
}
const Vu = {
  [re.INVALID_ARGUMENT]: "Invalid arguments",
  [re.INVALID_DATE_ARGUMENT]: "The date provided is an invalid Date object.Make sure your Date represents a valid date.",
  [re.INVALID_ISO_DATE_ARGUMENT]: "The argument provided is not a valid ISO date string",
  [re.NOT_SUPPORT_NON_STRING_MESSAGE]: "Not support non-string message",
  [re.NOT_SUPPORT_LOCALE_PROMISE_VALUE]: "cannot support promise value",
  [re.NOT_SUPPORT_LOCALE_ASYNC_FUNCTION]: "cannot support async function",
  [re.NOT_SUPPORT_LOCALE_TYPE]: "cannot support locale type"
};
function us(e, t) {
  return t.locale != null ? Ys(t.locale) : Ys(e.locale);
}
let $n;
function Ys(e) {
  if (I(e))
    return e;
  if (K(e)) {
    if (e.resolvedOnce && $n != null)
      return $n;
    if (e.constructor.name === "Function") {
      const t = e();
      if (Ml(t))
        throw Oe(re.NOT_SUPPORT_LOCALE_PROMISE_VALUE);
      return $n = t;
    } else
      throw Oe(re.NOT_SUPPORT_LOCALE_ASYNC_FUNCTION);
  } else
    throw Oe(re.NOT_SUPPORT_LOCALE_TYPE);
}
function Bu(e, t, n) {
  return [.../* @__PURE__ */ new Set([
    n,
    ...oe(t) ? t : Y(t) ? Object.keys(t) : I(t) ? [t] : [n]
  ])];
}
function Ko(e, t, n) {
  const s = I(n) ? n : Yt, o = e;
  o.__localeChainCache || (o.__localeChainCache = /* @__PURE__ */ new Map());
  let a = o.__localeChainCache.get(s);
  if (!a) {
    a = [];
    let r = [n];
    for (; oe(r); )
      r = js(a, r, t);
    const l = oe(t) || !x(t) ? t : t.default ? t.default : null;
    r = I(l) ? [l] : l, oe(r) && js(a, r, !1), o.__localeChainCache.set(s, a);
  }
  return a;
}
function js(e, t, n) {
  let s = !0;
  for (let o = 0; o < t.length && q(s); o++) {
    const a = t[o];
    I(a) && (s = Uu(e, t[o], n));
  }
  return s;
}
function Uu(e, t, n) {
  let s;
  const o = t.split("-");
  do {
    const a = o.join("-");
    s = xu(e, a, n), o.splice(-1, 1);
  } while (o.length && s === !0);
  return s;
}
function xu(e, t, n) {
  let s = !1;
  if (!e.includes(t) && (s = !0, t)) {
    s = t[t.length - 1] !== "!";
    const o = t.replace(/!/g, "");
    e.push(o), (oe(n) || x(n)) && n[o] && (s = n[o]);
  }
  return s;
}
const Hu = "9.10.2", wn = -1, Yt = "en-US", hn = "", qs = (e) => `${e.charAt(0).toLocaleUpperCase()}${e.substr(1)}`;
function Wu() {
  return {
    upper: (e, t) => t === "text" && I(e) ? e.toUpperCase() : t === "vnode" && Y(e) && "__v_isVNode" in e ? e.children.toUpperCase() : e,
    lower: (e, t) => t === "text" && I(e) ? e.toLowerCase() : t === "vnode" && Y(e) && "__v_isVNode" in e ? e.children.toLowerCase() : e,
    capitalize: (e, t) => t === "text" && I(e) ? qs(e) : t === "vnode" && Y(e) && "__v_isVNode" in e ? qs(e.children) : e
  };
}
let Xo;
function Ks(e) {
  Xo = e;
}
let Zo;
function Gu(e) {
  Zo = e;
}
let Jo;
function zu(e) {
  Jo = e;
}
let Qo = null;
const Yu = /* @__NO_SIDE_EFFECTS__ */ (e) => {
  Qo = e;
}, ju = /* @__NO_SIDE_EFFECTS__ */ () => Qo;
let ea = null;
const Xs = (e) => {
  ea = e;
}, qu = () => ea;
let Zs = 0;
function Ku(e = {}) {
  const t = K(e.onWarn) ? e.onWarn : Ke, n = I(e.version) ? e.version : Hu, s = I(e.locale) || K(e.locale) ? e.locale : Yt, o = K(s) ? Yt : s, a = oe(e.fallbackLocale) || x(e.fallbackLocale) || I(e.fallbackLocale) || e.fallbackLocale === !1 ? e.fallbackLocale : o, r = x(e.messages) ? e.messages : { [o]: {} }, l = x(e.datetimeFormats) ? e.datetimeFormats : { [o]: {} }, i = x(e.numberFormats) ? e.numberFormats : { [o]: {} }, d = de({}, e.modifiers || {}, Wu()), _ = e.pluralRules || {}, h = K(e.missing) ? e.missing : null, m = q(e.missingWarn) || wt(e.missingWarn) ? e.missingWarn : !0, b = q(e.fallbackWarn) || wt(e.fallbackWarn) ? e.fallbackWarn : !0, u = !!e.fallbackFormat, v = !!e.unresolving, y = K(e.postTranslation) ? e.postTranslation : null, f = x(e.processor) ? e.processor : null, g = q(e.warnHtmlMessage) ? e.warnHtmlMessage : !0, w = !!e.escapeParameter, E = K(e.messageCompiler) ? e.messageCompiler : Xo;
  process.env.NODE_ENV !== "production" && K(e.messageCompiler) && zo(lt(me.EXPERIMENTAL_CUSTOM_MESSAGE_COMPILER));
  const L = K(e.messageResolver) ? e.messageResolver : Zo || wu, N = K(e.localeFallbacker) ? e.localeFallbacker : Jo || Bu, A = Y(e.fallbackContext) ? e.fallbackContext : void 0, D = e, k = Y(D.__datetimeFormatters) ? D.__datetimeFormatters : /* @__PURE__ */ new Map(), U = Y(D.__numberFormatters) ? D.__numberFormatters : /* @__PURE__ */ new Map(), X = Y(D.__meta) ? D.__meta : {};
  Zs++;
  const W = {
    version: n,
    cid: Zs,
    locale: s,
    fallbackLocale: a,
    messages: r,
    modifiers: d,
    pluralRules: _,
    missing: h,
    missingWarn: m,
    fallbackWarn: b,
    fallbackFormat: u,
    unresolving: v,
    postTranslation: y,
    processor: f,
    warnHtmlMessage: g,
    escapeParameter: w,
    messageCompiler: E,
    messageResolver: L,
    localeFallbacker: N,
    fallbackContext: A,
    onWarn: t,
    __meta: X
  };
  return W.datetimeFormats = l, W.numberFormats = i, W.__datetimeFormatters = k, W.__numberFormatters = U, process.env.NODE_ENV !== "production" && (W.__v_emitter = D.__v_emitter != null ? D.__v_emitter : void 0), (process.env.NODE_ENV !== "production" || __INTLIFY_PROD_DEVTOOLS__) && Mu(W, n, X), W;
}
function Cn(e, t) {
  return e instanceof RegExp ? e.test(t) : e;
}
function ta(e, t) {
  return e instanceof RegExp ? e.test(t) : e;
}
function cs(e, t, n, s, o) {
  const { missing: a, onWarn: r } = e;
  if (process.env.NODE_ENV !== "production") {
    const l = e.__v_emitter;
    l && l.emit("missing", {
      locale: n,
      key: t,
      type: o,
      groupId: `${o}:${t}`
    });
  }
  if (a !== null) {
    const l = a(e, n, t, o);
    return I(l) ? l : t;
  } else
    return process.env.NODE_ENV !== "production" && ta(s, t) && r(lt(me.NOT_FOUND_KEY, { key: t, locale: n })), t;
}
function It(e, t, n) {
  const s = e;
  s.__localeChainCache = /* @__PURE__ */ new Map(), e.localeFallbacker(e, n, t);
}
function Sn(e) {
  return (n) => Xu(n, e);
}
function Xu(e, t) {
  const n = t.b || t.body;
  if ((n.t || n.type) === 1) {
    const s = n, o = s.c || s.cases;
    return e.plural(o.reduce((a, r) => [
      ...a,
      Js(e, r)
    ], []));
  } else
    return Js(e, n);
}
function Js(e, t) {
  const n = t.s || t.static;
  if (n)
    return e.type === "text" ? n : e.normalize([n]);
  {
    const s = (t.i || t.items).reduce((o, a) => [...o, xn(e, a)], []);
    return e.normalize(s);
  }
}
function xn(e, t) {
  const n = t.t || t.type;
  switch (n) {
    case 3: {
      const s = t;
      return s.v || s.value;
    }
    case 9: {
      const s = t;
      return s.v || s.value;
    }
    case 4: {
      const s = t;
      return e.interpolate(e.named(s.k || s.key));
    }
    case 5: {
      const s = t;
      return e.interpolate(e.list(s.i != null ? s.i : s.index));
    }
    case 6: {
      const s = t, o = s.m || s.modifier;
      return e.linked(xn(e, s.k || s.key), o ? xn(e, o) : void 0, e.type);
    }
    case 7: {
      const s = t;
      return s.v || s.value;
    }
    case 8: {
      const s = t;
      return s.v || s.value;
    }
    default:
      throw new Error(`unhandled node type on format message part: ${n}`);
  }
}
const Zu = "Detected HTML in '{source}' message. Recommend not using HTML messages to avoid XSS.";
function na(e, t) {
  t && Yl(e) && Ke(rs(Zu, { source: e }));
}
const sa = (e) => e;
let vt = /* @__PURE__ */ Object.create(null);
const ut = (e) => Y(e) && (e.t === 0 || e.type === 0) && ("b" in e || "body" in e);
function oa(e, t = {}) {
  let n = !1;
  const s = t.onError || Gl;
  return t.onError = (o) => {
    n = !0, s(o);
  }, { ...hu(e, t), detectError: n };
}
const Ju = /* @__NO_SIDE_EFFECTS__ */ (e, t) => {
  if (!I(e))
    throw Oe(re.NOT_SUPPORT_NON_STRING_MESSAGE);
  {
    const n = q(t.warnHtmlMessage) ? t.warnHtmlMessage : !0;
    process.env.NODE_ENV !== "production" && na(e, n);
    const o = (t.onCacheKey || sa)(e), a = vt[o];
    if (a)
      return a;
    const { code: r, detectError: l } = oa(e, t), i = new Function(`return ${r}`)();
    return l ? i : vt[o] = i;
  }
};
function Qu(e, t) {
  if (__INTLIFY_JIT_COMPILATION__ && !__INTLIFY_DROP_MESSAGE_COMPILER__ && I(e)) {
    const n = q(t.warnHtmlMessage) ? t.warnHtmlMessage : !0;
    process.env.NODE_ENV !== "production" && na(e, n);
    const o = (t.onCacheKey || sa)(e), a = vt[o];
    if (a)
      return a;
    const { ast: r, detectError: l } = oa(e, {
      ...t,
      location: process.env.NODE_ENV !== "production",
      jit: !0
    }), i = Sn(r);
    return l ? i : vt[o] = i;
  } else {
    if (process.env.NODE_ENV !== "production" && !ut(e))
      return Ke(`the message that is resolve with key '${t.key}' is not supported for jit compilation`), () => e;
    const n = e.cacheKey;
    if (n) {
      const s = vt[n];
      return s || (vt[n] = Sn(e));
    } else
      return Sn(e);
  }
}
const Qs = () => "", ve = (e) => K(e);
function eo(e, ...t) {
  const { fallbackFormat: n, postTranslation: s, unresolving: o, messageCompiler: a, fallbackLocale: r, messages: l } = e, [i, d] = Hn(...t), _ = q(d.missingWarn) ? d.missingWarn : e.missingWarn, h = q(d.fallbackWarn) ? d.fallbackWarn : e.fallbackWarn, m = q(d.escapeParameter) ? d.escapeParameter : e.escapeParameter, b = !!d.resolvedMessage, u = I(d.default) || q(d.default) ? q(d.default) ? a ? i : () => i : d.default : n ? a ? i : () => i : "", v = n || u !== "", y = us(e, d);
  m && ec(d);
  let [f, g, w] = b ? [
    i,
    y,
    l[y] || {}
  ] : aa(e, i, y, r, h, _), E = f, L = i;
  if (!b && !(I(E) || ut(E) || ve(E)) && v && (E = u, L = E), !b && (!(I(E) || ut(E) || ve(E)) || !I(g)))
    return o ? wn : i;
  if (process.env.NODE_ENV !== "production" && I(E) && e.messageCompiler == null)
    return Ke(`The message format compilation is not supported in this build. Because message compiler isn't included. You need to pre-compilation all message format. So translate function return '${i}'.`), i;
  let N = !1;
  const A = () => {
    N = !0;
  }, D = ve(E) ? E : ra(e, i, g, E, L, A);
  if (N)
    return E;
  const k = oc(e, g, w, d), U = Iu(k), X = tc(e, D, U), W = s ? s(X, i) : X;
  if (process.env.NODE_ENV !== "production" || __INTLIFY_PROD_DEVTOOLS__) {
    const te = {
      timestamp: Date.now(),
      key: I(i) ? i : ve(E) ? E.key : "",
      locale: g || (ve(E) ? E.locale : ""),
      format: I(E) ? E : ve(E) ? E.source : "",
      message: W
    };
    te.meta = de({}, e.__meta, /* @__PURE__ */ ju() || {}), Ru(te);
  }
  return W;
}
function ec(e) {
  oe(e.list) ? e.list = e.list.map((t) => I(t) ? Fs(t) : t) : Y(e.named) && Object.keys(e.named).forEach((t) => {
    I(e.named[t]) && (e.named[t] = Fs(e.named[t]));
  });
}
function aa(e, t, n, s, o, a) {
  const { messages: r, onWarn: l, messageResolver: i, localeFallbacker: d } = e, _ = d(e, s, n);
  let h = {}, m, b = null, u = n, v = null;
  const y = "translate";
  for (let f = 0; f < _.length; f++) {
    if (m = v = _[f], process.env.NODE_ENV !== "production" && n !== m && Cn(o, t) && l(lt(me.FALLBACK_TO_TRANSLATE, {
      key: t,
      target: m
    })), process.env.NODE_ENV !== "production" && n !== m) {
      const N = e.__v_emitter;
      N && N.emit("fallback", {
        type: y,
        key: t,
        from: u,
        to: v,
        groupId: `${y}:${t}`
      });
    }
    h = r[m] || {};
    let g = null, w, E;
    if (process.env.NODE_ENV !== "production" && Ve && (g = window.performance.now(), w = "intlify-message-resolve-start", E = "intlify-message-resolve-end", ge && ge(w)), (b = i(h, t)) === null && (b = h[t]), process.env.NODE_ENV !== "production" && Ve) {
      const N = window.performance.now(), A = e.__v_emitter;
      A && g && b && A.emit("message-resolve", {
        type: "message-resolve",
        key: t,
        message: b,
        time: N - g,
        groupId: `${y}:${t}`
      }), w && E && ge && it && (ge(E), it("intlify message resolve", w, E));
    }
    if (I(b) || ut(b) || ve(b))
      break;
    const L = cs(
      e,
      // eslint-disable-line @typescript-eslint/no-explicit-any
      t,
      m,
      a,
      y
    );
    L !== t && (b = L), u = v;
  }
  return [b, m, h];
}
function ra(e, t, n, s, o, a) {
  const { messageCompiler: r, warnHtmlMessage: l } = e;
  if (ve(s)) {
    const m = s;
    return m.locale = m.locale || n, m.key = m.key || t, m;
  }
  if (r == null) {
    const m = () => s;
    return m.locale = n, m.key = t, m;
  }
  let i = null, d, _;
  process.env.NODE_ENV !== "production" && Ve && (i = window.performance.now(), d = "intlify-message-compilation-start", _ = "intlify-message-compilation-end", ge && ge(d));
  const h = r(s, nc(e, n, o, s, l, a));
  if (process.env.NODE_ENV !== "production" && Ve) {
    const m = window.performance.now(), b = e.__v_emitter;
    b && i && b.emit("message-compilation", {
      type: "message-compilation",
      message: s,
      time: m - i,
      groupId: `translate:${t}`
    }), d && _ && ge && it && (ge(_), it("intlify message compilation", d, _));
  }
  return h.locale = n, h.key = t, h.source = s, h;
}
function tc(e, t, n) {
  let s = null, o, a;
  process.env.NODE_ENV !== "production" && Ve && (s = window.performance.now(), o = "intlify-message-evaluation-start", a = "intlify-message-evaluation-end", ge && ge(o));
  const r = t(n);
  if (process.env.NODE_ENV !== "production" && Ve) {
    const l = window.performance.now(), i = e.__v_emitter;
    i && s && i.emit("message-evaluation", {
      type: "message-evaluation",
      value: r,
      time: l - s,
      groupId: `translate:${t.key}`
    }), o && a && ge && it && (ge(a), it("intlify message evaluation", o, a));
  }
  return r;
}
function Hn(...e) {
  const [t, n, s] = e, o = {};
  if (!I(t) && !ce(t) && !ve(t) && !ut(t))
    throw Oe(re.INVALID_ARGUMENT);
  const a = ce(t) ? String(t) : (ve(t), t);
  return ce(n) ? o.plural = n : I(n) ? o.default = n : x(n) && !Tn(n) ? o.named = n : oe(n) && (o.list = n), ce(s) ? o.plural = s : I(s) ? o.default = s : x(s) && de(o, s), [a, o];
}
function nc(e, t, n, s, o, a) {
  return {
    locale: t,
    key: n,
    warnHtmlMessage: o,
    onError: (r) => {
      if (a && a(r), process.env.NODE_ENV !== "production") {
        const l = sc(s), i = `Message compilation error: ${r.message}`, d = r.location && l && Fl(l, r.location.start.offset, r.location.end.offset), _ = e.__v_emitter;
        _ && l && _.emit("compile-error", {
          message: l,
          error: r.message,
          start: r.location && r.location.start.offset,
          end: r.location && r.location.end.offset,
          groupId: `translate:${n}`
        }), console.error(d ? `${i}
${d}` : i);
      } else
        throw r;
    },
    onCacheKey: (r) => $l(t, n, r)
  };
}
function sc(e) {
  if (I(e))
    return e;
  if (e.loc && e.loc.source)
    return e.loc.source;
}
function oc(e, t, n, s) {
  const { modifiers: o, pluralRules: a, messageResolver: r, fallbackLocale: l, fallbackWarn: i, missingWarn: d, fallbackContext: _ } = e, m = {
    locale: t,
    modifiers: o,
    pluralRules: a,
    messages: (b) => {
      let u = r(n, b);
      if (u == null && _) {
        const [, , v] = aa(_, b, t, l, i, d);
        u = r(v, b);
      }
      if (I(u) || ut(u)) {
        let v = !1;
        const f = ra(e, b, t, u, b, () => {
          v = !0;
        });
        return v ? Qs : f;
      } else
        return ve(u) ? u : Qs;
    }
  };
  return e.processor && (m.processor = e.processor), s.list && (m.list = s.list), s.named && (m.named = s.named), ce(s.plural) && (m.pluralIndex = s.plural), m;
}
const to = typeof Intl < "u", ia = {
  dateTimeFormat: to && typeof Intl.DateTimeFormat < "u",
  numberFormat: to && typeof Intl.NumberFormat < "u"
};
function no(e, ...t) {
  const { datetimeFormats: n, unresolving: s, fallbackLocale: o, onWarn: a, localeFallbacker: r } = e, { __datetimeFormatters: l } = e;
  if (process.env.NODE_ENV !== "production" && !ia.dateTimeFormat)
    return a(lt(me.CANNOT_FORMAT_DATE)), hn;
  const [i, d, _, h] = Wn(...t), m = q(_.missingWarn) ? _.missingWarn : e.missingWarn, b = q(_.fallbackWarn) ? _.fallbackWarn : e.fallbackWarn, u = !!_.part, v = us(e, _), y = r(
    e,
    // eslint-disable-line @typescript-eslint/no-explicit-any
    o,
    v
  );
  if (!I(i) || i === "")
    return new Intl.DateTimeFormat(v, h).format(d);
  let f = {}, g, w = null, E = v, L = null;
  const N = "datetime format";
  for (let k = 0; k < y.length; k++) {
    if (g = L = y[k], process.env.NODE_ENV !== "production" && v !== g && Cn(b, i) && a(lt(me.FALLBACK_TO_DATE_FORMAT, {
      key: i,
      target: g
    })), process.env.NODE_ENV !== "production" && v !== g) {
      const U = e.__v_emitter;
      U && U.emit("fallback", {
        type: N,
        key: i,
        from: E,
        to: L,
        groupId: `${N}:${i}`
      });
    }
    if (f = n[g] || {}, w = f[i], x(w))
      break;
    cs(e, i, g, m, N), E = L;
  }
  if (!x(w) || !I(g))
    return s ? wn : i;
  let A = `${g}__${i}`;
  Tn(h) || (A = `${A}__${JSON.stringify(h)}`);
  let D = l.get(A);
  return D || (D = new Intl.DateTimeFormat(g, de({}, w, h)), l.set(A, D)), u ? D.formatToParts(d) : D.format(d);
}
const la = [
  "localeMatcher",
  "weekday",
  "era",
  "year",
  "month",
  "day",
  "hour",
  "minute",
  "second",
  "timeZoneName",
  "formatMatcher",
  "hour12",
  "timeZone",
  "dateStyle",
  "timeStyle",
  "calendar",
  "dayPeriod",
  "numberingSystem",
  "hourCycle",
  "fractionalSecondDigits"
];
function Wn(...e) {
  const [t, n, s, o] = e, a = {};
  let r = {}, l;
  if (I(t)) {
    const i = t.match(/(\d{4}-\d{2}-\d{2})(T|\s)?(.*)/);
    if (!i)
      throw Oe(re.INVALID_ISO_DATE_ARGUMENT);
    const d = i[3] ? i[3].trim().startsWith("T") ? `${i[1].trim()}${i[3].trim()}` : `${i[1].trim()}T${i[3].trim()}` : i[1].trim();
    l = new Date(d);
    try {
      l.toISOString();
    } catch {
      throw Oe(re.INVALID_ISO_DATE_ARGUMENT);
    }
  } else if (Il(t)) {
    if (isNaN(t.getTime()))
      throw Oe(re.INVALID_DATE_ARGUMENT);
    l = t;
  } else if (ce(t))
    l = t;
  else
    throw Oe(re.INVALID_ARGUMENT);
  return I(n) ? a.key = n : x(n) && Object.keys(n).forEach((i) => {
    la.includes(i) ? r[i] = n[i] : a[i] = n[i];
  }), I(s) ? a.locale = s : x(s) && (r = s), x(o) && (r = o), [a.key || "", l, a, r];
}
function so(e, t, n) {
  const s = e;
  for (const o in n) {
    const a = `${t}__${o}`;
    s.__datetimeFormatters.has(a) && s.__datetimeFormatters.delete(a);
  }
}
function oo(e, ...t) {
  const { numberFormats: n, unresolving: s, fallbackLocale: o, onWarn: a, localeFallbacker: r } = e, { __numberFormatters: l } = e;
  if (process.env.NODE_ENV !== "production" && !ia.numberFormat)
    return a(lt(me.CANNOT_FORMAT_NUMBER)), hn;
  const [i, d, _, h] = Gn(...t), m = q(_.missingWarn) ? _.missingWarn : e.missingWarn, b = q(_.fallbackWarn) ? _.fallbackWarn : e.fallbackWarn, u = !!_.part, v = us(e, _), y = r(
    e,
    // eslint-disable-line @typescript-eslint/no-explicit-any
    o,
    v
  );
  if (!I(i) || i === "")
    return new Intl.NumberFormat(v, h).format(d);
  let f = {}, g, w = null, E = v, L = null;
  const N = "number format";
  for (let k = 0; k < y.length; k++) {
    if (g = L = y[k], process.env.NODE_ENV !== "production" && v !== g && Cn(b, i) && a(lt(me.FALLBACK_TO_NUMBER_FORMAT, {
      key: i,
      target: g
    })), process.env.NODE_ENV !== "production" && v !== g) {
      const U = e.__v_emitter;
      U && U.emit("fallback", {
        type: N,
        key: i,
        from: E,
        to: L,
        groupId: `${N}:${i}`
      });
    }
    if (f = n[g] || {}, w = f[i], x(w))
      break;
    cs(e, i, g, m, N), E = L;
  }
  if (!x(w) || !I(g))
    return s ? wn : i;
  let A = `${g}__${i}`;
  Tn(h) || (A = `${A}__${JSON.stringify(h)}`);
  let D = l.get(A);
  return D || (D = new Intl.NumberFormat(g, de({}, w, h)), l.set(A, D)), u ? D.formatToParts(d) : D.format(d);
}
const ua = [
  "localeMatcher",
  "style",
  "currency",
  "currencyDisplay",
  "currencySign",
  "useGrouping",
  "minimumIntegerDigits",
  "minimumFractionDigits",
  "maximumFractionDigits",
  "minimumSignificantDigits",
  "maximumSignificantDigits",
  "compactDisplay",
  "notation",
  "signDisplay",
  "unit",
  "unitDisplay",
  "roundingMode",
  "roundingPriority",
  "roundingIncrement",
  "trailingZeroDisplay"
];
function Gn(...e) {
  const [t, n, s, o] = e, a = {};
  let r = {};
  if (!ce(t))
    throw Oe(re.INVALID_ARGUMENT);
  const l = t;
  return I(n) ? a.key = n : x(n) && Object.keys(n).forEach((i) => {
    ua.includes(i) ? r[i] = n[i] : a[i] = n[i];
  }), I(s) ? a.locale = s : x(s) && (r = s), x(o) && (r = o), [a.key || "", l, a, r];
}
function ao(e, t, n) {
  const s = e;
  for (const o in n) {
    const a = `${t}__${o}`;
    s.__numberFormatters.has(a) && s.__numberFormatters.delete(a);
  }
}
_u();
/*!
  * vue-i18n v9.10.2
  * (c) 2024 kazuya kawaguchi
  * Released under the MIT License.
  */
const ac = "9.10.2";
function rc() {
  typeof __VUE_I18N_FULL_INSTALL__ != "boolean" && (De().__VUE_I18N_FULL_INSTALL__ = !0), typeof __VUE_I18N_LEGACY_API__ != "boolean" && (De().__VUE_I18N_LEGACY_API__ = !0), typeof __INTLIFY_JIT_COMPILATION__ != "boolean" && (De().__INTLIFY_JIT_COMPILATION__ = !1), typeof __INTLIFY_DROP_MESSAGE_COMPILER__ != "boolean" && (De().__INTLIFY_DROP_MESSAGE_COMPILER__ = !1), typeof __INTLIFY_PROD_DEVTOOLS__ != "boolean" && (De().__INTLIFY_PROD_DEVTOOLS__ = !1);
}
const ca = me.__EXTEND_POINT__, Re = is(ca), _e = {
  FALLBACK_TO_ROOT: ca,
  // 9
  NOT_SUPPORTED_PRESERVE: Re(),
  // 10
  NOT_SUPPORTED_FORMATTER: Re(),
  // 11
  NOT_SUPPORTED_PRESERVE_DIRECTIVE: Re(),
  // 12
  NOT_SUPPORTED_GET_CHOICE_INDEX: Re(),
  // 13
  COMPONENT_NAME_LEGACY_COMPATIBLE: Re(),
  // 14
  NOT_FOUND_PARENT_SCOPE: Re(),
  // 15
  IGNORE_OBJ_FLATTEN: Re(),
  // 16
  NOTICE_DROP_ALLOW_COMPOSITION: Re(),
  // 17
  NOTICE_DROP_TRANSLATE_EXIST_COMPATIBLE_FLAG: Re()
  // 18
}, ic = {
  [_e.FALLBACK_TO_ROOT]: "Fall back to {type} '{key}' with root locale.",
  [_e.NOT_SUPPORTED_PRESERVE]: "Not supported 'preserve'.",
  [_e.NOT_SUPPORTED_FORMATTER]: "Not supported 'formatter'.",
  [_e.NOT_SUPPORTED_PRESERVE_DIRECTIVE]: "Not supported 'preserveDirectiveContent'.",
  [_e.NOT_SUPPORTED_GET_CHOICE_INDEX]: "Not supported 'getChoiceIndex'.",
  [_e.COMPONENT_NAME_LEGACY_COMPATIBLE]: "Component name legacy compatible: '{name}' -> 'i18n'",
  [_e.NOT_FOUND_PARENT_SCOPE]: "Not found parent scope. use the global scope.",
  [_e.IGNORE_OBJ_FLATTEN]: "Ignore object flatten: '{key}' key has an string value",
  [_e.NOTICE_DROP_ALLOW_COMPOSITION]: "'allowComposition' option will be dropped in the next major version. For more information, please see 👉 https://tinyurl.com/2p97mcze",
  [_e.NOTICE_DROP_TRANSLATE_EXIST_COMPATIBLE_FLAG]: "'translateExistCompatible' option will be dropped in the next major version."
};
function _n(e, ...t) {
  return rs(ic[e], ...t);
}
const da = re.__EXTEND_POINT__, he = is(da), Z = {
  // composer module errors
  UNEXPECTED_RETURN_TYPE: da,
  // 26
  // legacy module errors
  INVALID_ARGUMENT: he(),
  // 27
  // i18n module errors
  MUST_BE_CALL_SETUP_TOP: he(),
  // 28
  NOT_INSTALLED: he(),
  // 29
  NOT_AVAILABLE_IN_LEGACY_MODE: he(),
  // 30
  // directive module errors
  REQUIRED_VALUE: he(),
  // 31
  INVALID_VALUE: he(),
  // 32
  // vue-devtools errors
  CANNOT_SETUP_VUE_DEVTOOLS_PLUGIN: he(),
  // 33
  NOT_INSTALLED_WITH_PROVIDE: he(),
  // 34
  // unexpected error
  UNEXPECTED_ERROR: he(),
  // 35
  // not compatible legacy vue-i18n constructor
  NOT_COMPATIBLE_LEGACY_VUE_I18N: he(),
  // 36
  // bridge support vue 2.x only
  BRIDGE_SUPPORT_VUE_2_ONLY: he(),
  // 37
  // need to define `i18n` option in `allowComposition: true` and `useScope: 'local' at `useI18n``
  MUST_DEFINE_I18N_OPTION_IN_ALLOW_COMPOSITION: he(),
  // 38
  // Not available Compostion API in Legacy API mode. Please make sure that the legacy API mode is working properly
  NOT_AVAILABLE_COMPOSITION_IN_LEGACY: he(),
  // 39
  // for enhancement
  __EXTEND_POINT__: he()
  // 40
};
function je(e, ...t) {
  return Lt(e, null, process.env.NODE_ENV !== "production" ? { messages: lc, args: t } : void 0);
}
const lc = {
  [Z.UNEXPECTED_RETURN_TYPE]: "Unexpected return type in composer",
  [Z.INVALID_ARGUMENT]: "Invalid argument",
  [Z.MUST_BE_CALL_SETUP_TOP]: "Must be called at the top of a `setup` function",
  [Z.NOT_INSTALLED]: "Need to install with `app.use` function",
  [Z.UNEXPECTED_ERROR]: "Unexpected error",
  [Z.NOT_AVAILABLE_IN_LEGACY_MODE]: "Not available in legacy mode",
  [Z.REQUIRED_VALUE]: "Required in value: {0}",
  [Z.INVALID_VALUE]: "Invalid value",
  [Z.CANNOT_SETUP_VUE_DEVTOOLS_PLUGIN]: "Cannot setup vue-devtools plugin",
  [Z.NOT_INSTALLED_WITH_PROVIDE]: "Need to install with `provide` function",
  [Z.NOT_COMPATIBLE_LEGACY_VUE_I18N]: "Not compatible legacy VueI18n.",
  [Z.BRIDGE_SUPPORT_VUE_2_ONLY]: "vue-i18n-bridge support Vue 2.x only",
  [Z.MUST_DEFINE_I18N_OPTION_IN_ALLOW_COMPOSITION]: "Must define ‘i18n’ option or custom block in Composition API with using local scope in Legacy API mode",
  [Z.NOT_AVAILABLE_COMPOSITION_IN_LEGACY]: "Not available Compostion API in Legacy API mode. Please make sure that the legacy API mode is working properly"
}, zn = /* @__PURE__ */ Be("__translateVNode"), Yn = /* @__PURE__ */ Be("__datetimeParts"), jn = /* @__PURE__ */ Be("__numberParts"), qn = /* @__PURE__ */ Be("__enableEmitter"), Kn = /* @__PURE__ */ Be("__disableEmitter"), uc = Be("__setPluralRules"), fa = /* @__PURE__ */ Be("__injectWithOption"), Xn = /* @__PURE__ */ Be("__dispose");
function jt(e) {
  if (!Y(e))
    return e;
  for (const t in e)
    if (mn(e, t))
      if (!t.includes("."))
        Y(e[t]) && jt(e[t]);
      else {
        const n = t.split("."), s = n.length - 1;
        let o = e, a = !1;
        for (let r = 0; r < s; r++) {
          if (n[r] in o || (o[n[r]] = {}), !Y(o[n[r]])) {
            process.env.NODE_ENV !== "production" && Ke(_n(_e.IGNORE_OBJ_FLATTEN, {
              key: n[r]
            })), a = !0;
            break;
          }
          o = o[n[r]];
        }
        a || (o[n[s]] = e[t], delete e[t]), Y(o[n[s]]) && jt(o[n[s]]);
      }
  return e;
}
function ds(e, t) {
  const { messages: n, __i18n: s, messageResolver: o, flatJson: a } = t, r = x(n) ? n : oe(s) ? {} : { [e]: {} };
  if (oe(s) && s.forEach((l) => {
    if ("locale" in l && "resource" in l) {
      const { locale: i, resource: d } = l;
      i ? (r[i] = r[i] || {}, nn(d, r[i])) : nn(d, r);
    } else
      I(l) && nn(JSON.parse(l), r);
  }), o == null && a)
    for (const l in r)
      mn(r, l) && jt(r[l]);
  return r;
}
function pa(e) {
  return e.type;
}
function cc(e, t, n) {
  let s = Y(t.messages) ? t.messages : {};
  "__i18nGlobal" in n && (s = ds(e.locale.value, {
    messages: s,
    __i18n: n.__i18nGlobal
  }));
  const o = Object.keys(s);
  o.length && o.forEach((a) => {
    e.mergeLocaleMessage(a, s[a]);
  });
  {
    if (Y(t.datetimeFormats)) {
      const a = Object.keys(t.datetimeFormats);
      a.length && a.forEach((r) => {
        e.mergeDateTimeFormat(r, t.datetimeFormats[r]);
      });
    }
    if (Y(t.numberFormats)) {
      const a = Object.keys(t.numberFormats);
      a.length && a.forEach((r) => {
        e.mergeNumberFormat(r, t.numberFormats[r]);
      });
    }
  }
}
function ro(e) {
  return ot(Ra, null, e, 0);
}
const io = "__INTLIFY_META__", lo = () => [], dc = () => !1;
let uo = 0;
function co(e) {
  return (t, n, s, o) => e(n, s, gn() || void 0, o);
}
const fc = /* @__NO_SIDE_EFFECTS__ */ () => {
  const e = gn();
  let t = null;
  return e && (t = pa(e)[io]) ? { [io]: t } : null;
};
function pc(e = {}, t) {
  const { __root: n, __injectWithOption: s } = e, o = n === void 0, a = e.flatJson, r = Ve ? ue : yo, l = !!e.translateExistCompatible;
  process.env.NODE_ENV !== "production" && l && zo(_n(_e.NOTICE_DROP_TRANSLATE_EXIST_COMPATIBLE_FLAG));
  let i = q(e.inheritLocale) ? e.inheritLocale : !0;
  const d = r(
    // prettier-ignore
    n && i ? n.locale.value : I(e.locale) ? e.locale : Yt
  ), _ = r(
    // prettier-ignore
    n && i ? n.fallbackLocale.value : I(e.fallbackLocale) || oe(e.fallbackLocale) || x(e.fallbackLocale) || e.fallbackLocale === !1 ? e.fallbackLocale : d.value
  ), h = r(ds(d.value, e)), m = r(x(e.datetimeFormats) ? e.datetimeFormats : { [d.value]: {} }), b = r(x(e.numberFormats) ? e.numberFormats : { [d.value]: {} });
  let u = n ? n.missingWarn : q(e.missingWarn) || wt(e.missingWarn) ? e.missingWarn : !0, v = n ? n.fallbackWarn : q(e.fallbackWarn) || wt(e.fallbackWarn) ? e.fallbackWarn : !0, y = n ? n.fallbackRoot : q(e.fallbackRoot) ? e.fallbackRoot : !0, f = !!e.fallbackFormat, g = K(e.missing) ? e.missing : null, w = K(e.missing) ? co(e.missing) : null, E = K(e.postTranslation) ? e.postTranslation : null, L = n ? n.warnHtmlMessage : q(e.warnHtmlMessage) ? e.warnHtmlMessage : !0, N = !!e.escapeParameter;
  const A = n ? n.modifiers : x(e.modifiers) ? e.modifiers : {};
  let D = e.pluralRules || n && n.pluralRules, k;
  k = (() => {
    o && Xs(null);
    const C = {
      version: ac,
      locale: d.value,
      fallbackLocale: _.value,
      messages: h.value,
      modifiers: A,
      pluralRules: D,
      missing: w === null ? void 0 : w,
      missingWarn: u,
      fallbackWarn: v,
      fallbackFormat: f,
      unresolving: !0,
      postTranslation: E === null ? void 0 : E,
      warnHtmlMessage: L,
      escapeParameter: N,
      messageResolver: e.messageResolver,
      messageCompiler: e.messageCompiler,
      __meta: { framework: "vue" }
    };
    C.datetimeFormats = m.value, C.numberFormats = b.value, C.__datetimeFormatters = x(k) ? k.__datetimeFormatters : void 0, C.__numberFormatters = x(k) ? k.__numberFormatters : void 0, process.env.NODE_ENV !== "production" && (C.__v_emitter = x(k) ? k.__v_emitter : void 0);
    const O = Ku(C);
    return o && Xs(O), O;
  })(), It(k, d.value, _.value);
  function X() {
    return [
      d.value,
      _.value,
      h.value,
      m.value,
      b.value
    ];
  }
  const W = B({
    get: () => d.value,
    set: (C) => {
      d.value = C, k.locale = d.value;
    }
  }), te = B({
    get: () => _.value,
    set: (C) => {
      _.value = C, k.fallbackLocale = _.value, It(k, d.value, C);
    }
  }), ne = B(() => h.value), ie = /* @__PURE__ */ B(() => m.value), J = /* @__PURE__ */ B(() => b.value);
  function le() {
    return K(E) ? E : null;
  }
  function Ze(C) {
    E = C, k.postTranslation = C;
  }
  function ft() {
    return g;
  }
  function Nt(C) {
    C !== null && (w = co(C)), g = C, k.missing = w;
  }
  function Ot(C, O) {
    return C !== "translate" || !O.resolvedMessage;
  }
  const be = (C, O, z, Q, xe, Kt) => {
    X();
    let _t;
    try {
      process.env.NODE_ENV !== "production" || __INTLIFY_PROD_DEVTOOLS__, o || (k.fallbackContext = n ? qu() : void 0), _t = C(k);
    } finally {
      process.env.NODE_ENV !== "production" || __INTLIFY_PROD_DEVTOOLS__, o || (k.fallbackContext = void 0);
    }
    if (z !== "translate exists" && // for not `te` (e.g `t`)
    ce(_t) && _t === wn || z === "translate exists" && !_t) {
      const [et, Ta] = O();
      if (process.env.NODE_ENV !== "production" && n && I(et) && Ot(z, Ta) && (y && (Cn(v, et) || ta(u, et)) && Ke(_n(_e.FALLBACK_TO_ROOT, {
        key: et,
        type: z
      })), process.env.NODE_ENV !== "production")) {
        const { __v_emitter: ps } = k;
        ps && y && ps.emit("fallback", {
          type: z,
          key: et,
          to: "global",
          groupId: `${z}:${et}`
        });
      }
      return n && y ? Q(n) : xe(et);
    } else {
      if (Kt(_t))
        return _t;
      throw je(Z.UNEXPECTED_RETURN_TYPE);
    }
  };
  function pt(...C) {
    return be((O) => Reflect.apply(eo, null, [O, ...C]), () => Hn(...C), "translate", (O) => Reflect.apply(O.t, O, [...C]), (O) => O, (O) => I(O));
  }
  function mt(...C) {
    const [O, z, Q] = C;
    if (Q && !Y(Q))
      throw je(Z.INVALID_ARGUMENT);
    return pt(O, z, de({ resolvedMessage: !0 }, Q || {}));
  }
  function kt(...C) {
    return be((O) => Reflect.apply(no, null, [O, ...C]), () => Wn(...C), "datetime format", (O) => Reflect.apply(O.d, O, [...C]), () => hn, (O) => I(O));
  }
  function At(...C) {
    return be((O) => Reflect.apply(oo, null, [O, ...C]), () => Gn(...C), "number format", (O) => Reflect.apply(O.n, O, [...C]), () => hn, (O) => I(O));
  }
  function $t(C) {
    return C.map((O) => I(O) || ce(O) || q(O) ? ro(String(O)) : O);
  }
  const Je = {
    normalize: $t,
    interpolate: (C) => C,
    type: "vnode"
  };
  function Ue(...C) {
    return be(
      (O) => {
        let z;
        const Q = O;
        try {
          Q.processor = Je, z = Reflect.apply(eo, null, [Q, ...C]);
        } finally {
          Q.processor = null;
        }
        return z;
      },
      () => Hn(...C),
      "translate",
      // eslint-disable-next-line @typescript-eslint/no-explicit-any
      (O) => O[zn](...C),
      (O) => [ro(O)],
      (O) => oe(O)
    );
  }
  function Qe(...C) {
    return be(
      (O) => Reflect.apply(oo, null, [O, ...C]),
      () => Gn(...C),
      "number format",
      // eslint-disable-next-line @typescript-eslint/no-explicit-any
      (O) => O[jn](...C),
      lo,
      (O) => I(O) || oe(O)
    );
  }
  function St(...C) {
    return be(
      (O) => Reflect.apply(no, null, [O, ...C]),
      () => Wn(...C),
      "datetime format",
      // eslint-disable-next-line @typescript-eslint/no-explicit-any
      (O) => O[Yn](...C),
      lo,
      (O) => I(O) || oe(O)
    );
  }
  function p(C) {
    D = C, k.pluralRules = D;
  }
  function c(C, O) {
    return be(() => {
      if (!C)
        return !1;
      const z = I(O) ? O : d.value, Q = V(z), xe = k.messageResolver(Q, C);
      return l ? xe != null : ut(xe) || ve(xe) || I(xe);
    }, () => [C], "translate exists", (z) => Reflect.apply(z.te, z, [C, O]), dc, (z) => q(z));
  }
  function T(C) {
    let O = null;
    const z = Ko(k, _.value, d.value);
    for (let Q = 0; Q < z.length; Q++) {
      const xe = h.value[z[Q]] || {}, Kt = k.messageResolver(xe, C);
      if (Kt != null) {
        O = Kt;
        break;
      }
    }
    return O;
  }
  function $(C) {
    const O = T(C);
    return O ?? (n ? n.tm(C) || {} : {});
  }
  function V(C) {
    return h.value[C] || {};
  }
  function G(C, O) {
    if (a) {
      const z = { [C]: O };
      for (const Q in z)
        mn(z, Q) && jt(z[Q]);
      O = z[C];
    }
    h.value[C] = O, k.messages = h.value;
  }
  function Le(C, O) {
    h.value[C] = h.value[C] || {};
    const z = { [C]: O };
    if (a)
      for (const Q in z)
        mn(z, Q) && jt(z[Q]);
    O = z[C], nn(O, h.value[C]), k.messages = h.value;
  }
  function Pe(C) {
    return m.value[C] || {};
  }
  function ga(C, O) {
    m.value[C] = O, k.datetimeFormats = m.value, so(k, C, O);
  }
  function va(C, O) {
    m.value[C] = de(m.value[C] || {}, O), k.datetimeFormats = m.value, so(k, C, O);
  }
  function ya(C) {
    return b.value[C] || {};
  }
  function Ea(C, O) {
    b.value[C] = O, k.numberFormats = b.value, ao(k, C, O);
  }
  function ba(C, O) {
    b.value[C] = de(b.value[C] || {}, O), k.numberFormats = b.value, ao(k, C, O);
  }
  uo++, n && Ve && (rt(n.locale, (C) => {
    i && (d.value = C, k.locale = C, It(k, d.value, _.value));
  }), rt(n.fallbackLocale, (C) => {
    i && (_.value = C, k.fallbackLocale = C, It(k, d.value, _.value));
  }));
  const se = {
    id: uo,
    locale: W,
    fallbackLocale: te,
    get inheritLocale() {
      return i;
    },
    set inheritLocale(C) {
      i = C, C && n && (d.value = n.locale.value, _.value = n.fallbackLocale.value, It(k, d.value, _.value));
    },
    get availableLocales() {
      return Object.keys(h.value).sort();
    },
    messages: ne,
    get modifiers() {
      return A;
    },
    get pluralRules() {
      return D || {};
    },
    get isGlobal() {
      return o;
    },
    get missingWarn() {
      return u;
    },
    set missingWarn(C) {
      u = C, k.missingWarn = u;
    },
    get fallbackWarn() {
      return v;
    },
    set fallbackWarn(C) {
      v = C, k.fallbackWarn = v;
    },
    get fallbackRoot() {
      return y;
    },
    set fallbackRoot(C) {
      y = C;
    },
    get fallbackFormat() {
      return f;
    },
    set fallbackFormat(C) {
      f = C, k.fallbackFormat = f;
    },
    get warnHtmlMessage() {
      return L;
    },
    set warnHtmlMessage(C) {
      L = C, k.warnHtmlMessage = C;
    },
    get escapeParameter() {
      return N;
    },
    set escapeParameter(C) {
      N = C, k.escapeParameter = C;
    },
    t: pt,
    getLocaleMessage: V,
    setLocaleMessage: G,
    mergeLocaleMessage: Le,
    getPostTranslationHandler: le,
    setPostTranslationHandler: Ze,
    getMissingHandler: ft,
    setMissingHandler: Nt,
    [uc]: p
  };
  return se.datetimeFormats = ie, se.numberFormats = J, se.rt = mt, se.te = c, se.tm = $, se.d = kt, se.n = At, se.getDateTimeFormat = Pe, se.setDateTimeFormat = ga, se.mergeDateTimeFormat = va, se.getNumberFormat = ya, se.setNumberFormat = Ea, se.mergeNumberFormat = ba, se[fa] = s, se[zn] = Ue, se[Yn] = St, se[jn] = Qe, process.env.NODE_ENV !== "production" && (se[qn] = (C) => {
    k.__v_emitter = C;
  }, se[Kn] = () => {
    k.__v_emitter = void 0;
  }), se;
}
const fs = {
  tag: {
    type: [String, Object]
  },
  locale: {
    type: String
  },
  scope: {
    type: String,
    // NOTE: avoid https://github.com/microsoft/rushstack/issues/1050
    validator: (e) => e === "parent" || e === "global",
    default: "parent"
    /* ComponentI18nScope */
  },
  i18n: {
    type: Object
  }
};
function mc({ slots: e }, t) {
  return t.length === 1 && t[0] === "default" ? (e.default ? e.default() : []).reduce((s, o) => [
    ...s,
    // prettier-ignore
    ...o.type === Fe ? o.children : [o]
  ], []) : t.reduce((n, s) => {
    const o = e[s];
    return o && (n[s] = o()), n;
  }, {});
}
function ma(e) {
  return Fe;
}
de({
  keypath: {
    type: String,
    required: !0
  },
  plural: {
    type: [Number, String],
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    validator: (e) => ce(e) || !isNaN(e)
  }
}, fs);
function hc(e) {
  return oe(e) && !I(e[0]);
}
function ha(e, t, n, s) {
  const { slots: o, attrs: a } = t;
  return () => {
    const r = { part: !0 };
    let l = {};
    e.locale && (r.locale = e.locale), I(e.format) ? r.key = e.format : Y(e.format) && (I(e.format.key) && (r.key = e.format.key), l = Object.keys(e.format).reduce((m, b) => n.includes(b) ? de({}, m, { [b]: e.format[b] }) : m, {}));
    const i = s(e.value, r, l);
    let d = [r.key];
    oe(i) ? d = i.map((m, b) => {
      const u = o[m.type], v = u ? u({ [m.type]: m.value, index: b, parts: i }) : [m.value];
      return hc(v) && (v[0].key = `${m.type}-${b}`), v;
    }) : I(i) && (d = [i]);
    const _ = de({}, a), h = I(e.tag) || Y(e.tag) ? e.tag : ma();
    return vo(h, _, d);
  };
}
de({
  value: {
    type: Number,
    required: !0
  },
  format: {
    type: [String, Object]
  }
}, fs);
de({
  value: {
    type: [Number, Date],
    required: !0
  },
  format: {
    type: [String, Object]
  }
}, fs);
function fo(e, t) {
}
const _c = /* @__PURE__ */ Be("global-vue-i18n");
function Ln(e = {}) {
  const t = gn();
  if (t == null)
    throw je(Z.MUST_BE_CALL_SETUP_TOP);
  if (!t.isCE && t.appContext.app != null && !t.appContext.app.__VUE_I18N_SYMBOL__)
    throw je(Z.NOT_INSTALLED);
  const n = gc(t), s = yc(n), o = pa(t), a = vc(e, o);
  if (__VUE_I18N_LEGACY_API__ && n.mode === "legacy" && !e.__useComponent) {
    if (!n.allowComposition)
      throw je(Z.NOT_AVAILABLE_IN_LEGACY_MODE);
    return wc(t, a, s, e);
  }
  if (a === "global")
    return cc(s, e, o), s;
  if (a === "parent") {
    let i = Ec(n, t, e.__useComponent);
    return i == null && (process.env.NODE_ENV !== "production" && Ke(_n(_e.NOT_FOUND_PARENT_SCOPE)), i = s), i;
  }
  const r = n;
  let l = r.__getInstance(t);
  if (l == null) {
    const i = de({}, e);
    "__i18n" in o && (i.__i18n = o.__i18n), s && (i.__root = s), l = pc(i), r.__composerExtend && (l[Xn] = r.__composerExtend(l)), Tc(r, t, l), r.__setInstance(t, l);
  }
  return l;
}
function gc(e) {
  {
    const t = Ia(e.isCE ? _c : e.appContext.app.__VUE_I18N_SYMBOL__);
    if (!t)
      throw je(e.isCE ? Z.NOT_INSTALLED_WITH_PROVIDE : Z.UNEXPECTED_ERROR);
    return t;
  }
}
function vc(e, t) {
  return Tn(e) ? "__i18n" in t ? "local" : "global" : e.useScope ? e.useScope : "local";
}
function yc(e) {
  return e.mode === "composition" ? e.global : e.global.__composer;
}
function Ec(e, t, n = !1) {
  let s = null;
  const o = t.root;
  let a = bc(t, n);
  for (; a != null; ) {
    const r = e;
    if (e.mode === "composition")
      s = r.__getInstance(a);
    else if (__VUE_I18N_LEGACY_API__) {
      const l = r.__getInstance(a);
      l != null && (s = l.__composer, n && s && !s[fa] && (s = null));
    }
    if (s != null || o === a)
      break;
    a = a.parent;
  }
  return s;
}
function bc(e, t = !1) {
  return e == null ? null : t && e.vnode.ctx || e.parent;
}
function Tc(e, t, n) {
  let s = null;
  _o(() => {
    if (process.env.NODE_ENV !== "production" && t.vnode.el) {
      t.vnode.el.__VUE_I18N__ = n, s = Vl();
      const o = n;
      o[qn] && o[qn](s), s.on("*", fo);
    }
  }, t), Pa(() => {
    const o = n;
    process.env.NODE_ENV !== "production" && t.vnode.el && t.vnode.el.__VUE_I18N__ && (s && s.off("*", fo), o[Kn] && o[Kn](), delete t.vnode.el.__VUE_I18N__), e.__deleteInstance(t);
    const a = o[Xn];
    a && (a(), delete o[Xn]);
  }, t);
}
function wc(e, t, n, s = {}) {
  const o = t === "local", a = yo(null);
  if (o && e.proxy && !(e.proxy.$options.i18n || e.proxy.$options.__i18n))
    throw je(Z.MUST_DEFINE_I18N_OPTION_IN_ALLOW_COMPOSITION);
  const r = q(s.inheritLocale) ? s.inheritLocale : !I(s.locale), l = ue(
    // prettier-ignore
    !o || r ? n.locale.value : I(s.locale) ? s.locale : Yt
  ), i = ue(
    // prettier-ignore
    !o || r ? n.fallbackLocale.value : I(s.fallbackLocale) || oe(s.fallbackLocale) || x(s.fallbackLocale) || s.fallbackLocale === !1 ? s.fallbackLocale : l.value
  ), d = ue(ds(l.value, s)), _ = ue(x(s.datetimeFormats) ? s.datetimeFormats : { [l.value]: {} }), h = ue(x(s.numberFormats) ? s.numberFormats : { [l.value]: {} }), m = o ? n.missingWarn : q(s.missingWarn) || wt(s.missingWarn) ? s.missingWarn : !0, b = o ? n.fallbackWarn : q(s.fallbackWarn) || wt(s.fallbackWarn) ? s.fallbackWarn : !0, u = o ? n.fallbackRoot : q(s.fallbackRoot) ? s.fallbackRoot : !0, v = !!s.fallbackFormat, y = K(s.missing) ? s.missing : null, f = K(s.postTranslation) ? s.postTranslation : null, g = o ? n.warnHtmlMessage : q(s.warnHtmlMessage) ? s.warnHtmlMessage : !0, w = !!s.escapeParameter, E = o ? n.modifiers : x(s.modifiers) ? s.modifiers : {}, L = s.pluralRules || o && n.pluralRules;
  function N() {
    return [
      l.value,
      i.value,
      d.value,
      _.value,
      h.value
    ];
  }
  const A = B({
    get: () => a.value ? a.value.locale.value : l.value,
    set: (c) => {
      a.value && (a.value.locale.value = c), l.value = c;
    }
  }), D = B({
    get: () => a.value ? a.value.fallbackLocale.value : i.value,
    set: (c) => {
      a.value && (a.value.fallbackLocale.value = c), i.value = c;
    }
  }), k = B(() => a.value ? a.value.messages.value : d.value), U = B(() => _.value), X = B(() => h.value);
  function W() {
    return a.value ? a.value.getPostTranslationHandler() : f;
  }
  function te(c) {
    a.value && a.value.setPostTranslationHandler(c);
  }
  function ne() {
    return a.value ? a.value.getMissingHandler() : y;
  }
  function ie(c) {
    a.value && a.value.setMissingHandler(c);
  }
  function J(c) {
    return N(), c();
  }
  function le(...c) {
    return a.value ? J(() => Reflect.apply(a.value.t, null, [...c])) : J(() => "");
  }
  function Ze(...c) {
    return a.value ? Reflect.apply(a.value.rt, null, [...c]) : "";
  }
  function ft(...c) {
    return a.value ? J(() => Reflect.apply(a.value.d, null, [...c])) : J(() => "");
  }
  function Nt(...c) {
    return a.value ? J(() => Reflect.apply(a.value.n, null, [...c])) : J(() => "");
  }
  function Ot(c) {
    return a.value ? a.value.tm(c) : {};
  }
  function be(c, T) {
    return a.value ? a.value.te(c, T) : !1;
  }
  function pt(c) {
    return a.value ? a.value.getLocaleMessage(c) : {};
  }
  function mt(c, T) {
    a.value && (a.value.setLocaleMessage(c, T), d.value[c] = T);
  }
  function kt(c, T) {
    a.value && a.value.mergeLocaleMessage(c, T);
  }
  function At(c) {
    return a.value ? a.value.getDateTimeFormat(c) : {};
  }
  function $t(c, T) {
    a.value && (a.value.setDateTimeFormat(c, T), _.value[c] = T);
  }
  function ht(c, T) {
    a.value && a.value.mergeDateTimeFormat(c, T);
  }
  function Je(c) {
    return a.value ? a.value.getNumberFormat(c) : {};
  }
  function Ue(c, T) {
    a.value && (a.value.setNumberFormat(c, T), h.value[c] = T);
  }
  function Qe(c, T) {
    a.value && a.value.mergeNumberFormat(c, T);
  }
  const St = {
    get id() {
      return a.value ? a.value.id : -1;
    },
    locale: A,
    fallbackLocale: D,
    messages: k,
    datetimeFormats: U,
    numberFormats: X,
    get inheritLocale() {
      return a.value ? a.value.inheritLocale : r;
    },
    set inheritLocale(c) {
      a.value && (a.value.inheritLocale = c);
    },
    get availableLocales() {
      return a.value ? a.value.availableLocales : Object.keys(d.value);
    },
    get modifiers() {
      return a.value ? a.value.modifiers : E;
    },
    get pluralRules() {
      return a.value ? a.value.pluralRules : L;
    },
    get isGlobal() {
      return a.value ? a.value.isGlobal : !1;
    },
    get missingWarn() {
      return a.value ? a.value.missingWarn : m;
    },
    set missingWarn(c) {
      a.value && (a.value.missingWarn = c);
    },
    get fallbackWarn() {
      return a.value ? a.value.fallbackWarn : b;
    },
    set fallbackWarn(c) {
      a.value && (a.value.missingWarn = c);
    },
    get fallbackRoot() {
      return a.value ? a.value.fallbackRoot : u;
    },
    set fallbackRoot(c) {
      a.value && (a.value.fallbackRoot = c);
    },
    get fallbackFormat() {
      return a.value ? a.value.fallbackFormat : v;
    },
    set fallbackFormat(c) {
      a.value && (a.value.fallbackFormat = c);
    },
    get warnHtmlMessage() {
      return a.value ? a.value.warnHtmlMessage : g;
    },
    set warnHtmlMessage(c) {
      a.value && (a.value.warnHtmlMessage = c);
    },
    get escapeParameter() {
      return a.value ? a.value.escapeParameter : w;
    },
    set escapeParameter(c) {
      a.value && (a.value.escapeParameter = c);
    },
    t: le,
    getPostTranslationHandler: W,
    setPostTranslationHandler: te,
    getMissingHandler: ne,
    setMissingHandler: ie,
    rt: Ze,
    d: ft,
    n: Nt,
    tm: Ot,
    te: be,
    getLocaleMessage: pt,
    setLocaleMessage: mt,
    mergeLocaleMessage: kt,
    getDateTimeFormat: At,
    setDateTimeFormat: $t,
    mergeDateTimeFormat: ht,
    getNumberFormat: Je,
    setNumberFormat: Ue,
    mergeNumberFormat: Qe
  };
  function p(c) {
    c.locale.value = l.value, c.fallbackLocale.value = i.value, Object.keys(d.value).forEach((T) => {
      c.mergeLocaleMessage(T, d.value[T]);
    }), Object.keys(_.value).forEach((T) => {
      c.mergeDateTimeFormat(T, _.value[T]);
    }), Object.keys(h.value).forEach((T) => {
      c.mergeNumberFormat(T, h.value[T]);
    }), c.escapeParameter = w, c.fallbackFormat = v, c.fallbackRoot = u, c.fallbackWarn = b, c.missingWarn = m, c.warnHtmlMessage = g;
  }
  return Ma(() => {
    if (e.proxy == null || e.proxy.$i18n == null)
      throw je(Z.NOT_AVAILABLE_COMPOSITION_IN_LEGACY);
    const c = a.value = e.proxy.$i18n.__composer;
    t === "global" ? (l.value = c.locale.value, i.value = c.fallbackLocale.value, d.value = c.messages.value, _.value = c.datetimeFormats.value, h.value = c.numberFormats.value) : o && p(c);
  }), St;
}
rc();
__INTLIFY_JIT_COMPILATION__ ? Ks(Qu) : Ks(Ju);
Gu(Cu);
zu(Ko);
if (process.env.NODE_ENV !== "production" || __INTLIFY_PROD_DEVTOOLS__) {
  const e = De();
  e.__INTLIFY__ = !0, Pu(e.__INTLIFY_DEVTOOLS_GLOBAL_HOOK__);
}
process.env.NODE_ENV;
var _a = /* @__PURE__ */ ((e) => (e.DEFAULT = "default", e.BLUE = "blue", e.ICON = "icon", e))(_a || {});
const Cc = /* @__PURE__ */ M("g", { id: "Ä°cons/loader" }, [
  /* @__PURE__ */ M("path", {
    id: "Union",
    d: "M7 1.5C5.70632 1.5 4.44729 1.91813 3.41063 2.69205C2.37397 3.46596 1.61522 4.55421 1.24749 5.79452C0.879769 7.03484 0.922777 8.36079 1.3701 9.57467C1.81743 10.7886 2.64511 11.8253 3.72975 12.5305C4.81438 13.2356 6.09786 13.5712 7.38882 13.4874C8.67978 13.4036 9.90907 12.9047 10.8934 12.0652C11.8777 11.2258 12.5644 10.0906 12.8509 8.82911C13.1375 7.56757 13.0087 6.24719 12.4836 5.06485L11.4068 5.54305C11.8287 6.49321 11.9323 7.5543 11.702 8.56811C11.4717 9.58192 10.9199 10.4941 10.1288 11.1688C9.33781 11.8434 8.34992 12.2443 7.31247 12.3116C6.27502 12.379 5.24357 12.1093 4.37193 11.5426C3.50029 10.976 2.83514 10.1428 2.47566 9.16726C2.11617 8.19176 2.08161 7.12619 2.37712 6.12943C2.67264 5.13268 3.2824 4.25813 4.11548 3.63619C4.94857 3.01425 5.96036 2.67823 7 2.67823V1.5Z",
    fill: "#808080"
  })
], -1), Lc = [
  Cc
], Nc = /* @__PURE__ */ M("path", {
  d: "M5.15009 5.85039L0.150085 0.850391L0.850085 0.150391L5.15009 4.45039L9.45009 0.150391L10.1501 0.850391L5.15009 5.85039Z",
  fill: "#808080"
}, null, -1), Oc = [
  Nc
], kc = {
  key: 1,
  xmlns: "http://www.w3.org/2000/svg",
  width: "36",
  height: "36",
  viewBox: "0 0 36 36",
  fill: "none"
}, Ac = /* @__PURE__ */ M("path", {
  d: "M27.5 9.44922H8.5L16.1 18.4362V24.6492L19.9 26.5492V18.4362L27.5 9.44922Z",
  stroke: "#8E8E8D",
  "stroke-width": "2",
  "stroke-linecap": "round",
  "stroke-linejoin": "round"
}, null, -1), $c = [
  Ac
], Sc = ["onClick"], Ic = ["onClick"], Pc = { key: 3 }, Mc = ["onClick"], Rc = /* @__PURE__ */ ae({
  __name: "select-component",
  props: {
    options: {},
    modelValue: { default: "" },
    triggerClass: {},
    label: {},
    isLoading: { type: Boolean },
    isDisabled: { type: Boolean },
    isError: { type: Boolean },
    isSuccess: { type: Boolean },
    errorText: {},
    withSearch: { type: Boolean, default: !1 },
    isNeedClear: { type: Boolean, default: !0 },
    selectType: { default: _a.DEFAULT },
    placement: { default: os.BottomStart },
    infoText: {},
    onlyRead: { type: Boolean },
    onlyReadText: {},
    withBigAdaptive: { type: Boolean, default: !0 }
  },
  emits: ["update:modelValue", "update"],
  setup(e, { emit: t }) {
    const n = t, s = e, o = ue(""), a = ue(!1), r = B(() => s.options.filter(
      (u) => u.label.toLowerCase().includes(o.value.toLowerCase())
    )), l = B(() => {
      const u = s.options.find(
        (v) => v.value === s.modelValue
      );
      return u ? u.label : "";
    }), i = (u, v) => {
      if (s.modelValue instanceof Array) {
        if (u.value === "") {
          n("update:modelValue", []), n("update");
          return;
        }
        s.modelValue.includes(u.value) ? n(
          "update:modelValue",
          s.modelValue.filter((f) => f !== u.value)
        ) : n("update:modelValue", [...s.modelValue, u.value]), n("update");
      } else
        n("update:modelValue", u.value), n("update"), v && v();
    }, { locale: d } = Ln(), _ = {
      en: {
        search: "Search",
        clear: "Clear"
      },
      ru: {
        search: "Поиск",
        clear: "Очистить"
      },
      ba: {
        search: "Эҙләү",
        clear: "Таҙартыу"
      }
    }, h = B(() => _[d.value].search), m = B(() => _[d.value].clear), b = (u) => {
      s.onlyRead || (a.value = !0);
    };
    return rt(
      () => a.value,
      (u) => {
        u || (o.value = "");
      }
    ), (u, v) => (S(), P("div", {
      class: R(u.$style.container)
    }, [
      u.label ? (S(), P("div", {
        key: 0,
        class: R(u.$style.label)
      }, pe(u.label), 3)) : j("", !0),
      ot(Ee(Et), {
        placement: u.placement,
        "auto-boundary-max-size": "",
        "overflow-padding": 100,
        "only-read": u.onlyRead,
        onHideDropdown: v[1] || (v[1] = (y) => a.value = !1)
      }, {
        trigger: Ce(() => [
          M("div", {
            class: R(u.$style.triggerContainer)
          }, [
            u.isLoading ? (S(), P("div", {
              key: 0,
              class: R(u.$style.loaderContainer)
            }, [
              (S(), P("svg", {
                width: "14",
                height: "15",
                viewBox: "0 0 14 15",
                fill: "none",
                xmlns: "http://www.w3.org/2000/svg",
                class: R(u.$style.loaderIcon)
              }, Lc, 2))
            ], 2)) : j("", !0),
            M("div", {
              class: R([
                u.$style[u.selectType],
                u.$style.trigger,
                u.triggerClass,
                u.modelValue.length && u.$style.active,
                u.withBigAdaptive && u.$style.triggerBigAdaptive,
                {
                  [u.$style.error]: u.isError,
                  [u.$style.success]: u.isSuccess,
                  [u.$style.loading]: u.isLoading,
                  [u.$style.disabled]: u.isDisabled
                }
              ]),
              onClick: b
            }, [
              ee(u.$slots, "trigger", {
                activeValue: u.modelValue || u.$attrs.placeholder || "Select"
              }, () => [
                u.selectType !== "icon" ? (S(), P("div", {
                  key: 0,
                  class: R(u.$style.select)
                }, [
                  M("span", null, pe(u.onlyRead ? u.onlyReadText : l.value || u.$attrs.placeholder || "Select"), 1),
                  u.isLoading ? j("", !0) : (S(), P("svg", {
                    key: 0,
                    viewBox: "0 0 11 6",
                    fill: "none",
                    xmlns: "http://www.w3.org/2000/svg",
                    class: R([
                      u.$style.arrow,
                      a.value && u.$style.rotate,
                      u.withBigAdaptive && u.$style.arrowBigAdaptive
                    ])
                  }, Oc, 2))
                ], 2)) : (S(), P("svg", kc, $c))
              ])
            ], 2),
            u.isError ? (S(), P("div", {
              key: 1,
              class: R([
                u.$style.errorText,
                u.withBigAdaptive && u.$style.errorTextBigAdaptive
              ])
            }, pe(u.errorText), 3)) : j("", !0),
            u.infoText && !u.isError ? (S(), P("div", {
              key: 2,
              class: R([
                u.$style.infoText,
                u.withBigAdaptive && u.$style.infoTextBigAdaptive
              ])
            }, pe(u.infoText), 3)) : j("", !0)
          ], 2)
        ]),
        dropdown: Ce(({ hide: y }) => [
          u.withSearch && u.options.length ? (S(), $e(Ee(bt), {
            key: 0,
            type: "text",
            placeholder: h.value,
            modelValue: o.value,
            "onUpdate:modelValue": v[0] || (v[0] = (f) => o.value = f),
            "container-class": u.$style.input
          }, null, 8, ["placeholder", "modelValue", "container-class"])) : j("", !0),
          u.modelValue.length && !u.isDisabled && u.isNeedClear ? (S(), P("div", {
            key: 1,
            class: R([
              u.$style.item,
              u.$style.reset,
              u.withBigAdaptive && u.$style.itemBigAdaptive
            ]),
            onClick: (f) => i({ value: "", label: "" }, y)
          }, pe(m.value), 11, Sc)) : j("", !0),
          u.modelValue instanceof Array ? (S(!0), P(Fe, { key: 2 }, Tt(r.value, (f) => (S(), P("div", {
            class: R([u.$style.item, u.withBigAdaptive && u.$style.itemBigAdaptive]),
            key: f.value,
            onClick: (g) => i(f)
          }, [
            ee(u.$slots, "select-item", {
              item: f,
              changeVisibleShowContent: y
            }, () => [
              ot(Ee(Mt), {
                "model-value": u.modelValue.includes(f.value),
                isSmall: "",
                onClick: (g) => i(f)
              }, null, 8, ["model-value", "onClick"]),
              ms(" " + pe(f.label), 1)
            ])
          ], 10, Ic))), 128)) : (S(), P("div", Pc, [
            (S(!0), P(Fe, null, Tt(r.value, (f) => (S(), P("div", {
              class: R([
                u.$style.item,
                u.modelValue === f.value && u.$style.isCheck,
                u.withBigAdaptive && u.$style.itemBigAdaptive,
                u.withBigAdaptive && u.$style.isCheckBigAdaptive
              ]),
              key: f.value,
              onClick: (g) => i(f, y)
            }, [
              ee(u.$slots, "select-item", {
                item: f,
                changeVisibleShowContent: y
              }, () => [
                ms(pe(f.label), 1)
              ])
            ], 10, Mc))), 128))
          ]))
        ]),
        _: 3
      }, 8, ["placement", "only-read"])
    ], 2));
  }
}), Dc = "_container_1gqzp_2", Fc = "_label_1gqzp_8", Vc = "_triggerContainer_1gqzp_14", Bc = "_input_1gqzp_18", Uc = "_trigger_1gqzp_14", xc = "_triggerBigAdaptive_1gqzp_45", Hc = "_active_1gqzp_62", Wc = "_blue_1gqzp_67", Gc = "_icon_1gqzp_81", zc = "_select_1gqzp_99", Yc = "_notActive_1gqzp_107", jc = "_arrow_1gqzp_111", qc = "_arrowBigAdaptive_1gqzp_119", Kc = "_rotate_1gqzp_125", Xc = "_loaderIcon_1gqzp_129", Zc = "_loaderContainer_1gqzp_133", Jc = "_item_1gqzp_143", Qc = "_itemBigAdaptive_1gqzp_169", ed = "_reset_1gqzp_176", td = "_errorText_1gqzp_180", nd = "_errorTextBigAdaptive_1gqzp_191", sd = "_infoText_1gqzp_196", od = "_infoTextBigAdaptive_1gqzp_207", ad = "_error_1gqzp_180", rd = "_success_1gqzp_217", id = "_loading_1gqzp_221", ld = "_disabled_1gqzp_225", ud = "_isCheck_1gqzp_229", cd = "_isCheckBigAdaptive_1gqzp_254", dd = {
  container: Dc,
  label: Fc,
  triggerContainer: Vc,
  input: Bc,
  trigger: Uc,
  triggerBigAdaptive: xc,
  default: "_default_1gqzp_53",
  active: Hc,
  blue: Wc,
  icon: Gc,
  select: zc,
  notActive: Yc,
  arrow: jc,
  arrowBigAdaptive: qc,
  rotate: Kc,
  loaderIcon: Xc,
  loaderContainer: Zc,
  item: Jc,
  itemBigAdaptive: Qc,
  reset: ed,
  errorText: td,
  errorTextBigAdaptive: nd,
  infoText: sd,
  infoTextBigAdaptive: od,
  error: ad,
  success: rd,
  loading: id,
  disabled: ld,
  isCheck: ud,
  isCheckBigAdaptive: cd
}, fd = {
  $style: dd
}, sn = /* @__PURE__ */ Ie(Rc, [["__cssModules", fd]]);
sn.install = (e) => {
  e.component(sn.__name, sn);
};
const pd = /* @__PURE__ */ ae({
  __name: "loader-component",
  props: {
    size: { default: "72px" }
  },
  setup(e) {
    return Da((t) => ({
      "96c15bf0": t.size
    })), (t, n) => (S(), P("div", {
      class: R(t.$style.loader)
    }, null, 2));
  }
}), md = "_loader_exowx_1", hd = "_rotation_exowx_1", _d = {
  loader: md,
  rotation: hd
}, gd = {
  $style: _d
}, on = /* @__PURE__ */ Ie(pd, [["__cssModules", gd]]);
on.install = (e) => {
  e.component(on.__name, on);
};
const vd = ["fill"], yd = ["onClick"], Ed = ["fill"], bd = /* @__PURE__ */ ae({
  __name: "pagination-component",
  props: {
    modelValue: {},
    total: {}
  },
  emits: ["update:modelValue", "update"],
  setup(e, { emit: t }) {
    const n = t, s = Bi("(max-width: 500px)"), o = e, a = B(() => ({
      first: {
        length: s.value ? 5 : 9,
        step: 1
      },
      middle: {
        length: s.value ? 3 : 7,
        step: s.value ? 1 : 3
      },
      last: {
        length: s.value ? 5 : 9,
        step: s.value ? 4 : 8
      }
    })), r = B(() => o.total < 12 ? 0 : o.modelValue < (s.value ? 5 : 6) ? 1 : o.modelValue < o.total - (s.value ? 3 : 4) ? 2 : 3), l = B(() => r.value === 0), i = B(() => r.value === 1), d = B(() => r.value === 2), _ = B(() => r.value === 3), h = B(() => l.value ? Array.from(Array(o.total), (v, y) => y + 1) : i.value ? Array.from(
      Array(a.value.first.length),
      (v, y) => y + a.value.first.step
    ) : d.value ? Array.from(
      Array(a.value.middle.length),
      (v, y) => y + o.modelValue - a.value.middle.step
    ) : Array.from(
      Array(a.value.last.length),
      (v, y) => y + o.total - a.value.last.step
    )), m = (v) => {
      n("update:modelValue", v), n("update");
    }, b = () => {
      o.modelValue !== 1 && m(o.modelValue - 1);
    }, u = () => {
      o.modelValue !== o.total && m(o.modelValue + 1);
    };
    return (v, y) => v.total > 1 ? (S(), P("div", {
      key: 0,
      class: R(v.$style.pagination)
    }, [
      l.value ? j("", !0) : (S(), P("svg", {
        key: 0,
        xmlns: "http://www.w3.org/2000/svg",
        viewBox: "0 0 20 20",
        fill: "none",
        class: R([v.$style.prev, v.$style.arrow, v.modelValue === 1 && v.$style.disabled]),
        onClick: b
      }, [
        M("path", {
          d: "M7.76394 14.3173C7.58798 14.1391 7.5 13.9335 7.5 13.7007C7.5 13.4679 7.58798 13.2624 7.76394 13.0842L10.8188 10.0013L7.76394 6.91845C7.58798 6.74022 7.5 6.5347 7.5 6.30188C7.5 6.06907 7.58798 5.86354 7.76394 5.68531C7.94055 5.50708 8.14421 5.41797 8.37492 5.41797C8.60627 5.41797 8.80993 5.50708 8.98589 5.68531L12.6517 9.38473C12.7469 9.48075 12.815 9.58006 12.8561 9.68266C12.8965 9.78526 12.9167 9.89147 12.9167 10.0013C12.9167 10.1111 12.8965 10.2173 12.8561 10.3199C12.815 10.4225 12.7469 10.5219 12.6517 10.6179L8.98589 14.3173C8.80993 14.4955 8.60627 14.5846 8.37492 14.5846C8.14421 14.5846 7.94055 14.4955 7.76394 14.3173Z",
          fill: v.modelValue === 1 ? "#808080" : "#0E89E6"
        }, null, 8, vd)
      ], 2)),
      M("div", {
        class: R([v.$style.pagination])
      }, [
        !i.value && !l.value ? (S(), P("div", {
          key: 0,
          class: R(v.$style.item),
          onClick: y[0] || (y[0] = (f) => m(1))
        }, " 1 ", 2)) : j("", !0),
        !i.value && !l.value ? (S(), P("div", {
          key: 1,
          class: R(v.$style.dot)
        }, "...", 2)) : j("", !0),
        (S(!0), P(Fe, null, Tt(h.value, (f) => (S(), P("div", {
          key: f,
          class: R([v.$style.item, f === v.modelValue && v.$style.active]),
          onClick: (g) => m(f)
        }, pe(f), 11, yd))), 128)),
        !_.value && !l.value ? (S(), P("div", {
          key: 2,
          class: R(v.$style.dot)
        }, "...", 2)) : j("", !0),
        !_.value && !l.value ? (S(), P("div", {
          key: 3,
          class: R(v.$style.item),
          onClick: y[1] || (y[1] = (f) => m(v.total))
        }, pe(v.total), 3)) : j("", !0)
      ], 2),
      l.value ? j("", !0) : (S(), P("svg", {
        key: 1,
        xmlns: "http://www.w3.org/2000/svg",
        viewBox: "0 0 20 20",
        fill: "none",
        class: R([v.$style.arrow, v.modelValue === v.total && v.$style.disabled]),
        onClick: u
      }, [
        M("path", {
          d: "M7.76394 14.3173C7.58798 14.1391 7.5 13.9335 7.5 13.7007C7.5 13.4679 7.58798 13.2624 7.76394 13.0842L10.8188 10.0013L7.76394 6.91845C7.58798 6.74022 7.5 6.5347 7.5 6.30188C7.5 6.06907 7.58798 5.86354 7.76394 5.68531C7.94055 5.50708 8.14421 5.41797 8.37492 5.41797C8.60627 5.41797 8.80993 5.50708 8.98589 5.68531L12.6517 9.38473C12.7469 9.48075 12.815 9.58006 12.8561 9.68266C12.8965 9.78526 12.9167 9.89147 12.9167 10.0013C12.9167 10.1111 12.8965 10.2173 12.8561 10.3199C12.815 10.4225 12.7469 10.5219 12.6517 10.6179L8.98589 14.3173C8.80993 14.4955 8.60627 14.5846 8.37492 14.5846C8.14421 14.5846 7.94055 14.4955 7.76394 14.3173Z",
          fill: v.modelValue === v.total ? "#808080" : "#0E89E6"
        }, null, 8, Ed)
      ], 2))
    ], 2)) : j("", !0);
  }
}), Td = "_pagination_i3utt_1", wd = "_prev_i3utt_12", Cd = "_arrow_i3utt_16", Ld = "_item_i3utt_29", Nd = "_active_i3utt_40", Od = "_dot_i3utt_61", kd = "_disabled_i3utt_89", Ad = {
  pagination: Td,
  prev: wd,
  arrow: Cd,
  item: Ld,
  active: Nd,
  dot: Od,
  disabled: kd
}, $d = {
  $style: Ad
}, an = /* @__PURE__ */ Ie(bd, [["__cssModules", $d]]);
an.install = (e) => {
  e.component(an.__name, an);
};
var Zn = { name: "Toggle", emits: ["input", "update:modelValue", "change"], props: { value: { validator: function(e) {
  return (t) => ["number", "string", "boolean"].indexOf(typeof t) !== -1 || t == null;
}, required: !1 }, modelValue: { validator: function(e) {
  return (t) => ["number", "string", "boolean"].indexOf(typeof t) !== -1 || t == null;
}, required: !1 }, id: { type: [String, Number], required: !1, default: "toggle" }, name: { type: [String, Number], required: !1, default: "toggle" }, disabled: { type: Boolean, required: !1, default: !1 }, required: { type: Boolean, required: !1, default: !1 }, falseValue: { type: [String, Number, Boolean], required: !1, default: !1 }, trueValue: { type: [String, Number, Boolean], required: !1, default: !0 }, onLabel: { type: [String, Object], required: !1, default: "" }, offLabel: { type: [String, Object], required: !1, default: "" }, classes: { type: Object, required: !1, default: () => ({}) }, labelledby: { type: String, required: !1 }, describedby: { type: String, required: !1 }, aria: { required: !1, type: Object, default: () => ({}) } }, setup(e, t) {
  const n = function(r, l, i) {
    const { value: d, modelValue: _, falseValue: h, trueValue: m, disabled: b } = Pt(r), u = _ && _.value !== void 0 ? _ : d, v = B(() => u.value === m.value), y = (w) => {
      l.emit("input", w), l.emit("update:modelValue", w), l.emit("change", w);
    }, f = () => {
      y(m.value);
    }, g = () => {
      y(h.value);
    };
    return [null, void 0, !1, 0, "0", "off"].indexOf(u.value) !== -1 && [h.value, m.value].indexOf(u.value) === -1 && g(), [!0, 1, "1", "on"].indexOf(u.value) !== -1 && [h.value, m.value].indexOf(u.value) === -1 && f(), { externalValue: u, checked: v, update: y, check: f, uncheck: g, handleInput: (w) => {
      y(w.target.checked ? m.value : h.value);
    }, handleClick: () => {
      b.value || (v.value ? g() : f());
    } };
  }(e, t), s = function(r, l, i) {
    const { trueValue: d, falseValue: _, onLabel: h, offLabel: m } = Pt(r), b = i.checked, u = i.update;
    return { label: B(() => {
      let v = b.value ? h.value : m.value;
      return v || (v = "&nbsp;"), v;
    }), toggle: () => {
      u(b.value ? _.value : d.value);
    }, on: () => {
      u(d.value);
    }, off: () => {
      u(_.value);
    } };
  }(e, 0, { checked: n.checked, update: n.update }), o = function(r, l, i) {
    const d = Pt(r), _ = d.disabled, h = i.checked, m = B(() => ({ container: "toggle-container", toggle: "toggle", toggleOn: "toggle-on", toggleOff: "toggle-off", toggleOnDisabled: "toggle-on-disabled", toggleOffDisabled: "toggle-off-disabled", handle: "toggle-handle", handleOn: "toggle-handle-on", handleOff: "toggle-handle-off", handleOnDisabled: "toggle-handle-on-disabled", handleOffDisabled: "toggle-handle-off-disabled", label: "toggle-label", ...d.classes.value }));
    return { classList: B(() => ({ container: m.value.container, toggle: [m.value.toggle, _.value ? h.value ? m.value.toggleOnDisabled : m.value.toggleOffDisabled : h.value ? m.value.toggleOn : m.value.toggleOff], handle: [m.value.handle, _.value ? h.value ? m.value.handleOnDisabled : m.value.handleOffDisabled : h.value ? m.value.handleOn : m.value.handleOff], label: m.value.label })) };
  }(e, 0, { checked: n.checked }), a = function(r, l, i) {
    const { disabled: d } = Pt(r), _ = i.check, h = i.uncheck, m = i.checked;
    return { handleSpace: () => {
      d.value || (m.value ? h() : _());
    } };
  }(e, 0, { check: n.check, uncheck: n.uncheck, checked: n.checked });
  return { ...n, ...o, ...s, ...a };
} };
const Sd = ["tabindex", "aria-checked", "aria-describedby", "aria-labelledby"], Id = ["id", "name", "value", "checked", "disabled"], Pd = ["innerHTML"], Md = ["checked"];
Zn.render = function(e, t, n, s, o, a) {
  return S(), P("div", ct({ class: e.classList.container, tabindex: n.disabled ? void 0 : 0, "aria-checked": e.checked, "aria-describedby": n.describedby, "aria-labelledby": n.labelledby, role: "switch" }, n.aria, { onKeypress: t[1] || (t[1] = ho(Fa((...r) => e.handleSpace && e.handleSpace(...r), ["prevent"]), ["space"])) }), [go(M("input", { type: "checkbox", id: n.id, name: n.name, value: n.trueValue, checked: e.checked, disabled: n.disabled }, null, 8, Id), [[Va, !1]]), M("div", { class: R(e.classList.toggle), onClick: t[0] || (t[0] = (...r) => e.handleClick && e.handleClick(...r)) }, [M("span", { class: R(e.classList.handle) }, null, 2), ee(e.$slots, "label", { checked: e.checked, classList: e.classList }, () => [M("span", { class: R(e.classList.label), innerHTML: e.label }, null, 10, Pd)]), n.required ? (S(), P("input", { key: 0, type: "checkbox", style: { appearance: "none", height: "1px", margin: "0", padding: "0", fontSize: "0", background: "transparent", position: "absolute", width: "100%", bottom: "0", outline: "none" }, checked: e.checked, "aria-hidden": "true", tabindex: "-1", required: "" }, null, 8, Md)) : j("v-if", !0)], 2)], 16, Sd);
}, Zn.__file = "src/Toggle.vue";
const rn = /* @__PURE__ */ ae({
  __name: "toggle-component",
  props: {
    modelValue: { type: Boolean, default: void 0 },
    name: { default: "toggle" },
    disabled: { type: Boolean, default: !1 },
    required: { type: Boolean, default: !1 },
    falseValue: { type: [String, Number, Boolean], default: !1 },
    trueValue: { type: [String, Number, Boolean], default: !0 },
    offLabel: { default: "" },
    onLabel: { default: "" },
    labelledby: { default: "" },
    describedby: { default: "" }
  },
  emits: ["update:modelValue"],
  setup(e, { emit: t }) {
    const n = t, s = {
      container: "toggle-container",
      toggle: "toggle",
      toggleOn: "toggle-on",
      handle: "toggle-handle",
      handleOn: "toggle-handle-on",
      handleOff: "toggle-handle-off"
    };
    return (o, a) => (S(), $e(Ee(Zn), {
      "model-value": o.modelValue,
      "onUpdate:modelValue": a[0] || (a[0] = (r) => n("update:modelValue", r)),
      name: o.name,
      disabled: o.disabled,
      required: o.required,
      falseValue: o.falseValue,
      trueValue: o.trueValue,
      offLabel: o.offLabel,
      onLabel: o.onLabel,
      labelledby: o.labelledby,
      describedby: o.describedby,
      classes: s
    }, null, 8, ["model-value", "name", "disabled", "required", "falseValue", "trueValue", "offLabel", "onLabel", "labelledby", "describedby"]));
  }
});
rn.install = (e) => {
  e.component(rn.__name, rn);
};
const Rd = ["innerHTML"], Dd = /* @__PURE__ */ ae({
  __name: "autocomplete-item",
  props: {
    text: { default: "" },
    search: { default: "" }
  },
  setup(e) {
    const t = e, n = B(() => t.text.replace(
      new RegExp(t.search, "g"),
      `<span class="active">${t.search}</span>`
    ));
    return (s, o) => (S(), P("div", {
      class: R(s.$style.container)
    }, [
      M("span", { innerHTML: n.value }, null, 8, Rd)
    ], 2));
  }
}), Fd = "_container_13z89_1", Vd = "_text_13z89_9", Bd = {
  container: Fd,
  text: Vd
}, Ud = {
  $style: Bd
}, Ut = /* @__PURE__ */ Ie(Dd, [["__cssModules", Ud]]), xd = /* @__PURE__ */ ae({
  __name: "autocomplete-component",
  props: {
    modelValue: { default: "" },
    autoCompleteList: { default: null }
  },
  emits: ["update:modelValue"],
  setup(e, { emit: t }) {
    const n = e, s = t, o = ue(!1), a = B(() => n.modelValue.length && n.autoCompleteList && n.autoCompleteList.length && !o.value), r = B(() => n.autoCompleteList ? n.autoCompleteList.filter(
      (i) => i.title.toLowerCase().includes(n.modelValue.toLowerCase())
    ) : []), l = (i) => {
      s("update:modelValue", i.title), mo(() => {
        o.value = !0;
      });
    };
    return rt(
      () => n.modelValue,
      () => {
        o.value = !1;
      }
    ), (i, d) => (S(), P("div", {
      class: R(i.$style.container)
    }, [
      ot(Ee(bt), ct(i.$attrs, {
        "model-value": i.modelValue,
        "onUpdate:modelValue": d[0] || (d[0] = (_) => s("update:modelValue", _))
      }), null, 16, ["model-value"]),
      ot(Ba, null, {
        default: Ce(() => [
          a.value ? (S(), P("div", {
            key: 0,
            class: R(i.$style.list)
          }, [
            ot(Ua, { name: "list" }, {
              default: Ce(() => [
                (S(!0), P(Fe, null, Tt(r.value, (_) => (S(), $e(Ut, {
                  key: _.id,
                  search: i.modelValue,
                  text: _.title,
                  onClick: (h) => l(_)
                }, null, 8, ["search", "text", "onClick"]))), 128))
              ]),
              _: 1
            })
          ], 2)) : j("", !0)
        ]),
        _: 1
      })
    ], 2));
  }
}), Hd = "_container_bpykv_1", Wd = "_list_bpykv_6", Gd = {
  container: Hd,
  list: Wd
}, zd = {
  $style: Gd
}, ln = /* @__PURE__ */ Ie(xd, [["__cssModules", zd]]);
ln.install = (e) => {
  e.component(ln.__name, ln);
};
Ut.install = (e) => {
  e.component(Ut.__name, Ut);
};
var Jn = /* @__PURE__ */ ((e) => (e.LEFT = "left", e.RIGHT = "right", e))(Jn || {});
const Yd = ["onClick"], jd = /* @__PURE__ */ ae({
  __name: "count-per-page",
  props: {
    title: { default: "" },
    options: { default: () => [10, 20, 50, 100] },
    position: { default: Jn.LEFT },
    modelValue: {}
  },
  emits: ["update:modelValue", "update"],
  setup(e, { emit: t }) {
    const n = t, s = (o) => {
      n("update:modelValue", o), n("update", o);
    };
    return (o, a) => (S(), P("div", {
      class: R([
        o.$style.positions,
        o.position === Ee(Jn).RIGHT && o.$style.revertList
      ])
    }, [
      M("p", null, pe(o.title), 1),
      M("div", null, [
        (S(!0), P(Fe, null, Tt(o.options, (r) => (S(), P("button", {
          key: r,
          class: R([
            o.$style.position,
            o.modelValue === r && o.$style.activePosition
          ]),
          onClick: (l) => s(r)
        }, pe(r), 11, Yd))), 128))
      ])
    ], 2));
  }
}), qd = "_positions_tjlro_1", Kd = "_revertList_tjlro_11", Xd = "_position_tjlro_1", Zd = "_activePosition_tjlro_32", Jd = {
  positions: qd,
  revertList: Kd,
  position: Xd,
  activePosition: Zd
}, Qd = {
  $style: Jd
}, un = /* @__PURE__ */ Ie(jd, [["__cssModules", Qd]]);
un.install = (e) => {
  e.component(un.__name, un);
};
const ef = [
  Qt,
  en,
  Mt,
  Et,
  bt,
  sn,
  an,
  rn,
  ln,
  Ut,
  un,
  on
], tf = (e) => {
  ef.forEach((t) => {
    e.component(t.__name, t);
  });
}, sf = {
  install: tf
};
export {
  ln as Autocomplete,
  Ut as AutocompleteItem,
  en as Button,
  Eo as ButtonType,
  Mt as CheckBox,
  un as CountPerPage,
  Et as Dropdown,
  os as DropdownPlacement,
  Li as DropdownTrigger,
  bt as Input,
  Ho as InputType,
  Qt as Label,
  bo as LabelPosition,
  on as Loader,
  an as Pagination,
  sn as Select,
  _a as SelectType,
  Jn as TitlePositionEnum,
  rn as Toggle,
  sf as default
};
