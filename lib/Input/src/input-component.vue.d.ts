import { InputType } from "./InputType.enum";
export interface InputProps {
    isError?: boolean;
    isSuccess?: boolean;
    isLoading?: boolean;
    isDisabled?: boolean;
    modelValue: string;
    isTextarea?: boolean;
    containerClass?: string;
    errorText?: string;
    label?: string;
    isNeedSearchIcon?: boolean;
    info?: Array<string>;
    maskPattern?: string;
    dataMaskTokens?: string;
    inputType?: InputType;
    commentTextAreaStyle: string;
}
declare const _default: __VLS_WithTemplateSlots<import("vue").DefineComponent<__VLS_WithDefaults<__VLS_TypePropsToRuntimeProps<InputProps>, {
    isLoading: boolean;
    isDisabled: boolean;
    isError: boolean;
    isSuccess: boolean;
    modelValue: string;
    isTextarea: boolean;
    containerClass: string;
    errorText: string;
    label: string;
    isNeedSearchIcon: boolean;
    maskPattern: string;
    dataMaskTokens: string;
    inputType: InputType;
    commentTextAreaStyle: string;
}>, {}, unknown, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {
    "update:modelValue": (...args: any[]) => void;
    "update:isError": (...args: any[]) => void;
}, string, import("vue").PublicProps, Readonly<import("vue").ExtractPropTypes<__VLS_WithDefaults<__VLS_TypePropsToRuntimeProps<InputProps>, {
    isLoading: boolean;
    isDisabled: boolean;
    isError: boolean;
    isSuccess: boolean;
    modelValue: string;
    isTextarea: boolean;
    containerClass: string;
    errorText: string;
    label: string;
    isNeedSearchIcon: boolean;
    maskPattern: string;
    dataMaskTokens: string;
    inputType: InputType;
    commentTextAreaStyle: string;
}>>> & {
    "onUpdate:modelValue"?: ((...args: any[]) => any) | undefined;
    "onUpdate:isError"?: ((...args: any[]) => any) | undefined;
}, {
    modelValue: string;
    label: string;
    isError: boolean;
    isSuccess: boolean;
    isLoading: boolean;
    isDisabled: boolean;
    isTextarea: boolean;
    containerClass: string;
    errorText: string;
    isNeedSearchIcon: boolean;
    maskPattern: string;
    dataMaskTokens: string;
    inputType: InputType;
    commentTextAreaStyle: string;
}, {}>, {
    "search-icon"?(_: {}): any;
    "close-icon"?(_: {
        isShowCloseButton: number | true;
        clear: () => void;
    }): any;
    successIcon?(_: {}): any;
    append?(_: {}): any;
    extra?(_: {}): any;
    comment?(_: {}): any;
    "info-trigger"?(_: {}): any;
    "info-content"?(_: {}): any;
}>;
export default _default;
type __VLS_NonUndefinedable<T> = T extends undefined ? never : T;
type __VLS_TypePropsToRuntimeProps<T> = {
    [K in keyof T]-?: {} extends Pick<T, K> ? {
        type: import('vue').PropType<__VLS_NonUndefinedable<T[K]>>;
    } : {
        type: import('vue').PropType<T[K]>;
        required: true;
    };
};
type __VLS_WithDefaults<P, D> = {
    [K in keyof Pick<P, keyof P>]: K extends keyof D ? __VLS_Prettify<P[K] & {
        default: D[K];
    }> : P[K];
};
type __VLS_Prettify<T> = {
    [K in keyof T]: T[K];
} & {};
type __VLS_WithTemplateSlots<T, S> = T & {
    new (): {
        $slots: S;
    };
};
