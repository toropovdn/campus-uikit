export declare enum ButtonType {
    BLUE = "blue",
    ACCENT = "accent",
    WHITE = "white",
    TEXT = "text",
    SECONDARY = "secondary",
    SKYBLUE = "skyblue"
}
