import { defineConfig } from "vite";
import svgLoader from "vite-svg-loader";
import vue from "@vitejs/plugin-vue";
import * as path from "path";
import dts from "vite-plugin-dts";

export default defineConfig({
  plugins: [vue(), dts(), svgLoader()],
  resolve: {
    dedupe: ["vue"],
  },
  build: {
    outDir: "lib",
    lib: {
      entry: path.resolve(__dirname, "packages/index.ts"), // Specify the component compile entry file
      name: "CampusUiKit",
      fileName: "campus-uikit",
    }, // Library compilation mode configuration
    rollupOptions: {
      // Make sure that externalization of those dependencies you don't want to pack into the library
      external: ["vue"],
      output: {
        // Provide a global variable for these externalized dependencies in the UMD construction mode
        globals: {
          vue: "Vue",
        },
      },
    }, // rollup packaging configuration
  },
});
