import { createRouter, createWebHistory } from "vue-router";
import HomeView from "./App.vue";

const router = createRouter({
  history: createWebHistory(),
  routes: [
    {
      path: "/",
      name: "home",
      component: HomeView,
    },
    {
      path: "/slash",
      name: "slash",
      component: HomeView,
    },
  ],
});

export default router;
