import { createApp } from "vue";
import "./style.css";
import router from "./router";
import App from "./App.vue";

import { createI18n } from 'vue-i18n'

const i18n = createI18n({
    legacy: false,
    locale: 'ru',
    fallbackLocale: 'ru',
})

createApp(App).use(router).use(i18n).mount("#app");
