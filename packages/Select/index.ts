import SelectComponent from "./src/select-component.vue";
import { App } from "vue";

SelectComponent.install = (App: App) => {
  App.component(SelectComponent.__name as string, SelectComponent);
};

export default SelectComponent;
