import PaginationComponent from "./src/pagination-component.vue";
import { App } from "vue";

PaginationComponent.install = (App: App) => {
  App.component(PaginationComponent.__name as string, PaginationComponent);
};

export default PaginationComponent;
