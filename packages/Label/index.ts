import LabelComponent from "./src/label-component.vue";
import { App } from "vue";

LabelComponent.install = (App: App) => {
  App.component(LabelComponent.__name as string, LabelComponent);
};

export default LabelComponent;
