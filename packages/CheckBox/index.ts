import CheckBox from "./src/CheckBox.vue";
import { App } from "vue";

CheckBox.install = (App: App) => {
  App.component(CheckBox.__name as string, CheckBox);
};

export * from "./src/LabelPosition.enum";
export default CheckBox;
