import LoaderComponent from "./src/loader-component.vue";
import { App } from "vue";

LoaderComponent.install = (App: App) => {
  App.component(LoaderComponent.__name as string, LoaderComponent);
};

export default LoaderComponent;
