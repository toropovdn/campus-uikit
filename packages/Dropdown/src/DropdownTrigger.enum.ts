export enum DropdownTrigger {
  Click = 'click',
  Hover = 'hover',
  Focus = 'focus',
  Touch = 'touch',
}
