import DropdownComponent from "./src/dropdown-component.vue";
import { App } from "vue";

DropdownComponent.install = (App: App) => {
  App.component(DropdownComponent.__name as string, DropdownComponent);
};

export default DropdownComponent;
