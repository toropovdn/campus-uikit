import CountPerPageComponent from "./src/count-per-page.vue";
import { App } from "vue";

CountPerPageComponent.install = (App: App) => {
  App.component(CountPerPageComponent.__name as string, CountPerPageComponent);
};

export default CountPerPageComponent;
