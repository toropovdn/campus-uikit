import Autocomplete from "./src/autocomplete-component.vue";
import AutocompleteItem from "./src/autocomplete-item.vue";
import { App } from "vue";

Autocomplete.install = (App: App) => {
  App.component(Autocomplete.__name as string, Autocomplete);
};

AutocompleteItem.install = (App: App) => {
  App.component(AutocompleteItem.__name as string, AutocompleteItem);
};

export { Autocomplete, AutocompleteItem };
