import ToggleComponent from "./src/toggle-component.vue";
import { App } from "vue";

ToggleComponent.install = (App: App) => {
  App.component(ToggleComponent.__name as string, ToggleComponent);
};

export default ToggleComponent;
