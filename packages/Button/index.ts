import Button from "./src/Button.vue";
import { App } from "vue";

Button.install = (App: App) => {
  App.component(Button.__name as string, Button);
};

export * from "./src/ButtonType.enum";
export default Button;
