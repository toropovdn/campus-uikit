import InputComponent from "./src/input-component.vue";
import { App } from "vue";

InputComponent.install = (App: App) => {
    App.component(InputComponent.__name as string, InputComponent);
};

export default InputComponent;
